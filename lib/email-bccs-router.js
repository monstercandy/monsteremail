var lib = module.exports = function(app, knexes) {

    var router = app.ExpressPromiseRouter({mergeParams: true})

    var bccsLib = require("lib-bccs.js")

    var bccs = bccsLib(app, knexes)

    router.post("/list", function(req,res,next){
      return req.sendPromResultAsIs(bccs.GetBccsByUserAccountAndWebhosting(req.body.json))
    })


    
    router.route("/bcc/:bcc")
      .post(function(req,res,next){
          return req.sendOk(
             bccs.GetBccById(req.params.bcc)
             .then(bcc=>{
                return bcc.Change(req.body.json);
             })
          )

      })    
      .delete(function(req,res,next){
          return req.sendOk(
             bccs.GetBccById(req.params.bcc)
             .then(bcc=>{
                return bcc.Delete(null, req)
             })
          )

      })

    router.route("/")
      .get(function(req,res,next){
         return bccs.GetBccs(req.params.webhosting_id).then(bccs=>{
         	req.sendResponse({bccs: bccs.Cleanup()})
         })
      })


    return router


}
