var lib = module.exports = function(app, knexes) {


    var router = app.ExpressPromiseRouter({mergeParams: true})

    var accountsLib = require("lib-accounts.js")

    var accounts = accountsLib(app, knexes)

    router.route("/report/tallies")
      .post(function(req,res,next){
         return req.sendOk(
            accounts.ReportTallies()
         )
      })

    router.route("/cleanup/autoexpunge")
      .post(function(req,res,next){
         var emitter = app.commander.EventEmitter()
         return emitter.spawn()
           .then(h=>{
              req.sendTask(Promise.resolve(h))

              return accounts.AutoExpunge(emitter)
                .then(()=>{
                     // everything went well
                     emitter.close()
                })
                .catch(ex=>{
                     console.error("error during autoexpunge commands", ex)
                     emitter.close(1)
                })
           })

      })

    router.route("/cleanup/junk")
      .post(function(req,res,next){
         return req.sendTask(
            accounts.CleanupJunkFolders()
            .then(h=>{
                app.InsertEvent(req, {e_event_type: "cleanup-junk"});
                return h;
            })
         )
      })


    router.route("/email/:account")
      .get(getAccountByEmail, function(req,res,next){
        return req.sendResponse(req.account.Cleanup())
      })
      .post(getAccountByEmail, function(req,res,next){
        return req.sendOk(
            req.account.Change(req.body.json, req)
        )
      })

    router.route("/account/:account/tallies/recalculate")
      .post(getAccountById, function(req,res,next){
         return req.sendTask(
            req.account.RecalculateTallies()
            .then(h=>{
                app.InsertEvent(req, {e_event_type: "recalculate-tallies"});
                return h;
            })
          )
      })

    router.route("/account/:account")
      .post(getAccountById, function(req,res,next){
        return req.sendOk(
            req.account.Change(req.body.json, req)
        )
      })
      .delete(getAccountById, function(req,res,next){
         var emitter = app.commander.EventEmitter()
         return emitter.spawn()
           .then(h=>{
              req.sendTask(Promise.resolve(h))

              return req.account.Delete(req.body.json, emitter, req)
                 .then(()=>{
                     emitter.close()
                 })
                 .catch(ex=>{
                    console.error("Unable to delete account", ex)
                    emitter.close(1)
                 })
            })
      })

    router.post("/mail-service-account/fetch", function(req,res,next){
       return req.sendPromResultAsIs(accounts.LookupServiceAccount(req.body.json));
    })

    router.post("/list", function(req,res,next){
      return req.sendPromResultAsIs(accounts.GetAccountsByUserAccountAndWebhosting(req.body.json))
    })

    router.route("/")
      .get(function(req,res,next){
         return accounts.GetAccounts(req.params.webhosting_id).then(accounts=>{
         	req.sendResponse({accounts:accounts.Cleanup()})
         })
      })



    if(!app.cron_accounts_configured) {
          app.cron_accounts_configured = true

            const cron = require('Croner');

            var csp = app.config.get("cron_doveadm_cleanup")
            if(csp) {
                cron.schedule(csp, function(){
                    return accounts.CleanupJunkFolders()
                       .then(h=>{
                          return h.executionPromise.catch(ex=>{
                              console.error("error during cronned junk cleanup", ex)
                          })
                       })
                       .then(()=>{
                          return accounts.AutoExpunge()
                       })

                });
            }

            csp = app.config.get("cron_report_tallies")
            if(csp) {
                cron.schedule(csp, function(){
                    return accounts.ReportTallies()
                });
            }

    }


    return router

    function getAccountById(req,res,next){
         return accounts.GetAccountById(req.params.account)
           .then(function(account){
              req.account = account
              return next()
           })
    }


    function getAccountByEmail(req,res,next){
         return accounts.GetAccountByEmail(req.params.account)
           .then(function(account){
              req.account = account
              return next()
           })
    }
}
