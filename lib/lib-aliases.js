var lib = module.exports = function(app, knexes, domain){

    var knexPostfix = knexes.postfixDovecot

    const KnexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()

    var Common = ValidatorsLib.array()

    var moment = require("MonsterMoment")

	var re = {}

  function getAliasesBy(filterHash, joinDomain) {

    var f = KnexLib.filterHash(filterHash)

    var p = knexPostfix("emailaliases").select("emailaliases.*")
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
    if(joinDomain)
      p = p.leftJoin("domains", "domains.do_id", "emailaliases.el_domain").select("domains.do_domain_name")

    return p.then(rows=>{

              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })
  }

  function getAliasesCountBy(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    var p = knexPostfix("emailaliases").count().whereRaw(f.sqlFilterStr, f.sqlFilterValues)

    return p.then(rows=>{

          return rows[0]["count(*)"];
      })
  }

  re.RestoreBackup = function(in_data){
     return knexPostfix("emailaliases").insert(in_data).return();
  }

  re.GetAliasesByUserAccountAndWebhosting = function(in_data){
    return vali.async(in_data, app.listAccountWebhostingConstraint)
      .then(d=>{
         var params = {el_user_id: d.account}
         if(d.webhosting)
            params.el_webhosting = d.webhosting
         return getAliasesBy(params, true)
      })
      .then(aliases=>{
         return Promise.resolve(app.groupByWebhosting(aliases, "aliases", "el"))
      })

  }
	re.GetAliases = function(){
		 return getAliasesBy({})
	}
  re.GetAliasesByDomain = function(domain_id){
     return getAliasesBy({el_domain:domain_id})
  }
  re.GetAliasByDomainAndId = function(domain_id, el_id){
     return shouldBeOneRow(getAliasesBy({el_domain:domain_id, el_id:el_id}))
  }
  re.GetAliasById = function(el_id){
     return shouldBeOneRow(getAliasesBy({el_id:el_id}))
  }
  re.GetAliasesByWebhosting = function(webhosting_id){
     return getAliasesBy({el_webhosting:webhosting_id})
  }
  re.GetAliasesCountByWebhosting = function(wh_id){
     return getAliasesCountBy({el_webhosting: wh_id})
  }

  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("ALIAS_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }
  re.GetAliasByName = function(alias){
     return shouldBeOneRow( getAliasesBy({el_alias: alias}) )
  }
	re.Insert = function(in_data, req){

      var account
      var maxAliases
      var d
      return vali.async(in_data, {
        "el_domain": {presence: true},
        "el_user_id": {presence: true, isValidUserId: { returnLookup: true, info: app.MonsterInfoAccount }},
        "el_webhosting": {presence: true},

        "el_verified": {isBooleanLazy:true, default: {value: false}},
        "el_alias": {presence: true, mcEmail: {catchDomainAllowed: true, catchUserAllowed: true}},
        "el_destination": {presence: true, mcEmail: true},
      })
    	.then(ad=>{

          d = ad
          account = d.el_user_id
          d.el_user_id = account.u_id

          return domain.GetAliasesAndLimit()
            .then(re=>{
               if(!re.canBeAdded) throw new MError("ALIAS_LIMIT_IS_REACHED")
            })

      })
       .then(()=>{
          if(d.el_verified) return Promise.resolve("")

          const Token = require("Token")
          return Token.GetTokenAsync(app.config.get("verification_token_length"))
       })
       .then((code)=>{

           d.el_verification_token = code
           d.el_verification_token_expires = app.getExpirationDate()

           return knexPostfix
             .insert(d)
             .into("emailaliases")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                if(ex.code == 'SQLITE_CONSTRAINT')
                   throw new MError("ALREADY_EXISTS")
                throw ex
             })

        })
        .then((id)=>{
           app.InsertEvent(req, {e_event_type: "email-alias-create",
		      e_user_id: d.el_user_id,
              e_other: {el_id:id, el_alias: d.el_alias}}
           )


           if(!d.el_verified) {
               var mailer = app.GetMailer()

               var x = {}
               x.to = d.el_destination
               x.template = "email/verify-alias"
               x.locale = account.u_language
               x.context = {
                user: account,
                el_id: id,
                el_alias: d.el_alias,
                el_destination: d.el_destination,
                el_verification_token: d.el_verification_token,
                el_verification_token_expires: d.el_verification_token_expires
               }

               mailer.SendMailAsync(x)

           }

           return Promise.resolve({el_id:id})
        })

	}

  re.DeleteAliasesByDomain = function(domain_id, in_data,emitter){
      if(emitter)emitter.send_stdout_ln("Deleting aliases...")

      return knexPostfix("emailaliases").whereRaw("el_domain=?",[domain_id]).delete()
        .then(()=>{
          return Promise.resolve()
        })
  }

	return re


    function dbRowToObject(alias){

      if(!alias.el_verified)
         alias.verification_expired = moment.now() > alias.el_verification_token_expires

      alias.VerifyToken = function(in_data) {
        if(alias.el_verified)
           throw new MError("ALREADY_VERIFIED")
        if(alias.verification_expired)
           throw new MError("EXPIRED")
        if(!alias.el_verification_token)
           throw new MError("TOKEN_NOT_CONFIGURED")

        return vali.async(in_data, {token: {presence:true, isString: true}})
          .then(d=>{
              if(d.token != alias.el_verification_token)
                throw new MError("TOKEN_MISMATCH")

              return knexPostfix("emailaliases").whereRaw("el_id=?",[alias.el_id]).update({el_verified: true, el_verification_ts: moment.now()})
                .then(()=>{
                  alias.el_verified = true
                })
          })

      }

      alias.Change = function(in_data){
         return vali.async(in_data, {el_verified: {isBooleanLazy: true}})
           .then(d=>{
              if(!Object.keys(d)) return;

              if(d.el_verified)
                  d.el_verification_ts = moment.now();

              return knexPostfix("emailaliases").whereRaw("el_id=?",[alias.el_id]).update(d);
           })
      }


    	alias.Cleanup = function(){
        delete alias.el_domain
        delete alias.el_verification_token
        delete alias.el_verification_token_expires
        delete alias.el_verification_ts
    		return alias
    	}


      alias.Delete = function(in_data, req){
              return knexPostfix("emailaliases").whereRaw("el_id=?",[alias.el_id]).delete()
                .then(()=>{
                   app.InsertEvent(req, {e_event_type: "email-alias-remove",
           		      e_user_id: alias.el_user_id,
                      e_other: {el_id:alias.el_id, el_alias: alias.el_alias}}
                   )

                  return Promise.resolve()
                })
      }

    	return alias

    }





}
