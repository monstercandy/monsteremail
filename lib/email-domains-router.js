var lib = module.exports = function(app, knexes) {


    var router = app.ExpressPromiseRouter({mergeParams: true})

    var domainsLib = require("lib-domains.js")
    var domains = domainsLib(app, knexes)

    var dkimInstance;


    const knexLib = require("MonsterKnex");

    router.route(/^\/domain\/(.+)\/bcc\/([0-9]+)\/verify/)
      .post(getDomainAndBcc(function(req,res,next){
           return req.sendOk(req.bcc.VerifyToken(req.body.json))
        }))

    router.route(/^\/domain\/(.+)\/bcc\/([0-9]+)/)
      .delete(getDomainAndBcc(function(req,res,next){
           return req.sendOk(
              req.bcc.Delete(null, req)
            )
        }))
      .get(getDomainAndBcc(function(req,res,next){
           return req.sendResponse(req.bcc.Cleanup())
        }))


    router.post(/^\/domain\/(.+)\/bccs\/list/, getDomain(function(req,res,next){
        var searcher = knexLib.searchRoute({
          knex: knexes.postfixDovecot,
          table: "emailbccs",
          defaultQueryFilter: "bc_domain=?",
          defaultQueryFilterParams: req.domain.do_id,
          selectFields: [
            "bc_id", "bc_alias", "bc_destination", "bc_direction", "bc_verified"
          ],
          orderFields: ["bc_id", "bc_alias", "bc_destination"],
          defaultSearchFields: ["bc_alias", "bc_destination"],
          likeConstraints: { 
             "bc_alias": {isString: true},
             "bc_destination": {isString: true},
          },
          equalConstraints: {
             "bc_verified": {isBooleanLazy: true},
             "bc_direction": {isString: true},
             "bc_webhosting": {isString: true},
          }
        });
        return searcher(req);
    }))
    router.get(/^\/domain\/(.+)\/bccs\/limit/, getDomain(function(req,res,next){
           return req.sendPromResultAsIs(req.domain.GetBccsLimit())
    }))

    router.route(/^\/domain\/(.+)\/bccs/)
      .get(getDomain(function(req,res,next){
           return req.sendPromResultAsIs(req.domain.GetBccsAndLimit())
      }))
      .put(getDomain(function(req,res,next){
           return req.sendPromResultAsIs(
              req.domain.InsertBcc(req.body.json, req)
           )

        }))


    router.post(/^\/domain\/(.+)\/account\/([0-9]+)\/salearn/, getDomainAndAccount(function(req,res,next){
         return req.sendTask(req.account.SaLearn(req.body.json))
    }))

    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/cleanup/)
      .post(getDomainAndAccount(function(req,res,next){
         return req.sendTask(req.account.CleanupEmails(req.body.json))
      }))

    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/cluebringer\/quotas/)
      .get(getDomainAndAccount(function(req,res,next){
         return req.sendPromResultAsIs(req.account.GetClueQuotas())
      }))
    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/cluebringer\/sessions/)
      .get(getDomainAndAccount(function(req,res,next){
         return req.sendPromResultAsIs(req.account.GetClueSessions())
      }))


    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/tallies\/set/)
      .post(getDomainAndAccount(function(req,res,next){
         return req.sendOk(req.account.SetTallies(req.body.json))
      }))

    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/tallies\/recalculate/)
      .post(getDomainAndAccount(function(req,res,next){
         return req.sendTask(req.account.RecalculateTallies())
      }))


    router.route(/^\/domain\/(.+)\/account\/([0-9]+)\/whiteblacklist/)
      .get(getDomainAndAccount(function(req,res,next){
         return req.sendPromResultAsIs(req.account.GetWhitelistBlacklistItems())
      }))
      .put(getDomainAndAccount(function(req,res,next){
         return req.sendOk(
            req.account.AddWhitelistBlacklistItem(req.body.json, req)
         )
      }))
      .delete(getDomainAndAccount(function(req,res,next){
         return req.sendOk(
            req.account.DelWhitelistBlacklistItem(req.body.json, req)

         )
      }))


    router.route(/^\/domain\/(.+)\/account\/([0-9]+)/)
      .delete(getDomainAndAccount(function(req,res,next){
           var email = req.account.ea_email

           var emitter = app.commander.EventEmitter()
           return emitter.spawn()
             .then(h=>{
                req.sendTask(Promise.resolve(h))

                return req.account.Delete(req.body.json, emitter, req)
                   .then(()=>{
                       emitter.close()
                   })
                   .catch(ex=>{
                      console.error("Unable to delete account", ex)
                      emitter.close(1)
                   })
              })

        }))
      .post(getDomainAndAccount(function(req,res,next){
           return req.sendOk(
              req.account.Change(req.body.json)
            )
        }))
      .get(getDomainAndAccount(function(req,res,next){
           return getClueBringer().AssignPolicyForAccounts([req.account])
             .then(accounts=>{
                var account = accounts[0]
                return req.sendResponse(account.Cleanup())
             })
        }))

    router.route(/^\/domain\/(.+)\/greylisting/)
      .post(getDomain(function(req,res,next){
           return req.sendOk(
              getClueBringer().DoGreylisting(req.domain.do_domain_name, req.body.json)
              .then(()=>{
                  app.InsertEvent(req, {e_event_type: "greylisting-change", e_other: true});
              })
           )
      }))

    router.post(/^\/domain\/(.+)\/accounts\/list/, getDomain(function(req,res,next){
        var searcher = knexLib.searchRoute({
          knex: knexes.postfixDovecot,
          table: "emailaccounts",
          defaultQueryFilter: "ea_domain=?",
          defaultQueryFilterParams: req.domain.do_id,
          selectFields: [
            "ea_id", "ea_email", "ea_suspended", "ea_quota_bytes", "ea_autoexpunge_days"
          ],
          orderFields: ["ea_id", "ea_email", "ea_quota_bytes"],
          defaultSearchFields: ["ea_email"],
          likeConstraints: { 
             "ea_email": {isString: true},
          },
          equalConstraints: {
             "ea_verified": {isBooleanLazy: true},
             "ea_webhosting": {isString: true},
          }
        });
        return searcher(req);
    }))
    router.get(/^\/domain\/(.+)\/accounts\/limit/, getDomain(function(req,res,next){
        return req.sendPromResultAsIs(req.domain.GetAccountsLimit(true))
    }))


    router.route(/^\/domain\/(.+)\/accounts/)
      .get(getDomain(function(req,res,next){
           var re
           return req.sendPromResultAsIs(
             req.domain.GetAccountsAndLimit()
               .then(are=>{
                  re = are
                  return getClueBringer().AssignPolicyForAccounts(are.accounts)
               })
               .then(()=>{
                  // this one is needed here because the transformation is applied to a subnode
                  return Promise.resolve(re)
               })
           )

        }))
      .put(getDomain(function(req,res,next){

           return req.sendPromResultAsIs(
              req.domain.InsertAccount(req.body.json, req)
           )

        }))




    router.route(/^\/domain\/(.+)\/alias\/([0-9]+)\/verify/)
      .post(getDomainAndAlias(function(req,res,next){
           return req.sendOk(req.alias.VerifyToken(req.body.json))
        }))

    router.route(/^\/domain\/(.+)\/alias\/([0-9]+)/)
      .delete(getDomainAndAlias(function(req,res,next){
           return req.sendOk(
              req.alias.Delete(null, req)
            )
        }))
      .get(getDomainAndAlias(function(req,res,next){
           return req.sendResponse(req.alias.Cleanup())
        }))

    router.post(/^\/domain\/(.+)\/aliases\/list/, getDomain(function(req,res,next){
        var searcher = knexLib.searchRoute({
          knex: knexes.postfixDovecot,
          table: "emailaliases",
          defaultQueryFilter: "el_domain=?",
          defaultQueryFilterParams: req.domain.do_id,
          selectFields: [
            "el_id", "el_alias", "el_destination", "el_verified"
          ],
          orderFields: ["el_id", "el_alias", "el_destination"],
          defaultSearchFields: ["el_alias", "el_destination"],
          likeConstraints: { 
             "el_alias": {isString: true},
             "el_destination": {isString: true},
          },
          equalConstraints: {
             "el_verified": {isBooleanLazy: true},
             "el_webhosting": {isString: true},
          }
        });
        return searcher(req);
    }))
    router.get(/^\/domain\/(.+)\/aliases\/limit/, getDomain(function(req,res,next){
        return req.sendPromResultAsIs(req.domain.GetAliasesLimit(true))
    }))

    router.route(/^\/domain\/(.+)\/aliases/)
      .get(getDomain(function(req,res,next){

           return req.sendPromResultAsIs(req.domain.GetAliasesAndLimit())

        }))
      .put(getDomain(function(req,res,next){

           return req.sendPromResultAsIs(
              req.domain.InsertAlias(req.body.json, req)
           )

        }))

    router.route(/^\/domain\/(.+)\/move/)
      .post(getDomain(function(req,res,next){
          return  req.sendOk(req.domain.Move(req.body.json))
      }))


    router.route(/^\/domain\/(.+)\/?/)
      .search(function(req,res,next){
           var domain
           return domains.GetDomainByName(req.params[0])
             .then(ad=>{
                domain = ad
                return getClueBringer().GetGreylistedDomain(domain.do_domain_name)
             })
             .then(greylisted=>{
                domain.Greylisted = greylisted.length ? true: false

                return req.sendResponse(domain.Cleanup())
             })
             .catch(ex=>{
                console.error("error while querying greyisting status for domain", req.params[0], ex)
                return req.sendResponse(false)
             })

        })
      .get(getDomain(function(req,res,next){
           return  req.sendResponse(req.domain.Cleanup())
        }))
      .post(getDomain(function(req,res,next){
         return  req.sendOk(req.domain.Change(req.body.json))
       }))
      .delete(getDomain(function(req,res,next){
         return req.sendTask(
           app.getEmitter()
           .then(h=>{
              var emitter = h.emitter
              req.domain.Delete(req.body.json, emitter, req)
               .then(()=>{
                  return getClueBringer().DeletePolicyMemberDomain(req.domain.do_domain_name)
               })
               .then(()=>{
                  return getDkim().DeleteDomainDkimKeys(req.domain.do_domain_name)
               })
               .then(()=>{
                  emitter.send_stdout_ln("Ready.")
                  emitter.close()
               })
               .catch(ex=>{
                  console.error("Unable to delete domain", ex)
                  emitter.close(1)
               })

              return Promise.resolve(h)
           })
         )
       }))

    router.post("/list", function(req,res,next){
      return req.sendPromResultAsIs(domains.GetDomainsByUserAccountAndWebhosting(req.body.json))
    })

    router.route("/")
      .put(function(req,res,next){
         if(req.params.webhosting_id)
            req.body.json.do_webhosting = req.params.webhosting_id

         return  req.sendOk(
           domains.Insert(req.body.json, req)
             .then(()=>{
                 // do_domain_name has been validated at the first insert
                 return getClueBringer().InsertInternalDomain(req.body.json.do_domain_name)
             })
         )
      })
      .get(function(req,res,next){
         if(req.params.webhosting_id)
            return req.sendPromResultAsIs(domains.GetDomainsAndCanBeAdded(req.params.webhosting_id))
          
         return domains.GetDomains()
           .then(domains=>{
              return req.sendResponse({domains: domains.Cleanup()})
           })

      })


    return router


    function getDomain(callback) {
       return function(req,res,next){
           req.params.domainname = req.params[0]
           return domains.GetDomainByName(req.params.domainname, req.params.webhosting_id)
             .then(domain=>{
                req.domain = domain
                return callback(req,res,next)
             })
       }
    }

    function getDomainAndAlias(callback){
       return getDomain(function(req,res,next){
           var alias_id = req.params[1]
           return req.domain.GetAliasById(alias_id)
             .then(alias=>{
                req.alias = alias
                return callback(req,res,next)
             })

       })
    }

    function getDomainAndAccount(callback){
       return getDomain(function(req,res,next){
           var alias_id = req.params[1]
           return req.domain.GetAccountById(alias_id)
             .then(account=>{
                req.account = account
                return callback(req,res,next)
             })

       })
    }

    function getDomainAndBcc(callback){
       return getDomain(function(req,res,next){
           var bcc_id = req.params[1]
           return req.domain.GetBccById(bcc_id)
             .then(bcc=>{
                req.bcc = bcc
                return callback(req,res,next)
             })

       })
    }

    function getClueBringer(){
      var cluebringerLib = require("lib-cluebringer.js")
      var cluebringer = cluebringerLib(app, knexes)
      return cluebringer
    }
    function getDkim(){
      if(!dkimInstance) {
        var dkimLib = require("email-dkim-router.js");
        dkimInstance = dkimLib(app, knexes);
      }
      return dkimInstance;
    }

}
