 (function(){
  module.exports = function(app, options){

		if(!options) options = {};

		const MailParser = require('mailparser').MailParser;

		const dotq = require("MonsterDotq");
		const fs = options.fs || dotq.fs();
		const path = require("path");
	    const nodemailer = options.nodemailer || require('nodemailer');

		const accounts = options.accounts || app.getAccountsLib();
		// GetAccountByEmail

		const min_age = app.config.get("mail_slurper_file_min_age_ms");

		const smtp_client_config_base = options.smtp_client_config || app.config.get("smtp_client_config")
		if(!smtp_client_config_base){
			console.error("smtp_client_config not configured, disabling mail slurper support")
			return
		}

	   const this_server = app.config.get("mc_server_name");
	   const service_account_tld = app.config.get("service_account_tld");
	   const service_account_username = app.config.get("service_account_username");


		var re = {};

		re.processDirectory = function(dir){

			return fs.readdirAsync(dir)
			  .then(files=>{
			  	 return dotq.linearMap({catch:true, array: files, action: file=>{
			  	 	if(!file.match(/^[0-9]+$/)) return "unknwon_file";

          	  	 	const fullPath = path.join(dir, file);
			  	 	return re.processFile(fullPath);
			  	 }})
			  })

		}

		re.processFile = function(fullPath){

			const phpHeaderRegexp = new RegExp("^([0-9]+):(.+)$");

			var envelope;

       	 	return fs.statAsync(fullPath)
	  	 	  .then(stat=>{
	  	 	     if(!stat.isFile()) return {status:"not_a_file"};

	  	 	     var timeMs = new Date().getTime();

	  	 	  	 let age_ms = (timeMs - stat.mtime);
	  	 	  	 if(min_age > age_ms) return {status:"too_young"};

	  	 	  	 return new Promise(function(resolve,reject){
		  	 	  	 console.log("parsing email", fullPath);
	         	 	 let parser = new MailParser();
	         	 	 parser.on("headers", headers=>{
	         	 	 	var p = headers.get("x-php-originating-script");
	         	 	 	var aEnvelope = {
						        from: headerMapToAddresses("from"),
						        to: headerMapToAddresses("to"),
						        cc: headerMapToAddresses("cc"),
						        bcc: headerMapToAddresses("bcc"),
	         	 	 		    subject: headers.get("subject"), 
	         	 	 		    php: p
						};
	         	 	 	console.log("email parsed", aEnvelope);

	         	 	 	if(typeof p != "string") return reject(new Error("header_not_present"));
	         	 	 	// X-PHP-Originating-Script
	         	 	 	readFile.close();

	         	 	 	envelope = aEnvelope;

	         	 	 	var m = phpHeaderRegexp.exec(p);
	         	 	 	if(!m) throw new Error("invalid_header");

	         	 	 	var storage = m[1];

	         	 	 	return resolve({status:"send", storage: storage});

	         	 	 	function headerMapToAddresses(key) {
                           return ((headers.get(key)||{}).value || []).map(x => x.address);
	         	 	 	}
	         	 	 });

	         	 	 var readFile = fs.createReadStream(fullPath);
	         	 	 readFile.on("error", reject);
	         	 	 readFile.pipe(parser);

	  	 	  	 })

	  	 	  })
  	 	  	 .then(re=>{
  	 	  	 	if(re.status != "send") return re;

  	 	  	 	var svcEmail = service_account_username+"@"+re.storage+"."+this_server+service_account_tld;
  	 	  	 	
  	 	  	 	return accounts.GetAccountByEmail(svcEmail)
  	 	  	 	  .then((account)=>{

  	 	  	 	     return new Promise((resolve,reject)=>{

              	    	var smtp_client_config = simpleCloneObject(smtp_client_config_base)

	              	    // we are using the hashed password here, since dovecot sasl auth is configured to accept it
	              	    smtp_client_config.auth = {user: svcEmail, pass: account.ea_password}

		                var smtp_client_transporter= nodemailer.createTransport(smtp_client_config);

		                const emailStream = fs.createReadStream(fullPath);

						var message = {
						    envelope: envelope,
						    raw: emailStream
						};

						smtp_client_transporter.sendMail(message, function(err, info){
							if(err) return reject(err);

							console.log("mail relayed successfully", info);
							return resolve();
						})

  	 	  	 	     });

  	 	  	 	  })
  	 	  	 	  .then(()=>{

  	 	  	 	  	return fs.ensureRemovedAsync(fullPath);
  	 	  	 	  })
  	 	  	 	  .then(()=>{
  	 	  	 	     re.status = "sent";
  	 	  	 	     return re;  	 	  	 	  	
  	 	  	 	  })

  	 	  	 })
  	 	      .catch(err=>{
  	 	      	 console.error("error while trying to transmit the slurped email:", fullPath, envelope, err);
 	  	 	     return fs.renameAsync(fullPath, fullPath+".err")
 	  	 	       .then(()=>{
 	  	 	       	  return {status:"error",details: err};
 	  	 	       })
	  	 	  })


		}

		re.installWatcher = function(){
			const directories = app.config.get("mail_slurper_directories");
			if((!directories)||(!directories.length))
				throw new Error("mail_slurper_directories is not configured");

			processNext();

	        function processNext(){
				return dotq.linearMap({catch:true, array: directories, action: re.processDirectory})
				.then(()=>{
	   			   setTimeout(processNext, app.config.get("mail_slurper_interval_ms"))
				})        	
	        }

		}

        return re;

  }
})();
