var lib = module.exports = function(app, knexes) {

    var domainsLib = require("lib-domains.js")
    var domains = domainsLib(app, knexes)

    var router = app.ExpressPromiseRouter({mergeParams: true})
    var dkimKeysDir = app.config.get("amavisd_dkim_keys_path");
    const dotq = require("MonsterDotq");
    var fs = dotq.fs();
    const path = require("path");
    const MError = app.Error;

    var delay = app.config.get("amavisd_drehup_delay_msec");
    var timer;

    const dns = require("dns");
    app.dnsResolveAsync = dotq.single(dns.resolve);

    router.route("/amavisd/rehup")
      .post(function(req,res,next){
         return req.sendOk(rehupAmavisd());
      })

    router.route("/")
      .get(function(req,res,next){
          return fs.readdirAsync(dkimKeysDir)
            .then(files=>{
               var r = /(.+)\.key$/;
               var re = [];
               files.forEach(f=>{
                  var m = r.exec(f);
                  if(m) {
                     re.push(m[1]);                    
                  }
               })
               return req.sendResponse({active_domains: re});
            })
      })

    router.route("/:domainName/")
      .all(function(req,res,next){
         req.pathes = getFilenamesForDkimKeys(req.params.domainName);
         return next();
      })
      .get(function(req,res,next){
          // retrieving the public key

          return req.sendPromResultAsIs(
               getDkimDnsRecords(req.pathes)
               .then(re=>{
                  re.dkim_key_active = true;
                  re.dns_dkim_record_present = false;

                  return app.dnsResolveAsync(re.full_hostname, "TXT")
                      .catch(err=>{
                         console.error("error while resolving", re.full_hostname, err);
                         return [[]];
                      })
                      .then(records=>{
                          console.log("RECORDS", records); // RECORDS [ [ 'v=spf1 a ip4:88.151.102.104/32 ip4:212.47.236.1 ip4:94.130.37.181 ~all' ] ]
                          records = flatten(records);
                          var found = false;
                          records.some(record=>{
                             if(record == re.raw_txt_value){
                                found = true;
                                return true;
                             }
                          })

                          re.dns_dkim_record_present = found;

                          return re;
                      })

               })
               .catch(ex=>{
                  if(ex.code != "ENOENT") throw ex;
                  return {dkim_key_active: false};
               })
            )
      })
      .put(getDomain(function(req,res,next){
          // creating a new key pair and returning the public key

          return req.sendPromResultAsIs(            
             ThrowWhenAlreadyPresent(req.pathes)
             .then(()=>{
                return GenerateRawRSAPrivateKey(req.pathes)              
             })
             .then(()=>{
                return rehupAmavisd();
             })
             .then(()=>{
                return getDkimDnsRecords(req.pathes);
             })
          )
      }))
      .delete(function(req,res,next){
          // removing the stuff

          return req.sendOk(
             DeleteDomainDkimKeys(req.pathes)
          );
      })


    router.DeleteDomainDkimKeys = function(domainName){
         var pathes = getFilenamesForDkimKeys(domainName);
         return DeleteDomainDkimKeys(pathes);
    }


    return router;


    function getDomain(callback) {
       return function(req,res,next){
           return domains.GetDomainByName(req.params.domainName, req.params.webhosting_id)
             .then(domain=>{
                req.domain = domain
                return callback(req,res,next)
             })
       }
    }

    function DeleteDomainDkimKeys(pathes){
       return fs.ensureRemovedAsync(pathes.private)
             .then(()=>{
               return fs.ensureRemovedAsync(pathes.public)
             })
             .then(()=>{
                return rehupAmavisd();
             })
    }

    function getFilenamesForDkimKeys(domainName){
       var re = {
         domainName: domainName,
         private: path.join(dkimKeysDir, domainName+".key"),
         public: path.join(dkimKeysDir, domainName+".pub"),
       };
       return re;
    }

/*
test._domainkey.test.monstermedia.hu.   3600 TXT (
  "v=DKIM1; p="
  "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFoKe1EaAPMxyoHa4AzdU1Qgoh"
  "8xubw/kPLn13M0BCLgVlk8EmUd8sTQ3Ma/+m8wrvFLyGtlNR8cKbmsR5bkmJJK1c"
  "4ERgwVLBCAHhLaElsEDEiQsp3n9GPxwJIFOI5/6fGiCO9ci0vynk93heS0PSsVDM"
  "weC+hEN2jQMCgLMSZQIDAQAB")
*/
    function getDkimDnsRecords(pathes) {
        return fs.readFileAsync(pathes.public, "utf8")
          .then(data=>{
             var rawBase64 = getWrappedTextInside("PUBLIC KEY", data);
             var bindFormattedBase64 = "";
             var lines = rawBase64.split('\n');
             for(var i = 0;i < lines.length;i++){
                  var t = lines[i].trim();
                  if(!t) continue;
                  bindFormattedBase64 += `  "${t}"\n`;
             }
             var rawBase64WithoutWhitespace = rawBase64.replace(/(\r\n\t|\n|\r|\r\t)/gm,"");
             var server = app.config.get("mc_server_name");
             var host_prefix = server+"._domainkey";
             var full_hostname = host_prefix+"."+pathes.domainName;
             var re = {
                "selector": server,
                "host_prefix": host_prefix,
                "full_hostname": full_hostname,
                "full_bind": `${full_hostname}. 3600 TXT (\n  "v=DKIM1; p="\n${bindFormattedBase64}\n)`,
                "raw_txt_value": "v=DKIM1; p="+rawBase64WithoutWhitespace,
             };

             return re;
          })
    }

    function rehupAmavisd() {
       if(timer) {
          console.log("timer already ticking, skipping rehup of this turn");
          return;
       }
       timer = setTimeout(function(){
         timer = null;
         var cmd = app.config.get("cmd_rehup_amavisd_new");
         console.log("Rehuping amavisd-new");
         return app.commander.spawn(cmd).then(h=>{
            return h.executionPromise;
         })
         .catch((ex)=>{
           console.error("Unable to rehup amavisd-new:", ex)
         });

       }, delay);
    }

  function ThrowWhenAlreadyPresent(pathes){
      return ensureFileNotExists(pathes.private)
        .then(()=>{
           return ensureFileNotExists(pathes.public);
        })
  }

  function ensureFileNotExists(filepath){
      return fs.statAsync(filepath)
        .then(()=>{
           throw new MError("DKIM_KEY_ALREADY_EXISTS");
        })
        .catch(ex=>{
           if(ex.code == "ENOENT") return;
           throw ex;
        })
  }

  function GenerateRawRSAPrivateKey(pathes, bitLength){
      bitLength = bitLength || app.config.get("rsa_key_length_bit")
          var cmd = app.config.get("cmd_openssl_genrsa");
          var params = [
            app.config.get("cmd_paths"),
            app.config.get("cmd_params"), 
            pathes, 
            {keylength: bitLength}
          ];
            return app.commander.spawn(cmd, params)
            .then(h=>{
               return h.executionPromise
              .catch(ex=>{
                 console.error("Unable to generate RSA private key:", ex)
                 throw new MError("GENERATE_FAILED")
              })
            })
    }

  function getWrappedTextInside(name, openSslText) {
    var r = new RegExp("-----BEGIN "+name+"-----([\\s\\S]*?)-----END "+name+"-----")
    var m = r.exec(openSslText)
    if(!m) return
    return m[1]+"\n"
  }

  function flatten(arr) {
    if(!Array.isArray(arr)) {
      return [arr];
    }

    var array = [];
    for(var i = 0; i < arr.length; i++) {
      array = array.concat(flatten(arr[i]));
    }
    return array;
  }

}
