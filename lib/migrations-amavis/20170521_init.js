// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.raw("PRAGMA journal_mode=WAL;"),

         knex.raw(`
            CREATE TABLE maddr (
                  id         INTEGER PRIMARY KEY AUTOINCREMENT,
                  partition_tag integer   DEFAULT 0,   -- see $partition_tag
                  email      bytea        NOT NULL,    -- full e-mail address
                  domain     varchar(255) NOT NULL,    -- only domain part of the email address
                                                       -- with subdomain fields in reverse
                  CONSTRAINT part_email UNIQUE (partition_tag,email)
            );
        `),

         knex.raw(`
            CREATE TABLE msgrcpt (
                  partition_tag integer DEFAULT 0,  -- see $partition_tag
                  mail_id    bytea    NOT NULL,     -- (must allow duplicates)
                  rseqnum    integer  DEFAULT 0   NOT NULL, -- recip's enumeration within msg
                  rid        integer  NOT NULL,     -- recipient: maddr.id (duplicates allowed)
                  is_local   char(1)  DEFAULT ' ' NOT NULL, -- recip is: Y=local, N=foreign
                  content    char(1)  DEFAULT ' ' NOT NULL, -- content type V/B/U/S/Y/M/H/O/T/C
                  ds         char(1)  NOT NULL,     -- delivery status: P/R/B/D/T
                                                    -- pass/reject/bounce/discard/tempfail
                  rs         char(1)  NOT NULL,     -- release status: initialized to ' '
                  bl         char(1)  DEFAULT ' ',  -- sender blacklisted by this recip
                  wl         char(1)  DEFAULT ' ',  -- sender whitelisted by this recip
                  bspam_level real,                 -- per-recipient (total) spam level
                  smtp_resp  varchar(255) DEFAULT '', -- SMTP response given to MTA
                  CONSTRAINT msgrcpt_partition_mail_rseq UNIQUE (partition_tag,mail_id,rseqnum),
                  PRIMARY KEY (partition_tag,mail_id,rseqnum)
                --FOREIGN KEY (rid)     REFERENCES maddr(id)     ON DELETE RESTRICT,
                --FOREIGN KEY (mail_id) REFERENCES msgs(mail_id) ON DELETE CASCADE
            );
        `),


         knex.raw(`
            CREATE TABLE msgs (
                  partition_tag integer     DEFAULT 0,  -- see $partition_tag
                  mail_id     bytea         NOT NULL,   -- long-term unique mail id, dflt 12 ch
                  secret_id   bytea         DEFAULT '', -- authorizes release of mail_id, 12 ch
                  am_id       varchar(20)   NOT NULL,   -- id used in the log
                  time_num    integer NOT NULL CHECK (time_num >= 0),
                                                        -- rx_time: seconds since Unix epoch
                  time_iso timestamp WITH TIME ZONE NOT NULL,-- rx_time: ISO8601 UTC ascii time
                  sid         integer NOT NULL CHECK (sid >= 0), -- sender: maddr.id
                  policy      varchar(255)  DEFAULT '', -- policy bank path (like macro %p)
                  client_addr varchar(255)  DEFAULT '', -- SMTP client IP address (IPv4 or v6)
                  size        integer NOT NULL CHECK (size >= 0), -- message size in bytes
                  originating char(1) DEFAULT ' ' NOT NULL,  -- sender from inside or auth'd
                  content     char(1),                   -- content type: V/B/U/S/Y/M/H/O/T/C
                    -- virus/banned/unchecked/spam(kill)/spammy(tag2)/
                    -- /bad-mime/bad-header/oversized/mta-err/clean
                    -- is NULL on partially processed mail
                    -- (prior to 2.7.0 the CC_SPAMMY was logged as 's', now 'Y' is used;
                    --- to avoid a need for case-insenstivity in queries)
                  quar_type  char(1),                   -- quarantined as: ' '/F/Z/B/Q/M/L
                                                        --  none/file/zipfile/bsmtp/sql/
                                                        --  /mailbox(smtp)/mailbox(lmtp)
                  quar_loc   varchar(255)  DEFAULT '',  -- quarantine location (e.g. file)
                  dsn_sent   char(1),                   -- was DSN sent? Y/N/q (q=quenched)
                  spam_level real,                      -- SA spam level (no boosts)
                  message_id varchar(255)  DEFAULT '',  -- mail Message-ID header field
                  from_addr  varchar(255)  DEFAULT '',  -- mail From header field,    UTF8
                  subject    varchar(255)  DEFAULT '',  -- mail Subject header field, UTF8
                  host       varchar(255)  NOT NULL,    -- hostname where amavisd is running
                  CONSTRAINT msgs_partition_mail UNIQUE (partition_tag,mail_id),
                  PRIMARY KEY (partition_tag,mail_id)
                --FOREIGN KEY (sid) REFERENCES maddr(id) ON DELETE RESTRICT
            );
        `),

    ])
    .then(()=>{
      return Promise.all([
         knex.raw(`
            CREATE INDEX msgrcpt_idx_mail_id  ON msgrcpt (mail_id);
        `),
         knex.raw(`
            CREATE INDEX msgrcpt_idx_rid      ON msgrcpt (rid);
        `),

         knex.raw(`
            CREATE INDEX msgs_idx_sid      ON msgs (sid);
        `),
         knex.raw(`
            CREATE INDEX msgs_idx_mess_id  ON msgs (message_id); -- useful with pen pals
        `),
         knex.raw(`
            CREATE INDEX msgs_idx_time_iso ON msgs (time_iso);
        `),
         knex.raw(`
            CREATE INDEX msgs_idx_time_num ON msgs (time_num);   -- optional
        `),
      ])


    })
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('maddr'),
        knex.schema.dropTable('msgrcpt'),
        knex.schema.dropTable('msgs'),
    ])
	
};
