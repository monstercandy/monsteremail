var lib = module.exports = function(app, knexes) {

    var postfixLib = require("lib-postfix.js")

    var postfix = postfixLib(app, knexes)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.post("/flush", function(req,res,next){
       return req.sendTask(postfix.Flush())
    })

    const postfixConfD = app.config.get("postfix_main_cf_d");
    if(postfixConfD) {
        const simpleEditor = require("SimpleEditor")({baseDir: postfixConfD, fileFilter: /\.cf$/, onChange: function(){
            const cmd = app.config.get("cmd_postfix_main_cf_regen");
            if(!cmd) return;
            return app.commander.spawn({chain:cmd})
               .then(h=>{
                  return h.executionPromise
               })
               .catch(ex=>{
                  console.error("Unable to reload Postfix", ex);
               })

        }});
        router.use("/config", simpleEditor.getRouter(app));
    }

    router.get("/mailq/by/domain/:domain", function(req,res,next){
       return req.sendPromResultAsIs(postfix.GetMailqMessagesByDomains([req.params.domain]))
    })
    router.get("/mailq/by/webhosting/:whId", function(req,res,next){
       return req.sendPromResultAsIs(postfix.GetMailqMessagesByWebhosting(req.params.whId))
    })

    router.delete("/mailq/by/sender", function(req,res,next){
       return req.sendTask(postfix.DeleteMessagesBySender(req.body.json))
    })
    router.delete("/mailq/by/recipient", function(req,res,next){
       return req.sendTask(postfix.DeleteMessagesByRecipient(req.body.json))
    })
    router.delete("/mailq/by/email", function(req,res,next){
       return req.sendTask(postfix.DeleteMessagesByEmail(req.body.json))
    })

    router.route("/mailq/:mail_id")
       .get(function(req,res,next){
          return req.sendTask(postfix.FetchMessage(req.params.mail_id))
       })
       .delete(function(req,res,next){
          return req.sendTask(postfix.DeleteMessage(req.params.mail_id))
       })


    router.delete("/mailq/ALL", function(req,res,next){
       return req.sendTask(postfix.DeleteAllMessages())
    })

    router.get("/mailq/", function(req,res,next){
       return req.sendPromResultAsIs(postfix.GetMailqMessages())
    })




    router.post("/mailq-auto/cleanup", function(req,res,next){
       return req.sendTask(postfix.MailqAutoCleanup())
    })
    router.put("/mailq-auto/", function(req,res,next){
       return req.sendPromResultAsIs(postfix.MailqAutoInsert(req.body.json))
    })
    router.get("/mailq-auto/", function(req,res,next){
       return req.sendPromResultAsIs(postfix.MailqAutoFetch())
    })
    router.delete("/mailq-auto/:mc_id", function(req,res,next){
       return req.sendOk(postfix.MailqAutoRemove(req.params.mc_id))
    })

    return router

}
