//permissions needed: ["ADMIN_TALLY","FILEMAN_MAILDIRMAKE","EMIT_LOG","INFO_ACCOUNT","INFO_WEBHOSTING","SEND_EMAIL","SEND_EMAIL_TO_USER"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "email"

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    // note: dovecot tools (eg doveadm) override the default configuration file path
    // if this environment variable is present.
    delete process.env.CONFIG_FILE;

    config.defaults({
      "interval_remove_unverified_entities_ms": 15*60*1000, // every 15 minutes
      "token_code_expiration": 86400,
      "verification_token_length": 8,
      "default_charset": "utf8",

      "mail_log_parser_active": true,
      "mail_log_parser_cleaner_interval_ms": 60000,
      "mail_log_file_path": "/var/log/mail.log",
      "mail_log_loginlog_interval_second": 3600,
      "mail_log_quota_reject_notification_interval_second": 7200,

      "domain_suffixes_whitelisted_from_limit": [],
      "max_domain_per_user_account": 100,
      "max_whitelist_blacklist_items_per_email_account": 100,

      "public_ratelimit_window_sec": 15*60,
      "public_ratelimit_max": 50,

      "smtp_server_port": 25,
      "smtp_server_host": "127.0.0.2",
      "smtp_server_initial_data_limit_bytes": 10*1024,

      "salearner_watch_delay": 3000,

      "amavis_cleanup_db_older_than_days": 7,
      "amavis_cleanup_db_empty_older_than_hours": 1,
      "amavis_cleanup_quarantine_older_than_days": 30,

      "cron_mailq_query_notify_limit": 200,
      "cron_mailq_auto_cleanup": "*/5 * * * *",
      "cron_cluebringer_auto_cleanup": "0 3 * * *",
      "cron_spamassassin_auto": "0 3 * * *",

      "service_account_username": "mailer",
      "service_account_tld": ".svc",

      "mail_slurper_interval_ms": 5000,
      "mail_slurper_file_min_age_ms": 3000,

      "openssl_path": '/usr/bin/openssl',

      "amavisd_dkim_keys_path": "/var/lib/monster/email/dkim-keys",

      "cmd_rehup_amavisd_new": {
        removeImmediately: true, executeImmediately:true, dontLog_stdout: true, 
        omitControlMessages:true,omitAggregatedStderr: true,
        chain: [{"executable": "/usr/local/sbin/drehup", args:["stretch-amavisd-new"]}]
      },
      "cmd_paths": {
          "openssl_path": 'openssl',
          "chmod_path": "chmod",
          "chown_path": "chown",
      },
      "cmd_params": {},
      "rsa_key_length_bit": 1024,
      "cmd_openssl_genrsa": {
        removeImmediately: true, executeImmediately:true, dontLog_stdout: true, 
        omitControlMessages:true,omitAggregatedStderr: true,
        chain: [
          {"executable": "[openssl_path]", args:["genrsa", "-out", "[private]", "[keylength]"]},
          {"executable": "[chmod_path]", args:["0640","[private]"]},
          // this service is running as group postfix, just like all the other email related services
          // so this chown is not needed
          // {"executable": "[chown_path]", args:["[dkim_keys_user]:[dkim_keys_group]","[private]"]},
          {"executable": "[openssl_path]", args:["rsa", "-in", "[private]", "-out", "[public]", "-pubout"]},
        ]
      },

      "mailbox_group_owner": "vmail",

      "amavisd_drehup_delay_msec": 5000,
      "postfix_main_cf_d": "/etc/postfix/main.cf.d",
      "cmd_postfix_main_cf_regen": {"executable": "/usr/local/bin/postfix-main-conf-rebuild.sh", args:["1"]},

      "cmd_spamassassin_update": {
          chain: [
             {"executable": "/bin/bash",
               args:[
                 "-c",
                 '/usr/local/sbin/dexec stretch-amavisd-new /usr/bin/sa-update --gpghomedir /tmp && drehup stretch-amavisd-new; true'
               ]
             },
             {"executable": "/usr/bin/sa-learn", args:["--force-expire"]}
          ]
      },

      "cmd_cluebringer_cleanup": {"executable": "/usr/local/sbin/dexec", args:["stretch-cluebringer","/usr/local/bin/cbpadmin","--cleanup"]},
      "cmd_postfix_mailq": {"executable": "/usr/sbin/postqueue", args:["-p"]},
      "cmd_postfix_flush": {"executable": "/usr/sbin/postqueue", args:["-f"]},
      "cmd_postfix_fetch_message": {"executable": "/usr/sbin/postcat", args: ["-vq", "[mail_id]"]},
      "cmd_postfix_remove_message": {"executable": "/usr/sbin/postsuper", args: ["-d", "[mail_id]"]},
      "cmd_postfix_remove_all": {"executable": "/usr/sbin/postsuper", args: ["-d", "ALL"]},
      "cmd_salearn_stdin": {"executable": "[salearn_path]", args:["--[spam_or_ham]", "--username=[email]"]},
      "cmd_salearn": {"executable": "[salearn_path]", args:["--[spam_or_ham]", "--username=[email]", "[filename]"]},
      "cmd_amavis_cat": {"executable":"/bin/cat","args":["[filename]"]},
      "cmd_doveadm_cleanup_junk": {"executable": "[doveadm_path]", "args":["expunge","-A","mailbox","INBOX.Junk","SENTBEFORE","[cleanup_junk_older_than_days]d"]},
      "cmd_doveadm_expunge": {"executable": "[doveadm_path]", "args":["-v","expunge","-u","[ea_email]","mailbox","INBOX","SENTBEFORE","[older_than_days]d"]},
      "cmd_doveadm_quota_recalc": {"executable": "[doveadm_path]", "args":["quota","recalc","-u","[ea_email]"]},

      "cmd_common_params": {
        "cleanup_junk_older_than_days": 30,
      },
    })

    Array("mc_server_name").forEach(n=>{
        if(!config.get(n))
          throw new Error("Mandatory option is missing: "+n)
    })
    

    config.appRestPrefix = "/email"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    const dbKeys = ["db-postfix-dovecot", "db-cluebringer", "db-amavis", "db-dovecot-dict"]

    var knexes = {}
    const KnexLib = require("MonsterKnex")
    knexes.postfixDovecot = KnexLib(config, dbKeys[0])
    knexes.cluebringer = KnexLib(config, dbKeys[1])
    knexes.amavis = KnexLib(config, dbKeys[2])
    knexes.dovecotDict = KnexLib(config, dbKeys[3])

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    // external service providers will call this route
    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    const MonsterInfoLib = require("MonsterInfo")

    // for fileman we do NOT expect any valid internalapi secrets as we gonna call only the public tasks part of it
    app.ServersFileman = router.ServersMiddleware("fileman", 0)

    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})
    app.MonsterInfoAccount = MonsterInfoLib.account({config: app.config, accountServerRouter: app.ServersRelayer})

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)


    router.use("/webhostings/list", require("email-webhostings-list-router.js")(app, knexes))
    router.use("/webhostings/:webhosting_id", require("email-webhostings-router.js")(app, knexes))
    router.use("/domains", require("email-domains-router.js")(app, knexes))
    router.use("/aliases", require("email-aliases-router.js")(app, knexes))
    router.use("/accounts",require("email-accounts-router.js")(app, knexes))
    router.use("/bccs",require("email-bccs-router.js")(app, knexes))
    router.use("/pub",require("email-pub-router.js")(app, knexes))
    router.use("/postfix",require("email-postfix-router.js")(app, knexes))
    router.use("/amavis",require("email-amavis-router.js")(app, knexes))
    router.use("/cluebringer",require("email-cluebringer-router.js")(app, knexes))
    router.use("/dkim",require("email-dkim-router.js")(app, knexes))

    require("Wie")(app, router, require("lib-wie.js")(app, knexes));

    require("email-smtp-server.js")(app, knexes)

    require("lib-salearner.js")(app)

    app.listAccountWebhostingConstraint = {
      account:{presence:true, isString:true},
      webhosting: {isInteger: true}
    }

    app.Prepare = function() {
       return knexes.postfixDovecot.migrate.latest({
          directory: "migrations-postfix-dovecot",
          disableTransactions: true, // transactions are turned off in order to be able to turn on WAL mode
        })
         .then(()=>{
            return knexes.cluebringer.migrate.latest({
              directory: "migrations-cluebringer",
              disableTransactions: true, // transactions are turned off in order to be able to turn on WAL mode
            })
         })
         .then(()=>{
            return knexes.amavis.migrate.latest({
              directory: "migrations-amavis",
              disableTransactions: true, // transactions are turned off in order to be able to turn on WAL mode
            })
         })
         .then(()=>{
            return knexes.dovecotDict.migrate.latest({
              directory: "migrations-dovecot-dict",
              disableTransactions: true, // transactions are turned off in order to be able to turn on WAL mode
            })
         })
    }

    app.Cleanup = function() {

       var ps = []

       dbKeys.forEach(key=>{
          var x = config.get(key)
          if(!x)return
          var fn = x.connection.filename
          if(!fn) return

          ps.push(fs.unlinkAsync(fn).catch(x=>{})) // the database might not exists, which is not an error in this case
       })

      var patternsToDel = []
      patternsToDel.push(path.join(config.get("amavisd_dkim_keys_path"), "*.key"))
      patternsToDel.push(path.join(config.get("amavisd_dkim_keys_path"), "*.pub"))


      const del = require('MonsterDel');
      // console.log("xxx",patternsToDel)
      ps.push(del(patternsToDel,{"force": true}))


       return Promise.all(ps)

    }


    app.getExpirationDate = function(){
      const moment = require("MonsterMoment")
      return moment().add(app.config.get("token_code_expiration"),"second").toISOString()
    }


  app.getEmitter = function() {
         var emitter = app.commander.EventEmitter()
         return emitter.spawn()
           .then(h=>{
              h.emitter = emitter
              return Promise.resolve(h)
           })

  }

  app.groupByWebhosting = function(data, name, prefix){
     // console.log("groupby", data,name, webhostingKey)
     var re = {}
     re[name] = {}
     var webhostingKey = prefix + "_webhosting"
     var domainKey = (prefix == "do" ? "xxxxx" : "do_domain_name")
     data.Cleanup().forEach(row=>{
        var arr
        var wh_id = row[webhostingKey]
        var domain = row[domainKey]
        if(domain) {
          if(!re[name][wh_id])
            re[name][wh_id] = {}
          if(!re[name][wh_id][domain])
            re[name][wh_id][domain] = []
          arr = re[name][wh_id][domain]
        } else {
          if(!re[name][wh_id])
            re[name][wh_id] = []
          arr = re[name][wh_id]
        }

        arr.push(row)
     })

     return re
  }


  app.GetWebhostingMapiPool = app.GetRelayerMapiPool;


    app.getSaLearner = function(){
       const salib = require("lib-salearner.js")
       return salib(app)
    }

    app.getAccountsLib = function(){
        return require("lib-accounts.js")(app, knexes);
    }


    if(app.config.get("mail_slurper_active")) {
      const slurperLib = require('lib-mail-slurper.js');
      var slurper = slurperLib(app);
      if(slurper) slurper.installWatcher();

    }

    if(app.config.get("mail_log_parser_active")) {
      const mailLogParserLib = require('lib-mail-log-parser.js');
      var mailLogParser = mailLogParserLib(app);
      if(mailLogParser) mailLogParser.Start();

    }

    return app


}
