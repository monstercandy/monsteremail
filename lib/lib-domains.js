var lib = module.exports = function(app, knexes){

    var knexPostfix = knexes.postfixDovecot

    const knexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    const dotq = require("MonsterDotq");
    var fs = dotq.fs();

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()

    var Common = ValidatorsLib.array()

	var re = {}

  function getDomainsByRaw(sqlFilterStr, sqlFilterValues) {
    // console.log("getdomains", sqlFilterStr, sqlFilterValues)

    return knexPostfix("domains").select()
      .whereRaw(sqlFilterStr, sqlFilterValues)
      .then(rows=>{

              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })
  }
  function getDomainsBy(filterHash) {
    // console.log("getdomains", filterHash)
    var f = knexLib.filterHash(filterHash)
    return getDomainsByRaw(f.sqlFilterStr, f.sqlFilterValues)
  }

  re.GetDomainsAndCanBeAdded = function(webhosting_id){

      var maxDomains;
      var a = {};
      return app.MonsterInfoWebhosting.GetInfo(webhosting_id)
      .then(webhosting=>{
          maxDomains = webhosting.template.t_domains_max_number_attachable;

          if(webhosting.wh_is_expired)
             a.canBeAdded = false;

          return re.GetDomainsByWebhosting(webhosting_id)
      })
      .then(domains=>{

          a.domains = domains;

          if(typeof a.canBeAdded != "undefined")
             return a;

          var whitelist = app.config.get("domain_suffixes_whitelisted_from_limit")
          var whitelisted = []

          domains.forEach(domain=>{
              var go = false
              whitelist.some(wl=>{
                 if(domain.do_domain_name.endsWith(wl)){
                    go = true
                    return true
                 }
              })
              if(go)
                whitelisted.push(domain)
          })

          a.canBeAdded = (domains.length - whitelisted.length >= maxDomains) ? false : true;

          return a;
      })    

  }

	re.GetDomains = function(optional_webhosting_id){
     if(optional_webhosting_id) return re.GetDomainsByWebhosting(optional_webhosting_id)

		 return getDomainsBy({})
	}
  re.GetDomainsByWebhosting = function(webhosting_id){
     return getDomainsBy({do_webhosting:webhosting_id})
  }
  re.GetDomainsByAccount = function(account_id){
     return getDomainsByRaw("do_user_ids LIKE ?", ["% "+account_id+" %"])
  }
  re.GetDomainsByUserAccountAndWebhosting = function(in_data){
    return vali.async(in_data, app.listAccountWebhostingConstraint)
      .then(d=>{
         var sql = "do_user_ids LIKE ?"
         var params = ["% "+d.account+" %"]

         if(d.webhosting) {
            sql+= "AND do_webhosting=?"
            params.push(d.webhosting)
         }
         return getDomainsByRaw(sql, params)
      })
      .then(domains=>{
         return Promise.resolve(app.groupByWebhosting(domains, "domains", "do"))
      })

  }

  re.ChangeByWh = function(whId, in_data) {
      return vali.async(in_data, {
            "do_active": {isBooleanLazy: true},
            "do_suspended": {isBooleanLazy: true},
        })
      .then(d=>{
        return knexes.postfixDovecot("domains").update(d).whereRaw("do_webhosting=?", [whId])
      })

  }

  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("DOMAIN_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }
  re.GetDomainById = function(domain_id){
     return shouldBeOneRow( getDomainsBy({do_id: domain_id}) )
  }
  re.GetDomainByName = function(domain_name, optional_webhosting_id){
     var filterHash = {do_domain_name: domain_name}
     if(optional_webhosting_id)
       filterHash.do_webhosting = optional_webhosting_id
     return shouldBeOneRow( getDomainsBy(filterHash) )
  }
	re.Insert = function(in_data, req){

      var maxDomains
      var d
      return vali.async(in_data, {
        "account": {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
        "do_domain_name": {presence: true, isDomain: true},
        "do_webhosting": {presence: true, isInteger: {lazy:true, returnInt: true}, default: {value: 0}},
        "do_active": {isBooleanLazy: true, default: true},
      })
    	.then(ad=>{

          d = ad
          d.do_domain_name = d.do_domain_name.d_domain_canonical_name

          var svcTld = app.config.get("service_account_tld");
          var isServiceDomain = d.do_domain_name.endsWith(svcTld);
          if(isServiceDomain) {
             if(!d.do_webhosting)
               throw new MError("WEBSTORE_NOT_SPECIFIED");
             if(!d.do_domain_name.startsWith(d.do_webhosting+"."))
               throw new MError("WEBSTORE_MISMATCH");
          }


          return knexPostfix.transaction(trx=>{

              var subKnexes = simpleCloneObject(knexes)
              subKnexes.postfixDovecot= trx
              var domainsLib = lib(app, subKnexes)

              return domainsLib.GetDomainByName(d.do_domain_name)
                .catch(ex=>{
                    if(ex.message != "DOMAIN_NOT_FOUND") throw ex
                    console.log("domain not yet found, meaning this is a new one")
                    return;
                })
                .then(domain=>{

                  if(!domain) {

                     var p

                      if(!d.do_webhosting) {
                          console.log("no webhosting, so checking how many domains the target account has", d.account)

                          p = domainsLib.GetDomainsByAccount(d.account)
                            .then(domains=>{
                                var max = app.config.get("max_domain_per_user_account")

                                console.log("target account has", domains.length, max)

                                if(domains.length >= max)
                                   throw new MError("DOMAIN_LIMIT_IS_REACHED")

                                return Promise.resolve()
                            })
                      }
                      else
                        p = verifyWebhosting()

                     return p.then(()=>{

                        d.do_user_ids = Common.ForceEnumerableToString([d.account])
                        delete d.account

                        return trx
                         .insert(d)
                         .into("domains")
                      })
                      .then(ids=>{
                        return Promise.resolve(ids[0])
                      })
                      .then((id)=>{
                         app.InsertEvent(req, {e_event_type: "email-domain-create",
                            e_user_id: d.account,
                            e_other: {do_id:id, do_domain_name: d.do_domain_name}}
                         )

                         return Promise.resolve(id)
                      })

                  }
                  else
                  {
                     var p = Promise.resolve()
                     if((domain.do_webhosting)&&(domain.do_webhosting != d.do_webhosting)&&(d.do_webhosting)) {
                       throw new MError("DOMAIN_IS_ALREADY_ATTACHED_TO_WEBHOSTING")
                     }
                     else
                     if((!domain.do_webhosting)&&(d.do_webhosting)) {
                        p = verifyWebhosting()
                     }

                     return p.then(()=>{
                         if(domain.do_user_ids.indexOf(d.account) < 0)
                             domain.do_user_ids.push(d.account)

                         d.do_user_ids = Common.ForceEnumerableToString(domain.do_user_ids)


                         var toUpdate = {
                             do_user_ids: d.do_user_ids,
                             do_active: d.do_active,
                           }

                         if(d.do_webhosting)
                            toUpdate.do_webhosting = d.do_webhosting

                         return trx("domains")
                           .update(toUpdate)
                           .whereRaw('do_id=?', [domain.do_id])


                     })
                     .then(ids=>{
                        return Promise.resolve(domain.do_id)
                     })

                   }


                })



                function verifyWebhosting(){

                    return app.MonsterInfoWebhosting.GetInfo(d.do_webhosting)
                    .then(webhosting=>{
                        maxDomains = webhosting.template.t_domains_max_number_attachable;

                        if(webhosting.wh_is_expired)
                           throw new MError("WEBHOSTING_HAS_EXPIRED");

                        return domainsLib.GetDomainsByWebhosting(d.do_webhosting)
                    })
                    .then(domains=>{
                        var whitelist = app.config.get("domain_suffixes_whitelisted_from_limit")
                        var whitelisted = []
                        domains.push({do_domain_name: d.do_domain_name})

                        domains.forEach(domain=>{
                            var go = false
                            whitelist.some(wl=>{
                               if(domain.do_domain_name.endsWith(wl)){
                                  go = true
                                  return true
                               }
                            })
                            if(go)
                              whitelisted.push(domain)
                        })



                        if(domains.length - whitelisted.length > maxDomains) throw new MError("DOMAIN_LIMIT_IS_REACHED")

                        return Promise.resolve()
                    })

                }

            })


          })




	}

	return re


    function dbRowToObject(domain){

        domain.do_user_ids = Common.ForceEnumerableToList(domain.do_user_ids)


    	domain.Cleanup = function(){
    		delete domain.do_id
    		return domain
    	}

      domain.Change = function(in_data){
          return vali.async(in_data, {
            "do_webhosting": {isInteger: {lazy:true}},
            "do_active": {isBooleanLazy: true},
            "do_suspended": {isBooleanLazy: true},
          })
          .then(d=>{

              if(
                 (typeof d.do_webhosting != "undefined") &&
                 (domain.do_webhosting) &&
                 (d.do_webhosting != domain.do_webhosting)
                )
              {
                throw new MError("DOMAIN_IS_ALREADY_ATTACHED_TO_WEBHOSTING");
              }

              return knexPostfix('domains')
                    .whereRaw('do_domain_name=?', [domain.do_domain_name])
                    // NOTE: do_active is flipped globally
                    //.whereRaw('do_id=?', [domain.do_id])
                    .update(d)
                    .then(()=>{
                       if(typeof d.do_active != "undefined")
                         domain.do_active = d.do_active;
                       if(d.do_webhosting)
                         domain.do_webhosting = domain.do_webhosting;

                       return Promise.resolve() // good!
                    })

          })
      }

      function getEndsWith(){
         return "@"+domain.do_domain_name
      }

      function InsertCommon(in_data, prefix){
        prefix = prefix + "_"
        in_data[prefix+"user_id"] = in_data.account
        in_data[prefix+"domain"] = domain.do_id
        in_data[prefix+"webhosting"] = domain.do_webhosting
        // console.log(in_data); process.reallyExit()
        return getEndsWith()
      }

      domain.InsertBcc = function(in_data, req){
         const ends = InsertCommon(in_data, "bc")

         if((in_data.bc_destination)&&(in_data.bc_destination.endsWith(ends)))
           in_data.bc_verified = true

         return domain.GetAccounts()
           .then(accounts=>{
              var emailAddresses = accounts.map(function(x){ return x.ea_email; })
              //console.log("shoudl be one of", emailAddresses)

              return vali.async(in_data, {bc_alias:{presence:true, inclusion:emailAddresses}})
           })
           .then(()=>{
               return getBccsLib().Insert(in_data, req)
           })
      }

      domain.InsertAlias = function(in_data){
         const ends = InsertCommon(in_data, "el")

         if((in_data.el_destination)&&(in_data.el_destination.endsWith(ends)))
           in_data.el_verified = true

         var params = (in_data.el_alias && in_data.el_alias.indexOf("@") > -1) ? {endsWith:ends} : {}
         return vali.async(in_data, {el_alias:{presence:true, isString:params}})
           .then(()=>{
               return getAliasesLib().Insert(in_data)
           })
      }
      domain.InsertAccount = function(in_data, req){
         const ends = InsertCommon(in_data, "ea")

         return vali.async(in_data, {ea_email:{presence:true, isString:{endsWith:ends}}})
           .then(()=>{
               return getAccountsLib().Insert(in_data, domain, req)
           })
      }

      domain.GetAliasById = function(el_id){
         return getAliasesLib().GetAliasByDomainAndId(domain.do_id, el_id)
      }
      domain.GetAccountById = function(el_id){
         return getAccountsLib().GetAccountByDomainAndId(domain.do_id, el_id)
      }
      domain.GetBccById = function(bc_id){
         return getBccsLib().GetBccByDomainAndId(domain.do_id, bc_id)
      }
      domain.GetAliases = function(){
         return getAliasesLib().GetAliasesByDomain(domain.do_id)
      }
      domain.GetAliasesOnWebstore = function(){
         return getAliasesLib().GetAliasesByWebhosting(domain.do_webhosting);
      }
      domain.GetAliasesCountOnWebstore = function(){
         return getAliasesLib().GetAliasesCountByWebhosting(domain.do_webhosting);
      }


      domain.GetBccs = function(){
         return getBccsLib().GetBccsByDomain(domain.do_id)
      }
      domain.GetBccsOnWebstore = function(){
         return getBccsLib().GetBccsByWebhosting(domain.do_webhosting);
      }
      domain.GetBccsCountOnWebstore = function(){
         return getBccsLib().GetBccsCountByWebhosting(domain.do_webhosting);
      }

      domain.GetBccsByName = function(email){
         var ends = getEndsWith()
         return vali.async({email:email}, {email:{presence:true, isString:{endsWith:ends}}})
           .then(()=>{
              return getBccsLib().GetBccsByName(email)
           })
      }
      domain.GetAccounts = function(){
         return getAccountsLib().GetAccountsByDomain(domain.do_id)
      }
      domain.GetAccountsOnWebstore = function(){
         return getAccountsLib().GetAccountsByWebhosting(domain.do_webhosting);
      }
      domain.GetAccountsCountOnWebstore = function(){
         return getAccountsLib().GetAccountsCountByWebhosting(domain.do_webhosting);
      }

      domain.Move = function(in_data){
           var d;
           var oldAccounts;
           var newAccounts;
           return vali.async(in_data, {
              dest_u_id: {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
              dest_wh_id: {presence: true, isInteger: {lazy:true}},
           })
           .then(ad=>{
              d = ad;
              return domain.GetAccounts();
           })
           .then((accounts)=>{
              oldAccounts = buildAccountIdTable(accounts);

              return knexes.postfixDovecot("emailbccs").update({bc_user_id:d.dest_u_id,bc_webhosting:d.dest_wh_id}).whereRaw("bc_domain=?", [domain.do_id])
           })
           .then(()=>{

              return knexes.postfixDovecot("emailaliases").update({el_user_id:d.dest_u_id,el_webhosting:d.dest_wh_id}).whereRaw("el_domain=?", [domain.do_id])
           })
           .then(()=>{
              return knexes.postfixDovecot.raw('UPDATE emailaccounts SET ea_user_id=?, ea_webhosting=?, ea_maildir_path=REPLACE(ea_maildir_path,?,?) WHERE ea_domain=?', 
                [
                  d.dest_u_id,
                  d.dest_wh_id,
                  '/'+domain.do_webhosting+'/',
                  '/'+d.dest_wh_id+'/',
                  domain.do_id
                ]);
           })
           .then(()=>{
              var userIdsStr = " "+d.dest_u_id+" ";
              return knexes.postfixDovecot("domains").update({do_user_ids:userIdsStr,do_webhosting:d.dest_wh_id}).whereRaw("do_id=?", [domain.do_id])
           })
           .then(()=>{
              return domain.GetAccounts();
           })
           .then((accounts)=>{
              newAccounts = buildAccountIdTable(accounts);


              return dotq.linearMap({array: Object.keys(oldAccounts), action: function(ea_id){
                  var oldAccount = oldAccounts[ea_id];
                  var newAccount = newAccounts[ea_id];

                  var chain = {executable: "/bin/chown", args: ["-R","--from="+oldAccount.ea_webhosting,newAccount.ea_webhosting, oldAccount.ea_maildir_path]}
                  return app.commander.spawn({chain: chain})
                    .then((h)=>{
                       return h.executionPromise;
                    })
                    .then(()=>{
                       return fs.renameAsync(oldAccount.ea_maildir_path, newAccount.ea_maildir_path)
                        .catch(ex=>{
                            if(in_data.unit_test) return;
                            throw ex;
                        })
                    })

              }})


           })

           function buildAccountIdTable(acs) {
               var re = {};
               acs.forEach(ac=>{
                  re[ac.ea_id] = ac;
               })

               return re;
           }

      }

      function lookupCommon(prom, keyToStore, keyToCompare, returnWebhosting, filter){
         var re = {}
         var allEntitiesCount;
         return prom
           .then(a=>{
              if(keyToStore){
                  var allEntities = a;
                  re[keyToStore] = [];

                  // console.log("before here, allEntities", allEntities, filter, domain.do_id);
                  allEntities.forEach(entity=>{
                     if(entity[filter] == domain.do_id) {
                       re[keyToStore].push(entity.Cleanup())
                     }
                  })

                  allEntitiesCount = allEntities.length;
              } 
              else {
                  re.count = allEntitiesCount = a;
              }

              if(!domain.do_webhosting)
                 throw new MError("DOMAIN_NOT_ATTACHED_TO_WEBHOSTING")

              return app.MonsterInfoWebhosting.GetInfo(domain.do_webhosting)
           })
           .then(webhosting=>{

              var maxKey = keyToStore ? "max_"+keyToStore : "max";
              re[maxKey] = webhosting.template[keyToCompare]
              re.canBeAdded = (re[maxKey] > allEntitiesCount)
              if(returnWebhosting) re.webhosting = webhosting

              return re;
           })
      }
      domain.GetAccountsLimit = function(returnWebhosting) {
         return lookupCommon(domain.GetAccountsCountOnWebstore(), null, "t_email_max_number_of_accounts", returnWebhosting, "ea_domain")
      }
      domain.GetAccountsAndLimit = function(returnWebhosting) {
         return lookupCommon(domain.GetAccountsOnWebstore(), "accounts", "t_email_max_number_of_accounts", returnWebhosting, "ea_domain")
      }

      domain.GetBccsLimit = function(returnWebhosting) {
        return lookupCommon(domain.GetBccsCountOnWebstore(), null, "t_email_max_number_of_bccs", returnWebhosting, "bc_domain")
      }      
      domain.GetBccsAndLimit = function(returnWebhosting) {
         return lookupCommon(domain.GetBccsOnWebstore(), "bccs", "t_email_max_number_of_bccs", returnWebhosting, "bc_domain")
      }

      domain.GetAliasesLimit = function(returnWebhosting) {
         return lookupCommon(domain.GetAliasesCountOnWebstore(), null, "t_email_max_number_of_aliases", returnWebhosting, "el_domain")
      }
      domain.GetAliasesAndLimit = function(returnWebhosting) {
         return lookupCommon(domain.GetAliasesOnWebstore(), "aliases", "t_email_max_number_of_aliases", returnWebhosting, "el_domain")
      }


      domain.Delete = function(in_data, emitter, req){
         return getAliasesLib().DeleteAliasesByDomain(domain.do_id, in_data, emitter)
           .then(()=>{
              return getBccsLib().DeleteBccsByDomain(domain.do_id, in_data,emitter)
           })
           .then(()=>{
              return getAccountsLib().DeleteAccountsByDomain(domain.do_id, in_data,emitter, req)
           })
           .then(()=>{
              if(emitter)emitter.send_stdout_ln("Deleting domain...")

              return knexPostfix("domains").whereRaw("do_id=?",[domain.do_id]).delete()
           })
           .then(()=>{
              app.InsertEvent(req, {e_event_type: "email-domain-remove",
                e_other: {do_id:domain.do_id, do_domain_name: domain.do_domain_name}}
              )

             return Promise.resolve()
           })
      }

    	return domain

      function getAliasesLib(){
        return require("lib-aliases.js")(app, knexes, domain)
      }

      function getBccsLib(){
        return require("lib-bccs.js")(app, knexes, domain)
      }

      function getAccountsLib(){
        return require("lib-accounts.js")(app, knexes, domain)
      }

    }



}
