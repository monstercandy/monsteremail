var lib = module.exports = function(app, knexes){

  var knex= knexes.postfixDovecot

    const MError = require("MonsterError")
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

	var re = {}

  // we make this function public so it can be verified by unit tests
  re.ParseMailq = function(text){
    // /<p>Can[^]*?<\/p>/;

      var re = []
      //
      var myRegexp = /[^]*?([A-F0-9]{8,12})(\*?)\s+(\d+)\s+(\w{3} \w{3} +\d+ \d+:\d+:\d+)\s+(.+)[^]*?\s*(.+)[^]*?\s+(.+)/g
      var match = myRegexp.exec(text);
      while (match != null) {
        var a = {mail_id: match[1], starred: match[2] ? true : false, size: parseInt(match[3],10), ts: match[4], sender: match[5], status: match[6], recipient: match[7]}
        re.push(a)
        match = myRegexp.exec(text);
      }
      return re
  }

  re.GetMailqMessagesByDomains = function(domains){
     return re.GetMailqMessages()
       .then(msgs=>{
          var re = [];
          msgs.forEach(msg=>{
             //sender
             if(senderOrRecipientAmongDomains(msg)) {
                re.push(msg);
             }
          })
          return re;
       })

       function senderOrRecipientAmongDomains(msg) {
          var re = false;
          domains.some(dom=>{
              var atDom = "@"+dom;
              if(
                (msg.recipient.endsWith(atDom))
                ||
                (msg.sender.endsWith(atDom))
                ) {
                 re = true;
                 return true;
              }
          })
          return re;
       }
  }

  re.GetMailqMessagesByWebhosting = function(whId){
     return knexes.postfixDovecot("domains").select().whereRaw("do_webhosting=?",[whId])
       .then(rows=>{
          var doms = rows.map(x => x.do_domain_name);
          return re.GetMailqMessagesByDomains(doms);
       })
  }

  re.GetMailqMessages = function(){
     var cmd_postfix_mailq = app.config.get("cmd_postfix_mailq")
     return app.commander.spawn({dontLog_stdout: true, removeImmediately: true, chain:cmd_postfix_mailq})
       .then(h=>{
          return h.executionPromise
       })
       .then(task=>{
          return re.ParseMailq(task.output);
       })
  }


  re.Flush = function(){
     var cmd_postfix_flush = app.config.get("cmd_postfix_flush")
     return app.commander.spawn({chain:cmd_postfix_flush})
  }

  re.DeleteAllMessages = function(){
     var cmd_postfix_remove_all = app.config.get("cmd_postfix_remove_all")
     return app.commander.spawn({chain:cmd_postfix_remove_all})
  }

  re.DeleteMessagesByEmail = function(in_data){
     return valiEmailArray(in_data)
       .then(d=>{
         return deleteMessagesByEmail(d.emails, d.emails)
      })
  }

  re.DeleteMessagesByRecipient = function(in_data){
     return valiEmailArray(in_data)
       .then(d=>{
          return deleteMessagesByEmail([], d.emails)
      })
  }

  re.DeleteMessagesBySender = function(in_data){
     return valiEmailArray(in_data)
       .then(d=>{
          return deleteMessagesByEmail(d.emails, [])
      })
  }

  function valiEmailArray(in_data) {
     return vali.async(in_data, {emails: {presence:true, isEmailArray:true}})
  }

  function deleteMessagesByEmail(senderMatch, recipientMatch){
     return re.GetMailqMessages()
       .then(messages=>{
          var mailNotifyLimit = app.config.get("cron_mailq_query_notify_limit");
          if((mailNotifyLimit > 0)&& (messages.length >= mailNotifyLimit)) {
            app.EscalateEvent("MAILQ_HEAVY_LOAD", {messages: messages.length});
          }

          // console.log(d, senderMatch, recipientMatch)
          var matches = []
          // console.log(messages)
          messages.forEach(m=>{
             if((senderMatch.indexOf(m.sender) > -1) || (recipientMatch.indexOf(m.recipient) > -1))
                matches.push(m.mail_id)
          })

          if(!matches.length)
            throw new MError("NOTHING_TO_REMOVE")

          var cmd_postfix_remove_message = simpleCloneObject(app.config.get("cmd_postfix_remove_message"))
          cmd_postfix_remove_message.stdin = matches.join("\n")+"\n"
          return app.commander.spawn({chain:cmd_postfix_remove_message}, [{mail_id:"-"}])
       })

  }

  function validateMailId(mail_id) {
     return vali.async({mail_id:mail_id},{mail_id:{presence:true, isString:{strictName: true}}})
  }

  re.FetchMessage = function(mail_id) {
     return validateMailId(mail_id)
       .then(d=>{
           var cmd_postfix_fetch_message = app.config.get("cmd_postfix_fetch_message")
           return app.commander.spawn({dontLog_stdout: true, chain:cmd_postfix_fetch_message}, [d])
       })
  }

  re.DeleteMessage = function(mail_id) {
     return validateMailId(mail_id)
       .then(d=>{
           var cmd_postfix_remove_message = app.config.get("cmd_postfix_remove_message")
           return app.commander.spawn({chain:cmd_postfix_remove_message}, [d])
       })
  }

  re.MailqAutoCleanup = function(){
      return knex("mailqcleanup").select()
        .then(rows=>{
           if(!rows.length) throw new MError("NOTHING_TO_CLEANUP")

           console.log("Cleaning up mailq removing configured emails")

           var r = {sender:[],recipient:[]}
           rows.forEach(row=>{
              if(!r[row.mc_type]) {
                r.sender.push(row.mc_email)
                r.recipient.push(row.mc_email)
              }
              else
                r[row.mc_type].push(row.mc_email)

           })

           return deleteMessagesByEmail(r.sender, r.recipient)
        })
  }

  re.MailqAutoRemove = function(mc_id){
     return knex("mailqcleanup").whereRaw("mc_id=?", mc_id).delete().return()
  }

  re.MailqAutoFetch = function(mc_id){
     return knex("mailqcleanup").select()
  }

  re.MailqAutoInsert = function(in_data){
     return vali.async(in_data, {mc_email: {presence:true, mcEmail: true}, mc_type: {presence:true, inclusion: ["email","sender","recipient"]}})
       .then(d=>{
          return knex("mailqcleanup").insert(d)
       })
       .then(rows=>{
          return Promise.resolve({mc_id: rows[0]})
       })
  }

    if(!app.cron_mailq_auto_cleanup) {
          app.cron_mailq_auto_cleanup = true

            const cron = require('Croner');

            var csp = app.config.get("cron_mailq_auto_cleanup")
            if(csp) {
                cron.schedule(csp, function(){
                    return re.MailqAutoCleanup()
                        .catch(ex=>{
                           // we swallow this one here, as noone cares.
                           if(ex.message == "NOTHING_TO_REMOVE") return;
                           if(ex.message == "NOTHING_TO_CLEANUP") return;

                           console.error("Exception while cleaning up mailq", ex);
                        })
                });
            }


    }


  return re
}
