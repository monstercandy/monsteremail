// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('emailtallies', function(table) {
            table.string('username').primary();
            table.bigInteger('bytes').notNullable();
            table.integer('messages').notNullable();
         }),

         knex.raw("PRAGMA journal_mode=WAL;"),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('emailtallies'),
    ])

};
