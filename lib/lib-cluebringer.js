var lib = module.exports = function(app, knexes){

    var knexClue = knexes.cluebringer

    const MError = require("MonsterError")

    const ValidatorsLib = require("MonsterValidators")
    var Common = ValidatorsLib.array()
    var vali = ValidatorsLib.ValidateJs()

    const dotq = require("MonsterDotq");

    const internal_domains = "internal_domains"
    const greylist_domains = "greylist_domains"
    const outbound_priority = 10

      vali.validators.isQuotas = function(value, options, key, attributes, glob) {
          for(var row of value) {

              if(!vali.isInteger(row.Limit)) return "invalid Limit"
              if(row.Interval == "monthly") row.Interval = 30*86400
              if(row.Interval == "weekly") row.Interval = 7*86400
              if(row.Interval == "daily") row.Interval = 86400
              if(row.Interval == "hourly") row.Interval = 3600
              if(!vali.isInteger(row.Interval)) return "invalid Interval"

              if(!row.Message) row.Message = `Max number of emails / ${row.Interval}s: ${row.Limit}`
              if(!vali.isString(row.Message)) return "invalid Message"
          }

          attributes[key] = value
      }

	var re = {}

/*
select Member, policy_group_members.Comment AS member_comment, policy_groups.Name,policy_groups.Comment AS group_comment from policy_group_members   left join policy_groups on policy_groups.ID=policy_group_members.PolicyGroupID where Member="$m30@nymemobilitas.hu" AND policy_groups.Disabled=0 AND policy_group_members.Disabled=0;
+-----------------------+----------------+-----------------+------------------------------------------------------------------+
| Member                | member_comment | Name            | group_comment                                                    |
+-----------------------+----------------+-----------------+------------------------------------------------------------------+
| $m30@nymemobilitas.hu |                | sasl_high_users | azoknak a juzereknek a listaja, akik napi 2000 emailt kuldhetnek |
+-----------------------+----------------+-----------------+------------------------------------------------------------------+
*/
  re.FetchPolicyForEmails=function(emails){

     var query = knexClue("policy_group_members")
      .select(
        "Member",
        "Comment"
      );

     if(emails.length < 100) {
        var prefixedEmails = emails.map(x=>"$"+x);
        query = query.whereIn("Member", prefixedEmails);

     } else {
         var whereStrs = [];
         var whereParams = [];

         var uniqueDomains = {};
         var r = /.+@(.+)/;
         emails.forEach(email=>{
            var m = r.exec(email);
            if(m) {
                var domain = m[1];
                if(!uniqueDomains[domain]) {
                   uniqueDomains[domain] = 1;
                   whereParams.push("$%@"+domain);
                   whereStrs.push("Member LIKE ?");
                }
            }
         })

         if(whereStrs.length > 0)
            query = query.whereRaw(whereStrs.join(" OR "), whereParams);
     }



     return query.andWhere(knexClue.raw("policy_group_members.Disabled=0",[]))

  }
  re.FetchPolicyForEmail = function(email){
     return re.FetchPolicyForEmails([email])
  }
  re.ResetPolicyForEmail = function(email){
     return knexClue("policy_group_members").whereRaw("Member=?", ["$"+email]).delete().return()
  }

  re.MergePolicies = function(policiesToImport){
     return dotq.linearMap({array:policiesToImport, action: function(policyToImport){
            return re.getOutboundPolicyByName(policyToImport.Name)
              .then((policy)=>{
                  throw new MError("ALREADY_EXISTS", policyToImport.Name)
              })
              .catch(ex =>{
                  if(ex.message == "POLICY_NOT_FOUND") {
                    // console.log("inserting", policyToImport);
                    return re.AddNewPolicy(policyToImport);                
                  }
                  if(ex.message == "ALREADY_EXISTS") return;
                  throw ex;
              })

     }})
  }


  re.BackupPolicies = function(){
     return re.FetchAllOutboundPolicies()
       .then(policies=>{
          return dotq.linearMap({array: policies, catch:true, action: function(policy){
             return re.FetchQuotasForPolicy(policy.Name, policy)
               .then(quotas=>{
                  policy.Quotas = quotas;
                  delete policy.ID;
                  delete policy.Priority;
                  delete policy.DefaultPolicy;
               })
          }})
          .then(()=>{
             return policies;
          })
          
       })
  }

  function shouldBeOneRow(prom, err_msg){
     return prom.then(rows=>{
        if(rows.length != 1) throw new MError(err_msg)
        return Promise.resolve(rows[0])
     })
  }

  re.lookupPolicyGroupByName = function(name) {
     return shouldBeOneRow(knexClue("policy_groups").select().whereRaw("Disabled=0 AND Name=?",[name]), "GROUP_NOT_FOUND")
  }

  function insertPolicyMemberDomain(group_name, domain_name){
     return re.lookupPolicyGroupByName(group_name)
       .then(group=>{
           return knexClue("policy_group_members").insert({
              PolicyGroupID: group.ID,
              Member: policyMemberEncodeDomain(domain_name),
              Disabled: 0
            })
            .return()
       })
  }
  function removeMemberFromGroup(group_name, member){
     return re.lookupPolicyGroupByName(group_name)
       .then(group=>{
          return knexClue("policy_group_members").whereRaw("Member=? AND PolicyGroupID=?",[member, group.ID]).delete().return()
       })
  }
  re.GetGreylistedDomain = function(domain_name){
     return re.lookupPolicyGroupByName(greylist_domains)
       .then(group=>{
         return knexClue("policy_group_members")
          .select()
          .whereRaw("Disabled=0 AND Member=? AND PolicyGroupID=?",
            [policyMemberEncodeDomain(domain_name), group.ID]
          )
       })
  }
  re.InsertGreylistDomain = function(domain_name){
     return insertPolicyMemberDomain(greylist_domains, domain_name)
  }
  re.RemoveGreylistDomain = function(domain_name){
     return removeMemberFromGroup(greylist_domains, policyMemberEncodeDomain(domain_name))
  }

  re.InsertInternalDomain = function(domain_name){
     return insertPolicyMemberDomain(internal_domains, domain_name)
  }


  re.DeletePolicyMember = function(member){
      return knexClue("policy_group_members").whereRaw("Member=?",[member]).delete().return()
  }

  re.DeletePolicyMemberDomain = function(domain_name){
     return re.DeletePolicyMember(policyMemberEncodeDomain(domain_name))
  }

  re.FetchGroupMembersByName = function(group_name){
     return re.lookupPolicyGroupByName(group_name)
       .then(group=>{
         return knexClue("policy_group_members").select().whereRaw("Disabled=0 AND PolicyGroupID=?",[group.ID])
       })
  }

  function policyMemberEncodeDomain(domain_name){
     return "@"+domain_name
  }


  re.AssignPolicyForAccounts = function(accounts){
    var emails = []
    var helper = {}
    accounts.forEach(accountRow=>{
        helper["$"+accountRow.ea_email] = accountRow
        emails.push(accountRow.ea_email)
    })

    return re.FetchPolicyForEmails(emails)
      .then(policyRows=>{
         policyRows.forEach(policyRow=>{
            // console.log("hey", policyRow)
            if(helper[policyRow.Member])
              helper[policyRow.Member].Policy = policyRow.Comment
         })

         return Promise.resolve(accounts)
      })
  }


// TrackKey                        | LastUpdate | Counter | Type         | CounterLimit | Comment | Disabled
  function getBaseQueryForQuotas(){
      return knexClue("quotas_tracking")
        .select("TrackKey","LastUpdate","Counter","Type","CounterLimit","Comment")
        .leftJoin('quotas_limits', 'ID', 'QuotasLimitsID')
        .whereRaw("Disabled=0",[])
  }
  re.FetchQuotasForEmail = function(email){
      return convertTimestamp(getBaseQueryForQuotas()
        .andWhere(knexClue.raw("TrackKey=?", "SASLUsername:"+email)))
  }
  re.FetchAllQuotas = function(){
      return convertTimestamp(getBaseQueryForQuotas())
  }

  re.FetchAllGroups = function(){
      return knexClue("policy_groups").select()
  }

  function AddRemoveGroupMemberCommon(group_name, in_data){
      var d
      return vali.async(in_data, {Member:{presence:true, isString:true}})
        .then(ad=>{
             d = ad
             return re.lookupPolicyGroupByName(group_name)
        })
        .then(group=>{
          return Promise.resolve({group: group, data: d})
        })
  }

  re.AddGroupMember = function(group_name, in_data){
     return AddRemoveGroupMemberCommon(group_name, in_data)
        .then((x)=>{
           return knexClue("policy_group_members").insert({
              PolicyGroupID: x.group.ID,
              Member: x.data.Member,
              Disabled: 0,
           }).return()
        })
  }

  re.RemoveGroupMember = function(group_name, in_data){
     return AddRemoveGroupMemberCommon(group_name, in_data)
        .then((x)=>{
           return knexClue("policy_group_members")
             .whereRaw("PolicyGroupID=? AND Member=?",[x.group.ID,x.data.Member])
             .delete()
             .return()
        })
  }

  function doInTransaction(callback){
      return knexClue.transaction((trxKnexClue)=>{
             var trxKnexes = simpleCloneObject(knexes)
             trxKnexes.cluebringer = trxKnexClue
             var trxClueLib = lib(app, trxKnexes)

             return callback(trxClueLib, trxKnexClue)
      })
  }


  re.DoGreylisting = function(domain_name, in_data){
     return vali.async(in_data, {active:{presence:true, isBooleanLazy:true}})
       .then(d=>{
          return (d.active ? re.InsertGreylistDomain(domain_name) : re.RemoveGreylistDomain(domain_name))
       })
  }

  re.FlushOutboundQuotasForPolicy = function(policy_name, policyObject){
      return re.getOutboundPolicyByName(policy_name, policyObject)
        .then(policy=>{
            return knexClue("quotas_tracking")
              .whereRaw("QuotasLimitsID IN (SELECT ID FROM quotas_limits WHERE QuotasID IN (SELECT ID FROM quotas WHERE PolicyID=?))",[policy.ID])
              .delete()
              .return()

        })
  }

  re.ReplaceQuotasForPolicyInner = function(policy, Quotas){
        return re.FlushOutboundQuotasForPolicy(policy.Name, policy)
          .then(()=>{
             return knexClue("quotas_limits").whereRaw("QuotasID IN (SELECT ID FROM quotas WHERE PolicyID=?)", policy.ID).delete()
          })
          .then(()=>{
             return knexClue("quotas").whereRaw("PolicyID=?", policy.ID).delete()
          })
          .then(()=>{
             return insertQuotas(knexClue, policy, Quotas)
          })

  }

  re.ReplaceQuotasForPolicy = function(policy_name, in_data, policyObject){
    return vali.async({Quotas:in_data}, {Quotas:{presence: true, isArray:true, isQuotas: true}})
      .then(d=>{
        return doInTransaction((trxClueLib, trxKnexClue)=>{
             var policy
             return trxClueLib.getOutboundPolicyByName(policy_name, policyObject)
               .then(ap=>{
                  policy = ap

                  return trxClueLib.ReplaceQuotasForPolicyInner(policy, d.Quotas)
               })
        })

     })
  }

  re.RemovePolicy = function(policy_name){
    return doInTransaction((trxClueLib, trxKnexClue)=>{
       var policy
       var groupName
       return trxClueLib.getOutboundPolicyByName(policy_name)
         .then(ap=>{
            policy = ap
            if(policy.DefaultPolicy) throw new MError("DEFAULT_POLICY_CANNOT_BE_REMOVED")
            groupName = policyNameToMemberGroupName(policy.Name)

            return trxClueLib.ReplaceQuotasForPolicyInner(policy, [])
         })
         .then(()=>{
            return trxKnexClue("policy_members").whereRaw("PolicyID=?",[policy.ID]).delete()
         })
         .then(()=>{
            return trxKnexClue("policy_group_members").whereRaw("PolicyGroupID IN (SELECT ID FROM policy_groups WHERE Name=?)",[groupName]).delete()
         })
         .then(()=>{
            return trxKnexClue("policy_groups").whereRaw("Name=?",[groupName]).delete()
         })
         .then(()=>{
            return trxKnexClue("greylisting").whereRaw("PolicyID=?",[policy.ID]).delete()
         })
         .then(()=>{
            return trxKnexClue("policies").whereRaw("ID=?",[policy.ID]).delete().return()
         })
    })

  }

  function addRemoveEmailForPolicyCommon(policy_name, in_data) {
      var d
      var xre = {}
      return vali.async(in_data, {ea_email:{presence:true, mcEmail:true}})
        .then(ad=>{
             xre.data = ad
             return re.getOutboundPolicyByName(policy_name)
        })
        .then(policy=>{
          xre.policy = policy

          var group_name = policyNameToMemberGroupName(policy.Name)
          // console.log("looking up:", group_name)
          return re.lookupPolicyGroupByName(group_name)
        })
        .then(group=>{
          xre.group = group
          return Promise.resolve(xre)
        })
  }
  function encodeEmailForSaslPolicy(email){
     return "$"+email
  }
  function decodeEmailForSaslPolicy(rows){
     var re = []
     rows.forEach(row=>{
        var s = row.Member
        // console.log("FOO", row)
        if(!s.startsWith("$")) return
        re.push(s.substr(1))
     })
     return re
  }
  re.RemoveAllPoliciesForEmail = function(email){
      return knexClue("policy_group_members").whereRaw("Member=?",[encodeEmailForSaslPolicy(email)]).delete().return()
  }
  re.AddEmailForPolicy = function(policy_name, in_data){
     return addRemoveEmailForPolicyCommon(policy_name, in_data)
       .then(x=>{

          return doInTransaction(function(clueLib, trx){
             return clueLib.RemoveAllPoliciesForEmail(x.data.ea_email)
               .then(()=>{
                  return trx("policy_group_members").insert({
                    PolicyGroupID: x.group.ID,
                    Member: encodeEmailForSaslPolicy(x.data.ea_email),
                    Disabled: false,
                    Comment: x.policy.Name,
                  }).return()

               })
          })
       })
  }

  re.DeleteEmailForPolicy = function(policy_name, in_data){
     return addRemoveEmailForPolicyCommon(policy_name, in_data)
       .then(x=>{
          return knexClue("policy_group_members").whereRaw("Member=? AND PolicyGroupID=?",[encodeEmailForSaslPolicy(x.data.ea_email), x.group.ID]).delete().return()
       })
  }

  re.FetchEmailsForPolicy = function(policy_name){
      var group_name = policyNameToMemberGroupName(policy_name)
      // console.log("listing", group_name)
      return re.FetchGroupMembersByName(group_name)
        .then(decodeEmailForSaslPolicy)
  }

  re.FetchQuotasForPolicy = function(policy_name, policyObject){
     return re.getOutboundPolicyByName(policy_name, policyObject)
       .then(policy=>{
          // console.log("!!!here", policy)
          return knexClue("quotas")
            .select()
            .whereRaw("quotas.Disabled=0 AND quotas_limits.Disabled=0 AND quotas.PolicyID=?",[policy.ID])
            .leftJoin("quotas_limits", "quotas_limits.QuotasID", "quotas.ID")
       })
       .then(rows=>{
          var re = []
          rows.forEach(row=>{
             re.push({
                Interval: (mapPeriodBack(row.Period)),
                Message: row.Data,
                Limit: row.CounterLimit,
             })
          })

          return Promise.resolve(re)
       })
  }
  function mapPeriodBack(seconds){
      if(seconds == 30*86400) return "monthly"
      if(seconds == 7*86400) return "weekly"
      if(seconds == 86400) return "daily"
      if(seconds == 3600) return "hourly"
      return seconds
  }

  re.getOutboundPolicyByName = function(policy_name, policyObject){
       if(policyObject) return Promise.resolve(policyObject)

       return shouldBeOneRow(
        getPolicySqlChainCommon()
        .andWhere("Name",policy_name),
        "POLICY_NOT_FOUND")
        .then(policy=>{
           policyTransformations(policy)
           return Promise.resolve(policy)
         })
  }

  function policyNameToMemberGroupName(human_policy_name){
      var groupName = human_policy_name.toLowerCase().replace(" ", "_")+"_users"
      return groupName
  }

  re.AddNewPolicy = function(in_data){
      return vali.async(in_data, {
        Name: {presence:true, isString:{strictName: true}},
        Description: {isString:true},
        Quotas: {presence: true, isArray:true, isQuotas: true},
      })
      .then(d=>{
         if(!d.Description) d.Description = originalName
         return knexClue.transaction(function(trxClue){

             var policyId
             var cname = d.Name
             var groupName = policyNameToMemberGroupName(d.Name)

              return trxClue("policies").insert({Name:cname,Description:d.Description,Disabled:0,Priority:outbound_priority,mc_outbound:true})
               .then(ids=>{
                  policyId = ids[0]

                  return trxClue("policy_groups").insert({Name: groupName, Disabled: 0, Comment: d.Name})
               })
               .then(()=>{
                  return trxClue("greylisting").insert({PolicyID:policyId, Name: cname+": no greylist", UseGreylisting:false,Track: "SenderIP:/32", Comment: d.Name, Disabled: false})
               })
               .then(()=>{
                  return trxClue("policy_members").insert({PolicyID:policyId, Source:"%"+groupName, Destination:"any",Comment:d.Name,Disabled:false})
               })
               .then(()=>{
                  return insertQuotas(trxClue, {ID:policyId, Name: d.Name, DefaultPolicy: false}, d.Quotas)
               })
         })
        .catch(ex=>{
           if(ex.code == "SQLITE_CONSTRAINT")
              throw new MError("ALREADY_EXISTS")
           throw ex;
        })

      })
  }

  function insertQuotas(trxClue, policy, Quotas) {
      var ps = []
      Quotas.forEach(quota=>{
         ps.push(
           trxClue("quotas").insert({
              PolicyID: policy.ID,
              Name: policy.Name+"/"+quota.Interval+"s",
              Track: "SASLUsername",
              Period: quota.Interval,
              Verdict: policy.DefaultPolicy ? "REJECT" : "REJECT_OR_STOP",
              Data: quota.Message,
              Comment: policy.Name,
              Disabled: false
           })
           .then(ids=>{
                var quotaId = ids[0]
                return trxClue("quotas_limits")
                  .insert({
                    QuotasID: quotaId,
                    Type: "MessageCount",
                    CounterLimit: quota.Limit,
                    Disabled:false,
                    Comment: policy.Name+": "+quota.Limit+"/"+quota.Interval+"s"
                  })
           })
         )
      })

      return Promise.all(ps)
  }

  function getPolicySqlChainCommon(){
    return knexClue("policies").select("ID","Description","Name","Priority").whereRaw("mc_outbound=1 AND Disabled=0")
  }

  re.FetchAllOutboundPolicies = function(){
    return getPolicySqlChainCommon()
      .then(markDefaultPolicyArray)
  }

  function policyTransformations(row) {
     row.DefaultPolicy = (row.Name == "SASL")
  }
  function markDefaultPolicyArray(rows) {
     rows.forEach(policyTransformations)
     return rows
  }

//Instance | QueueID      | Timestamp  | ClientAddress | ClientName                    | ClientReverseName             | Protocol | EncryptionProtocol | EncryptionCipher            | EncryptionKeySize | SASLMethod | SASLSender | SASLUsername                   | Helo     | Sender                         | Size | RecipientData
  function getBaseQueryForSessions(){
      return knexClue("session_tracking").select()
  }
  re.FetchSessionsForEmail = function(email){
      return convertRecipient(getBaseQueryForSessions().whereRaw("SASLUsername=?", email))
  }
  re.FetchAllSessions = function(){
      return convertRecipient(getBaseQueryForSessions())
  }

  re.Cleanup = function(){
     var cmd_cluebringer_cleanup = app.config.get("cmd_cluebringer_cleanup")
     return app.commander.spawn({chain:cmd_cluebringer_cleanup});
  }

  installCron()

  return re


  function installCron(){

    if(app.cron_cluebringer_auto_cleanup) return;

    app.cron_cluebringer_auto_cleanup = true

    const cron = require('Croner');

    var csp = app.config.get("cron_cluebringer_auto_cleanup")
    if(!csp) return;
    cron.schedule(csp, function(){
        console.log("Cleaning up cluebringer");
        return re.Cleanup()
            .catch(ex=>{
               console.error("Exception while cleaning up cluebringer", ex);
            })
    });

  }


  function convertRecipient(prom){
     return prom.then(d=>{
        d.forEach(row=>{

            row.Recipients = []
            var myRegexp = /<([^>]+)>/g
            var match = myRegexp.exec(row.RecipientData);
            while (match != null) {
              // matched text: match[0]
              // match start: match.index
              // capturing group n: match[n]
              row.Recipients.push(match[1])
              match = myRegexp.exec(row.RecipientData);
            }
        })

        return Promise.resolve(d)
     })
  }

  function convertTimestamp(prom){
     return prom.then(d=>{
        d.forEach(row=>{
            row.LastUpdateHuman = new Date(row.LastUpdate*1000).toISOString();
        })

        return Promise.resolve(d)
     })
  }


}
