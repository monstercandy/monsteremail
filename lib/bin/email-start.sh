#!/bin/bash

. /opt/MonsterCommon/lib/common.inc

chown 0:0 "/var/lib/monster/$INSTANCE_NAME" 
chown 0:0 /var/lib/monster/$INSTANCE_NAME/*.json || true

chown -R 0:postfix /var/lib/monster/$INSTANCE_NAME/db-* || true
chmod 0775 /var/lib/monster/$INSTANCE_NAME/db-* || true

chown -R 0:0 "/var/log/monster/$INSTANCE_NAME"


mc_start
