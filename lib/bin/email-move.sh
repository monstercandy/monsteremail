#!/bin/bash

DOMAIN_NAME=$1
DEST_U_ID=$2
DEST_WH_ID=$3

if [ -z "$DEST_WH_ID" ]; then
   echo "Usage: $0 domain_name dest_u_id dest_wh_id"
   exit 1
fi

/opt/client-libs-for-other-languages/internal/perl/generic-light.pl email POST "/email/domains/domain/$DOMAIN_NAME/move" \
   "{\"dest_wh_id\":$DEST_WH_ID,\"dest_u_id\": \"$DEST_U_ID\"}"
