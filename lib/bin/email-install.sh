#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

mkdir -p "/var/lib/monster/$INSTANCE_NAME/db-postfix-dovecot" \
         "/var/lib/monster/$INSTANCE_NAME/db-cluebringer" \
         "/var/lib/monster/$INSTANCE_NAME/db-amavis" \
         "/var/lib/monster/$INSTANCE_NAME/db-dovecot-dict" \
         "/var/lib/monster/$INSTANCE_NAME/dkim-keys"

mkdir -p "/var/log/monster/$INSTANCE_NAME"
