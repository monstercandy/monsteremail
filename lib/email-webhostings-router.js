var lib = module.exports = function(app, knexes) {

    var domainsLib = require("lib-domains.js")
    var domains = domainsLib(app, knexes)
    const dotq = require("MonsterDotq");

    var router = app.ExpressPromiseRouter({mergeParams: true});

    router.get("/accounts", function(req,res,next){
      var accountsLib = require("lib-accounts.js");
      var accounts = accountsLib(app, knexes);
      return accounts.GetAccountsByWebhosting(req.params.webhosting_id)
        .then(aAccounts=>{
           accounts = aAccounts;
           return getClueBringer().AssignPolicyForAccounts(accounts)
        })
        .then(function(){

           req.sendResponse({accounts: accounts.Cleanup()});
        })
    })
    router.get("/aliases", function(req,res,next){
      var aliasesLib = require("lib-aliases.js");
      var aliases = aliasesLib(app, knexes);
      return aliases.GetAliasesByWebhosting(req.params.webhosting_id)
        .then(aliases=>{
           req.sendResponse({aliases: aliases.Cleanup()});
        })
    })

    router.get("/bccs", function(req,res,next){
      var bccsLib = require("lib-bccs.js");
      var bccs = bccsLib(app, knexes);
      return bccs.GetBccsByWebhosting(req.params.webhosting_id)
        .then(bccs=>{
           req.sendResponse({bccs: bccs.Cleanup()});
        })
    })

    router.use("/domains", require("email-domains-router.js")(app, knexes))

    router.post("/", function(req,res,next){

       return req.sendOk(
           domains.ChangeByWh(req.params.webhosting_id, req.body.json)
           
       )

    });

    router.delete("/", function(req,res,next){
         var domainObjects
         return req.sendTask(
           domains.GetDomainsByWebhosting(req.params.webhosting_id)
           .then(adomains=>{
              domainObjects = adomains
              return app.getEmitter()
           })
           .then(h=>{
              var emitter = h.emitter

              app.InsertEvent(req, {e_event_type: "webhosting-remove", e_other: true});


              dotq.linearMap({array: domainObjects, action: function(domain){
                    return domain.Delete(req.body.json, emitter)
                       .then(()=>{
                          emitter.send_stdout_ln("Ready deleting: "+ domain.do_domain_name)
                       })

              }})
               .then(()=>{
                  emitter.send_stdout_ln("Ready.")
                  emitter.close()
               })
               .catch(ex=>{
                  console.error("Unable to delete domains", ex)
                  emitter.close(1)
               })

              return Promise.resolve(h)
           })
         )

    })


    return router

    function getClueBringer(){
      var cluebringerLib = require("lib-cluebringer.js")
      var cluebringer = cluebringerLib(app, knexes)
      return cluebringer
    }


}
