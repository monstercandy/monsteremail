var lib = module.exports = function(app, knexes) {

   const MError = app.MError;

   const accountsLib = require("lib-accounts.js")
   var accounts = accountsLib(app, knexes);

   const domainsLib = require("lib-domains.js")
   var domains = domainsLib(app, knexes);

   const aliasesLib = require("lib-aliases.js")
   var aliases = aliasesLib(app, knexes);

   const bccsLib = require("lib-bccs.js")
   var bccs = bccsLib(app, knexes);

   const cluebringerLib = require("lib-cluebringer.js")
   var cluebringer = cluebringerLib(app, knexes);

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   const this_server = app.config.get("mc_server_name");
   const service_account_tld = app.config.get("service_account_tld");
   const service_account_username = app.config.get("service_account_username");


   wie.BackupWh = function(wh, req) {

    console.log("backupwh!", wh)

      var expectedServer = wh.wh_id+"."+this_server+service_account_tld;
      var expectedEmail = service_account_username+"@"+expectedServer;

      var replacedServer = "[[wh_id]].[[server]]"+service_account_tld;
      var replacedEmail = service_account_username+"@"+replacedServer;

      var re = {};
      return domains.GetDomainsByWebhosting(wh.wh_id)
        .then(emailDomains=>{
           re.domains = emailDomains;

           replaceStuff(re.domains, "do_domain_name");

           emailDomains.forEach(d=>{
              Array("do_id", "do_user_ids", "do_webhosting", "ea_domain").forEach(x=>{
                  delete d[x];
              })
           })

           return accounts.GetAccountsByWebhosting(wh.wh_id)
        })
        .then(emailAccounts=>{
           re.accounts = emailAccounts;

           replaceStuff(re.accounts, "ea_email");

           return cluebringer.AssignPolicyForAccounts(re.accounts);

        })
        .then(()=>{
           re.accounts.forEach(ea=>{
              Array("ea_id", "ea_user_id", "ea_webhosting", "ea_domain").forEach(x=>{
                  delete ea[x];
              })
           })

           return aliases.GetAliasesByWebhosting(wh.wh_id)

        })
        .then(emailAliases=>{
           re.aliases = emailAliases;

           emailAliases.forEach(ea=>{
              Array("el_id", "el_user_id", "el_webhosting", "el_domain", "verification_expired").forEach(x=>{
                  delete ea[x];
              })
           })

           return bccs.GetBccsByWebhosting(wh.wh_id)

        })
        .then(emailBccs=>{
           re.bccs = emailBccs;

           emailBccs.forEach(ea=>{
              Array("bc_id", "bc_user_id", "bc_webhosting", "bc_domain", "verification_expired").forEach(x=>{
                  delete ea[x];
              })
           })

        })
        .then((policies)=>{
           re.policies = policies;
        })
        .then(()=>{
           return re;
        })


      function replaceStuff(arr, f){

         arr.forEach(e=>{
            var s = e[f];
            var n;

            if(s == expectedEmail) {
              console.log("replacing", e[f], "to", replacedEmail);
                e[f] = replacedEmail;
            }
            else
            if(s == expectedServer) {
              console.log("replacing", e[f], "to", replacedServer);
                e[f] = replacedServer;
            }

            delete e.ea_maildir_path;


         })
      }

   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knexes.postfixDovecot.transaction(function(trx){
          var newKnexes = extend({}, knexes);
          newKnexes.postfixDovecot = trx;
          var wieTrx = require("Wie").transformWie(app, lib(app, newKnexes));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){
      return knexes.postfixDovecot.raw("SELECT DISTINCT do_webhosting FROM domains WHERE do_webhosting!=0")
        .then(rows=>{
            return rows.map(x => x.do_webhosting)
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      var now = moment.now();
      var domain_ids = {};

      return dotq.linearMap({array: in_data.domains, action: in_data_domain=>{

         in_data_domain.do_webhosting = wh.wh_id;
         in_data_domain.account = wh.wh_user_id;

         in_data_domain.do_domain_name = replaceStuff(in_data_domain.do_domain_name);

         return domains.Insert(in_data_domain, req)
           .then((do_id)=>{
               domain_ids[in_data_domain.do_domain_name] = do_id;
           })
      }})
      .then(()=>{

          return dotq.linearMap({array: in_data.aliases, action: in_data_alias=>{
             var domain_name = get_domain_name(in_data_alias.el_alias);
             in_data_alias.el_domain = domain_ids[domain_name];

             in_data_alias.el_webhosting = wh.wh_id;
             in_data_alias.el_user_id = wh.wh_user_id;
             in_data_alias.updated_at = now;

             return aliases.RestoreBackup(in_data_alias)
               .catch(ex=>{
                   if(ex.code != "SQLITE_CONSTRAINT")
                      throw ex;
                   // otherwise its fine.
               })
          }})

      })
      .then(()=>{

          return dotq.linearMap({array: in_data.accounts, action: in_data_account=>{
             var domain_name = get_domain_name(in_data_account.ea_email);
             in_data_account.ea_domain = domain_ids[domain_name];

             const path = require("path");
             in_data_account.ea_maildir_path = replaceStuff(path.join(wh.extras.mail_path, in_data_account.ea_email));
             // console.log("shit?", wh.extras, wh.extras.mail_path, "X", in_data_account.ea_maildir_path)

             in_data_account.ea_webhosting = wh.wh_id;
             in_data_account.ea_user_id = wh.wh_user_id;
             in_data_account.updated_at = now;

             in_data_account.ea_email = replaceStuff(in_data_account.ea_email);

             var tally = {};
             var policy = in_data_account.Policy;
             delete in_data_account.Policy;
             Array("bytes", "messages").forEach(x=>{
                var n = "tally_"+x;
                // console.log("nx", n, x, typeof in_data_account[n])
                if(typeof in_data_account[n] != "undefined") {
                   tally[x] = in_data_account[n];
                   delete in_data_account[n];
                }
             })

             // console.log("shit", in_data_account);

             var tallyToAdjust = Object.keys(tally).length > 0;


             return accounts.RestoreBackup(in_data_account)
               .catch(ex=>{
                   if(ex.code != "SQLITE_CONSTRAINT")
                      throw ex;
                   // otherwise its fine.
               })
               .then(()=>{
                  if((!tallyToAdjust) && (!policy))
                     return;

                  // we need to adjust the tally or the policy!
                  var email;
                  return accounts.GetAccountByEmail(in_data_account.ea_email)
                    .then(aemail=>{
                        email = aemail;
                        if(!policy) return;

                        return cluebringer.AddEmailForPolicy(policy, {ea_email: in_data_account.ea_email});

                    })
                    .then(aemail=>{

                        if(!tallyToAdjust) return;

                        return email.SetTallies(tally);
                    })
               })
          }})

      })
      .then(()=>{
          return dotq.linearMap({array: in_data.bccs, action: in_data_bcc=>{
             var domain_name = get_domain_name(in_data_bcc.bc_alias);
             in_data_bcc.bc_domain = domain_ids[domain_name];

             in_data_bcc.bc_webhosting = wh.wh_id;
             in_data_bcc.bc_user_id = wh.wh_user_id;
             in_data_bcc.updated_at = now;

             return bccs.RestoreBackup(in_data_bcc)
               .catch(ex=>{
                   if(ex.code != "SQLITE_CONSTRAINT")
                      throw ex;
                   // otherwise its fine.
               })
          }})
      })
      .then(()=>{
          app.InsertEvent(req, {e_event_type: "restore-wh-emails", e_other: wh.wh_id})

      })


      function get_domain_name(email) {
         var i = email.indexOf("@");
         return email.substr(i+1);
      }


      function replaceStuff(where){
         where = replaceAll(where, "[[wh_id]]", wh.wh_id);
         where = replaceAll(where, "[[server]]", this_server);        
         return where;
      }


   }



   return wie;

    function replaceAll(target, search, replacement) {
        return target.split(search).join(replacement);
    };

}
