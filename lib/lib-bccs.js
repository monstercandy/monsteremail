var lib = module.exports = function(app, knexes, domain){

    var knexPostfix = knexes.postfixDovecot

    const KnexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()

    var Common = ValidatorsLib.array()

    var moment = require("MonsterMoment")

	var re = {}

  function getBccsBy(filterHash, joinDomain) {

    var f = KnexLib.filterHash(filterHash)

    var p = knexPostfix("emailbccs").select("emailbccs.*")
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
    if(joinDomain)
      p = p.leftJoin("domains", "domains.do_id", "emailbccs.bc_domain").select("domains.do_domain_name")

    return p.then(rows=>{

              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })
  }
  function getBccsCountBy(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    var p = knexPostfix("emailbccs").count().whereRaw(f.sqlFilterStr, f.sqlFilterValues)

    return p.then(rows=>{
          return rows[0]["count(*)"]
    })
  }

  re.RestoreBackup = function(in_data){
     return knexPostfix("emailbccs").insert(in_data).return();
  }
  re.GetBccsByUserAccountAndWebhosting = function(in_data){
    return vali.async(in_data, app.listAccountWebhostingConstraint)
      .then(d=>{
         var params = {bc_user_id: d.account}
         if(d.webhosting)
            params.bc_webhosting = d.webhosting
         return getBccsBy(params, true)
      })
      .then(bccs=>{
         return Promise.resolve(app.groupByWebhosting(bccs, "bccs", "bc"))
      })
  }
	re.GetBccs = function(){
		 return getBccsBy({})
	}
  re.GetBccsByDomain = function(do_id){
     return getBccsBy({bc_domain: do_id})
  }
  re.GetBccsByWebhosting = function(wh_id){
     return getBccsBy({bc_webhosting: wh_id})
  }
  re.GetBccsCountByWebhosting = function(wh_id){
     return getBccsCountBy({bc_webhosting: wh_id})
  }
  re.GetBccByDomainAndId = function(do_id, bc_id){
     return shouldBeOneRow(getBccsBy({bc_domain: do_id, bc_id: bc_id}))
  }
  re.GetBccById = function(bc_id){
     return shouldBeOneRow(getBccsBy({bc_id: bc_id}))
  }
  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("BCC_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }
  re.GetBccsByName = function(bcc){
     return getBccsBy({bc_alias: bcc})
  }

	re.Insert = function(in_data, req){

      var maxBccs
      var account
      var d
      return vali.async(in_data, {
        "bc_domain": {presence: true},
        "bc_user_id": {presence: true, isValidUserId: { returnLookup: true, info: app.MonsterInfoAccount }},
        "bc_webhosting": {presence: true},

        "bc_direction": {presence: true, inclusion:["IN","OUT"]},
        "bc_verified": {isBooleanLazy:true, default: {value: false}},
        "bc_alias": {presence: true, mcEmail: true},
        "bc_destination": {presence: true, mcEmail: true},
      })
      .then(ad=>{

          d = ad
          account = d.bc_user_id
          d.bc_user_id = account.u_id

          return domain.GetBccsAndLimit()
            .then(re=>{
               if(!re.canBeAdded) throw new MError("BCC_LIMIT_IS_REACHED")
            })

      })
       .then(()=>{
          if(d.bc_verified) return Promise.resolve("")

          const Token = require("Token")
          return Token.GetTokenAsync(app.config.get("verification_token_length"))
       })
       .then((code)=>{

           d.bc_verification_token = code
           d.bc_verification_token_expires = app.getExpirationDate()

           return knexPostfix
             .insert(d)
             .into("emailbccs")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                if(ex.code == 'SQLITE_CONSTRAINT'){
                   console.error("error during sql query", ex)
                   throw new MError("ALREADY_EXISTS")
                }
                throw ex
             })

        })
        .then((id)=>{
           app.InsertEvent(req, {e_event_type: "email-bcc-create",
   		      e_user_id: d.bc_user_id,
              e_other: {bc_id:id, bc_alias: d.bc_alias}}
           )



           if(!d.bc_verified) {
               var mailer = app.GetMailer()

               var x = {}
               x.to = d.bc_destination
               x.template = "email/verify-bcc"
               x.locale = account.u_language
               x.context = {
                user: account,
                bc_id: id,
                bc_alias: d.bc_alias,
                bc_destination: d.bc_destination,
                bc_verification_token: d.bc_verification_token,
                bc_verification_token_expires: d.bc_verification_token_expires
               }

               mailer.SendMailAsync(x)

           }

           return Promise.resolve({bc_id:id})
        })

	}

  re.DeleteBccsByDomain = function(domain_id, in_data, emitter){
      if(emitter)emitter.send_stdout_ln("Deleting bccs...")

      return knexPostfix("emailbccs").whereRaw("bc_domain=?",[domain_id]).delete()
        .then(()=>{

          return Promise.resolve()
        })
  }

	return re


    function dbRowToObject(bcc){

      if(!bcc.bc_verified)
         bcc.verification_expired = moment.now() > bcc.bc_verification_token_expires

    	bcc.Cleanup = function(){
    		// delete bcc.bc_id
        delete bcc.bc_domain
        delete bcc.bc_verification_token
        delete bcc.bc_verification_token_expires
        delete bcc.bc_verification_ts
    		return bcc
    	}


      bcc.VerifyToken = function(in_data) {
        if(bcc.bc_verified)
           throw new MError("ALREADY_VERIFIED")
        if(bcc.verification_expired)
           throw new MError("EXPIRED")
        if(!bcc.bc_verification_token)
           throw new MError("TOKEN_NOT_CONFIGURED")

        return vali.async(in_data, {token: {presence:true, isString: true}})
          .then(d=>{
              if(d.token != bcc.bc_verification_token)
                throw new MError("TOKEN_MISMATCH")

              return knexPostfix("emailbccs").whereRaw("bc_id=?",[bcc.bc_id]).update({bc_verified: true, bc_verification_ts: moment.now()})
                .then(()=>{
                  bcc.el_verified = true
                  return Promise.resolve()
                })
          })

      }

      bcc.Change = function(in_data){
         return vali.async(in_data, {bc_verified: {isBooleanLazy: true}})
           .then(d=>{
              if(!Object.keys(d)) return;

              if(d.bc_verified)
                  d.bc_verification_ts = moment.now();

              return knexPostfix("emailbccs").whereRaw("bc_id=?",[bcc.bc_id]).update(d);
           })
      }


      bcc.Delete = function(in_data, req){
              return knexPostfix("emailbccs").whereRaw("bc_id=?",[bcc.bc_id]).delete()
                .then(()=>{

                   app.InsertEvent(req, {e_event_type: "email-bcc-remove",
      		          e_user_id: bcc.bc_user_id,
                      e_other: {bc_id:bcc.bc_id, bc_alias: bcc.bc_alias}}
                   )

                  return Promise.resolve()
                })
      }

    	return bcc

    }


}
