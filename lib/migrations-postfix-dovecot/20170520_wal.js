
exports.up = function(knex, Promise) {

    return Promise.all([

         // WAL mode is persistent and affects another apps as well. See: https://www.sqlite.org/wal.html
         knex.raw("PRAGMA journal_mode=WAL;"),
    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.resolve()
};
