exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('emailaccounts', function(table) {
            table.boolean('ea_suspended').notNullable().defaultTo(false);
         })


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('emailaccounts', function(table){
            table.dropColumn("ea_suspended");
        }),

    ])

};

