exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('emailaccounts', function(table) {
            table.string('ea_maildir_path').notNullable().defaultTo("");
         })
         .then(()=>{
         	 return knex.raw("UPDATE emailaccounts SET ea_maildir_path='/web/mails/' || ea_webhosting || '/' || ea_email ", []).return()
         })


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('emailaccounts', function(table){
            table.dropColumn("ea_maildir_path");
        }),

    ])

};
