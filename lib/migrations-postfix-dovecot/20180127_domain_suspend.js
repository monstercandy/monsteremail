exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('domains', function(table) {
            table.boolean('do_suspended').notNullable().defaultTo(false);
         })


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('domains', function(table){
            table.dropColumn("do_suspended");
        }),

    ])

};
