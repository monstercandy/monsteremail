exports.up = function(knex, Promise) {

    return Promise.all([



         knex.schema.createTable('mailqcleanup', function(table) {
            table.increments('mc_id').primary();

            table.string('mc_email').notNullable();
            table.enum('mc_type', ["email", "sender", "recipient"]).notNullable();
         }),


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('mailqcleanup'),
    ])

};
