// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('emailaccounts', function(table) {
            table.increments('ea_id').primary();

            table.uuid('ea_user_id');
            table.integer('ea_webhosting').notNullable();

            table.integer('ea_domain')
                 .notNullable()
                 .references('do_id')
                 .inTable('domains');

            table.string('ea_email').notNullable().unique();
            table.string('ea_password').notNullable();

            table.bigInteger('ea_quota_bytes').notNullable();
            table.integer('ea_autoexpunge_days').notNullable().defaultTo(0);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());

            table.index(["ea_webhosting"]);
            table.index(["ea_email"]);
         }),

         knex.schema.createTable('emailaliases', function(table) {
            table.increments('el_id').primary();

            table.uuid('el_user_id');
            table.integer('el_webhosting').notNullable();

            table.integer('el_domain')
                 .notNullable()
                 .references('do_id')
                 .inTable('domains');

            table.string('el_alias').notNullable();
            table.string('el_destination').notNullable();
            table.boolean('el_verified').notNullable().defaultTo(false);
            table.string('el_verification_token').notNullable();
            table.timestamp('el_verification_token_expires').notNullable();
            table.timestamp('el_verification_ts');

            table.index(['el_alias','el_verified'])

            table.unique(['el_alias','el_destination'])

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('emailbccs', function(table) {
            table.increments('bc_id').primary();

            table.uuid('bc_user_id');
            table.integer('bc_webhosting').notNullable();

            table.integer('bc_domain')
                 .notNullable()
                 .references('do_id')
                 .inTable('domains');

            table.string('bc_alias').notNullable();
            table.string('bc_destination').notNullable();
            table.enu('bc_direction', ['IN','OUT']).notNullable();

            table.boolean('bc_verified').notNullable().defaultTo(false);
            table.string('bc_verification_token').notNullable();
            table.timestamp('bc_verification_token_expires').notNullable();
            table.timestamp('bc_verification_ts');

            table.unique(['bc_alias','bc_direction'])

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('domains', function(table) {
            table.increments('do_id').primary();

            table.string('do_user_ids');
            table.integer('do_webhosting').notNullable();

            table.string('do_domain_name').notNullable().unique();
            table.boolean('do_active').notNullable().defaultTo(true);


            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('whitelist_blacklist', function(table) {
            table.increments('wb_id').primary();

            table.integer('wb_emailaccount')
                 .notNullable()
                 .references('ea_id')
                 .inTable('emailaccounts');

            table.string('wb_senderemail').notNullable();
            table.enum('wb_action', ["W","B"]).notNullable();

         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('emailaccounts'),
        knex.schema.dropTable('emailaliases'),
        knex.schema.dropTable('emailbcc'),
        knex.schema.dropTable('emailquota'),
        knex.schema.dropTable('domains'),
    ])

};
