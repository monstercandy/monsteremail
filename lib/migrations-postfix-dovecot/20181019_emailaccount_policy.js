exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('emailaccounts', function(table) {
            table.float('ea_spam_tag2_level', 2);
            table.float('ea_spam_kill_level', 2);
         })


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('emailaccounts', function(table){
            table.dropColumn('ea_spam_tag2_level');
            table.dropColumn('ea_spam_kill_level');
        }),

    ])

};

