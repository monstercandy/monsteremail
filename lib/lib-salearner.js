var lib = module.exports = function(app){

    var ValidatorsLib = require("MonsterValidators")

    var vali = ValidatorsLib.ValidateJs()


  var re = {}

  re.Learn = function(in_data){
     return vali.async(in_data, {email:{presence: true, mcEmail: true}, spam_or_ham: {presence: true, inclusion:["spam","ham"]}})
       .then(d=>{
           var cmd_salearn_stdin = simpleCloneObject(app.config.get("cmd_salearn_stdin"))
           return app.commander.spawn({chain:cmd_salearn_stdin, upload: true}, [app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), d])
       })

  }

  configureWatcher()

  return re

  function configureWatcher(){

      if(app.salearnWatcherConfigured) return
      app.salearnWatcherConfigured = true

      var dir = app.config.get("salearner_watch_directory")
      if(!dir) return

      const fs = require("MonsterDotq").fs()
      const path = require("path")

      var cmd_salearn = app.config.get("cmd_salearn")
      var regexp = /^[0-9]+-[0-9]+-(ham|spam)-(.+)/

      var lastfilename
      fs.watch(dir, function(e, filename){
         if(e != "change") return

         if(lastfilename == filename) return

         var b = path.basename(filename)
         var m = regexp.exec(b)

         if((!m)||(!m[0])) return

         lastfilename = filename

         console.log("salearn", e, b, m[1], m[2])

         var fullFilename = path.join(dir,filename)

         setTimeout(function(){

    //["--[spam_or_ham]", "--username=[email]", "[filename]
             return app.commander.spawn({chain:cmd_salearn}, [app.config.get("cmd_pathes"), {spam_or_ham: m[1], email: m[2], filename: fullFilename}])
               .then(h=>{
                  return h.executionPromise
               })
               .catch(ex=>{
                  console.error("error running salearn", ex)
               })
               .then(()=>{
                  return fs.unlinkAsync(fullFilename)
               })

         }, app.config.get("salearner_watch_delay"))
      })


  }
}
