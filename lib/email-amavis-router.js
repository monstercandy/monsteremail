var lib = module.exports = function(app, knexes) {

    var amavisLib = require("lib-amavis.js")

    var amavis = amavisLib(app, knexes)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.route("/msgs/quarantine/:mail_id")
       .all(function(req,res,next){
          return amavis.GetMailById(req.params.mail_id)
            .then(mail=>{
                req.mail = mail
                next()
            })
       })
       .get(function(req,res,next){
          return req.sendTask(req.mail.FetchAsTask())
       })
       .delete(function(req,res,next){
          return req.sendOk(
              req.mail.Delete()
                .then(()=>{
                    app.InsertEvent(req, {e_event_type: "quarantine-removed", e_other: true});
                })
          )
       })

    router.get("/msgs/quarantine", function(req,res,next){
       return req.sendPromResultAsIs(amavis.GetMailsInQuarantine())
    })

    router.post("/stats/mails/last", function(req,res,next){
       return req.sendPromResultAsIs(amavis.GetLastMsgs(req.body.json))
    })
    router.post("/stats/mails/clean", function(req,res,next){
       return req.sendPromResultAsIs(amavis.GetCleanMsgs(req.body.json))
    })
    router.post("/stats/mails/spammy", function(req,res,next){
       return req.sendPromResultAsIs(amavis.GetSpammyMsgs(req.body.json))
    })
    router.post("/stats/domains", function(req,res,next){
       return req.sendPromResultAsIs(amavis.GetDomains(req.body.json))
    })

    router.post("/cleanup", function(req,res,next){
       return req.sendPromResultAsIs(
         amavis.Cleanup()
            .then((r)=>{
                app.InsertEvent(req, {e_event_type: "amavis-cleanup"});
                return r;
            })
       )
    })


    if(!app.cron_amavis_configured) {
          app.cron_amavis_configured = true

            const cron = require('Croner');

            var csp = app.config.get("cron_amavis_cleanup")
            if(csp) {
                cron.schedule(csp, function(){
                    return amavis.Cleanup()
                });
            }


    }


    return router

}
