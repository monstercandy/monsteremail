(function(){
const regexpAutoSubmitted = /Auto-Submitted:\s*auto-notified; owner-email="(.+)"/i
const regexpAutoReplied = /Auto-Submitted: auto-replied \(vacation\)/i
const regexpFrom = /From:.*?<(.+)>/i
const regexpFromPure = /From: (.+)/i
const regexpSieveFrom = /X-Sieve-Redirected-From:\s*(.+)/i
const regexpSieveDeliveredTo = /Delivered-To:.*?<(.+)>/i

module.exports = function(app, knexes) {


    const accounts = require("lib-accounts")(app, knexes)

    const PassThroughStream = require('stream').PassThrough

	var smtp_client_config_base = app.config.get("smtp_client_config")
	if(!smtp_client_config_base){
		console.error("smtp_client_config not configured, disabling smtp server support")
		return
	}


    const nodemailer = require('nodemailer');
	var  smtp_client_transporter_global
	if((smtp_client_config_base.auth)&&(smtp_client_config_base.auth.user)) {
		smtp_client_transporter_global= nodemailer.createTransport(smtp_client_config_base);
		smtp_client_transporter_global.verify(err=>{
			if(err)
			  console.error("SMTP client transport returned failure:", err)
			else
			  console.log("SMTP client seems to be ok")
		})
    }



    const SMTPServer = require('smtp-server').SMTPServer;

	const server = new SMTPServer({
	    authOptional: true,

        onData: function(incomingStream, session, callback){

            /*
    	     envelope looks something like:
    	     { mailFrom: { address: 'test@sdfs.hu', args: false },
  rcptTo: [ { address: 'sdf@sdf.hu', args: false } ] }
              */

    	    console.log("incoming message with envelope", session.envelope)


            var tos = session.envelope.rcptTo.map(x=>x.address)

            var initialDataBuffers = []
            var dataSoFar = 0
    	    var relaying = false
    	    var fromFoundAlready = false
            var finished = false

    	    var outgoingStream
    	    var fullStr = ""


	    	incomingStream.on("error", function(err){
	    		console.error("error in incomingStream", err)
	    		finish(err)
	    	})

	    	incomingStream.on("data", function(d){
	    		if(relaying) {
	    			outgoingStream.push(d)
	    			return
	    		}

                initialDataBuffers.push(d)
                dataSoFar += d.length

                if(fromFoundAlready) return


                var aStr = d.toString()
                fullStr += aStr

            	// console.log("Incoming line:", aStr)

                var justFound = module.exports.parseMailHeaders(fullStr);
				if(justFound){
              	    fromFoundAlready = true
					console.log("found sender in the headers", justFound)
					queryAccount(justFound)
				}

                if((!fromFoundAlready)&&(dataSoFar > app.config.get("smtp_server_initial_data_limit_bytes"))) {
                	finish(new Error("we didnt find the stuff in the headers"))
                    return
                }

	    	})

	    	incomingStream.on("end", function(){
	    		if(fromFoundAlready) {
	    		  // everything was fine
	    		  finish()
	    		} else {
	    			console.error("Sender not found in", fullStr)
        		    finish(new Error("sender not found"))
	    		}

	    	})


	    	function finish(err){
	    		console.log("finish ws called", err)
	    		if(relaying)
	    			outgoingStream.end()

	    		app.InsertEvent(null, {e_event_type: "sieve-relay", e_other: session.envelope});

	    		finished = true
	    		callback(err ? new Error("Error") : undefined)
	    	}

            function queryAccount(from){
	            return accounts.GetAccountByEmail(from)
	            //return Promise.resolve({ea_password:"foobar"})
	              .then(account=>{
	              	  // setup outgoint email

	              	    var smtp_client_transporter = smtp_client_transporter_global
	              	    if(!smtp_client_transporter) {
	              	    	var smtp_client_config = simpleCloneObject(smtp_client_config_base)

	              	    	// we are using the hashes password here, since dovecot sasl auth is configured to accept it
	              	    	smtp_client_config.auth = {user: from, pass: account.ea_password}

			                smtp_client_transporter= nodemailer.createTransport(smtp_client_config);
	              	    }


			    		relaying = true

			            outgoingStream = new PassThroughStream()
			    	    outgoingStream.on("error", function(err){
			    	    	console.error("client error in outgoingStream", err)
		  	    		    finish(err)
			    	    })


						var message = {
						    envelope: {
						        from: from,
						        to: tos
						    },
						    raw: outgoingStream
						};

						smtp_client_transporter.sendMail(message, function(err, info){
							if(err){
						  	  console.error("error while sending mail:", err)
						  	  finish(err)
						  	  return
							}

							console.log("mail relayed successfully", info)
						})


						initialDataBuffers.forEach(d=>{
							outgoingStream.push(d)
						})
						initialDataBuffers = []

						if(finished)
							outgoingStream.end()

	              })
	              .catch(err=>{
	              	 console.error("wanted to send from invalid email address", from, err)
	              	 finish(err)
	              })

            }

        }
	});

	server.on('error', err => {
       console.error('Server error', err);
    });


	server.listen(app.config.get("smtp_server_port"), app.config.get("smtp_server_host"));


}

module.exports.parseMailHeaders = function(fullStr){

                var match = regexpAutoSubmitted.exec(fullStr)
                if((!match)||(!match[1]))
                	match = regexpAutoReplied.exec(fullStr);

                if((!match)||(!match[1]))
                	match = regexpSieveFrom.exec(fullStr)

                if((!match)||(!match[1]))
                	match = regexpSieveDeliveredTo.exec(fullStr)

                // from shall be really the last resort, the others are more meaningful
                if((!match)||(!match[1]))
                	match = regexpFrom.exec(fullStr)

                if((!match)||(!match[1]))
                	match = regexpFromPure.exec(fullStr)

			   if((match)&&(match[1]))
			   	  return match[1];

}

})()
