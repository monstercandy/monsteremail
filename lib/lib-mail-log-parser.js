(function(){
  var lib = module.exports = function(app, options){

    if(!options) options = {};

	const moment = require("MonsterMoment");
	const loginLogIntervalSecond = options.loginLogIntervalSecond || app.config.get("mail_log_loginlog_interval_second");
	const mailLogQuotaRejectNotificationIntervalSecond = options.mail_log_quota_reject_notification_interval_second || app.config.get("mail_log_quota_reject_notification_interval_second");

    var re = {};
	re.Start = function(){
       var logfile = options.mail_log_file_path || app.config.get("mail_log_file_path");
       if(!logfile) {
       	  throw new Error("mail_log_file_path not configured");
       }

      const Tail = require("MonsterTail").Tail;
	    var tail = new Tail.robust(logfile);
	    tail.on("line", function(data) {
          re.processLine(data);
	    });

	   cleanup();

	   function cleanup(){
	   	  setTimeout(function(){
	   	  	 var now = moment.nowUnixtime();
	   	  	 clenaupInner(lastLoginCache, loginLogIntervalSecond);
	   	  	 clenaupInner(lastQuotaExceededCache, mailLogQuotaRejectNotificationIntervalSecond);

	   	  	 cleanup();

	   	  	 function clenaupInner(arr, threshold){
	   	  	 	 Object.keys(arr).forEach(ip=>{
	   	  	 	 	var an = arr[ip];
	   	  	 	 	if(an < now - threshold) {
	   	  	 	 		delete arr[ip];
	   	  	 	 	}
	   	  	 	 })

	   	  	 }
	   	  }, app.config.get("mail_log_parser_cleaner_interval_ms"))
	   }

	}


	const saslFailedRegexp = new RegExp('.+\\[(.+)\]: SASL (LOGIN|PLAIN) authentication failed');
	// const tooManyErrorsRegexp = new RegExp('too many errors after AUTH from [^\[]+\[([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\]');
	// 
  const quotaRejectRegexp2 = new RegExp('rejected: You are allowed to send only ([0-9]+) .+ from=<([^>]+)>');
	const quotaRejectRegexp = new RegExp('action=reject,.+, from=(.+?),.+reason=quota_match.+, quota=[0-9]+(?:\.[0-9]+)?/([0-9]+)');
    const successfulLoginRegexp = new RegExp('-login: Login: user=<([^>]+)>, method=[^,]+, rip=([^,]+),');

    const accounts = options.accounts || app.getAccountsLib();

    var lastLoginCache = {};
    var lastQuotaExceededCache = {};

	re.processLine = function(line){
       var m = successfulLoginRegexp.exec(line);
       if(m) {
       	  var emailAddress = m[1];
       	  var ip = m[2];
       	  var now = moment.nowUnixtime();
          var key = emailAddress+"-"+ip;
       	  var oCached = lastLoginCache[key];
       	  lastLoginCache[key] = now;
       	  if((!oCached)||(now - oCached > loginLogIntervalSecond)) {
  	 	  	 accounts.GetAccountByEmail(emailAddress)
 	  	 	  .then((email)=>{
       	         return app.InsertEvent(null, {
                   e_ip: ip,
                   e_event_type: "auth-credential",
                   e_username: emailAddress,
                   e_user_id: email.ea_user_id,
                   e_other: {successful: true},
                });
 	  	 	  })
 	  	 	  .catch(ex=>{
 	  	 	  	 console.error("error while reporting sucessful login event", ex);
 	  	 	  })

       	  }

       	  return;
       } 

       m = saslFailedRegexp.exec(line);
       if(m) {
            var ip = m[1];
            app.InsertEvent(null, {
               e_ip: ip,
               e_event_type: "auth-credential",
               e_other: {successful: false},
            });
       	  return;
       }


       var emailAddress;
       var quotaLimit;
       m = quotaRejectRegexp.exec(line);
       if(m)
       {
          emailAddress = m[1];
          quotaLimit = parseInt(m[2], 10);        
       }
       else
       {
          m = quotaRejectRegexp2.exec(line);
          if(m)
          {
            emailAddress = m[2];
            quotaLimit = parseInt(m[1], 10);                
          }
       }

       if(m) {
       	
       	  var now = moment.nowUnixtime();
       	  var oCached = lastQuotaExceededCache[ip];
       	  lastQuotaExceededCache[ip] = now;
       	  if((!oCached)||(now - oCached > loginLogIntervalSecond)) {
  	 	  	 accounts.GetAccountByEmail(emailAddress)
  	 	  	  .then(email=>{
	               var mailer = app.GetMailer()

	               var x = {}
	               x.toAccount = {user_id: email.ea_user_id, emailCategory: "TECH"};
	               x.cc = emailAddress;
	               x.template = "email/quota-exceeded"
	               x.context = {
	                 emailAccount: emailAddress,
	                 quotaLimit: quotaLimit,
	               };

	               return mailer.SendMailAsync(x)

  	 	  	  })
  	 	  	  .catch(ex=>{
  	 	  	  	 console.error("error while sending quota exceeded message", ex);
  	 	  	  })

       	  }

       	  return;
       }

	}

	return re;


  }
})();
