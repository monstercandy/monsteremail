require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "somewebhosting.hu"
    const email_user = "anything"
    const email_full = `${email_user}@${domain_name}`

    const webhostingId = 20005

    describe('basic tests', function() {

        common.createDomain(domain_name, null, webhostingId)

        common.addAccount(email_user, domain_name, webhostingId)


        it('get webhosting list grouped by webhosting', function(done) {

             mapi.post( "/webhostings/list", {account: common.account_id}, function(err, result, httpResponse){

                 // console.log(require("util").inspect(result, false, null)); process.reallyExit();

                 assert.isNull(err)
                 assert.property(result, "bccs")
                 assert.property(result, "domains")
                 assert.property(result, "aliases")
                 assert.property(result, "accounts")

                 Object.keys(result.accounts).forEach(x=>{
                     Object.keys(result.accounts[x]).forEach(y=>{
                        result.accounts[x][y].forEach(z=>{
                            assert.property(z, "created_at")
                            assert.property(z, "updated_at")
                        })
                    })
                 })
                 Object.keys(result.domains).forEach(x=>{
                        result.domains[x].forEach(z=>{
                            assert.property(z, "created_at")
                            assert.property(z, "updated_at")
                        })
                 })                 
                 // console.dir(result.domains)
                 // console.dir(result.accounts)

                 done()

             })


        });

        it('suspending domains should work via the webhosting route', function(done) {

             mapi.post( "/webhostings/"+webhostingId,{do_suspended: true},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done();
             })
        });

        it('should be reflected', function(done) {

             mapi.get( "/domains/domain/"+domain_name, function(err, result, httpResponse){
                // console.log(result);
                assert.propertyVal(result, "do_suspended", 1)
                done();
             })
        });

        it('resuming domains should work via the webhosting route', function(done) {

             mapi.post( "/webhostings/"+webhostingId,{do_suspended: false},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done();
             })
        });

        it('should be reflected', function(done) {

             mapi.get( "/domains/domain/"+domain_name, function(err, result, httpResponse){
                // console.log(result);
                assert.propertyVal(result, "do_suspended", 0)
                done();
             })
        });

        it('delete everything through webhosting route should work', function(done) {

             common.setupRelayerForFilemanRemove(email_full, webhostingId)

             mapi.delete( "/webhostings/"+webhostingId,{},function(err, result, httpResponse){
                assert.property(result, "id")

                var url = "http://127.0.0.1:"+app.config.get("listen_port")+"/email/tasks/"+result.id

 
                request({method:'GET',uri: url}, (error, response, body) => {
                      // console.log("task request done", err, body)
                      assert.ok(body.indexOf(": successful :") > -1)
                      done()
                })

             })

        })     


        it('get domains should be empty', function(done) {

             mapi.get( "/webhostings/"+webhostingId+"/domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "domains")
                assert.deepEqual(result.domains, [])
                done()

             })

        })    


    })




}, 10000)

