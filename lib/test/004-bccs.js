require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "somebcc.hu"


    var verification_codes = {}

    describe('basic tests', function() {

        shouldBeEmpty()

        common.createDomain(domain_name)

        for(var i = 1; i <= 3; i++)
           common.addAccount("some"+i, domain_name)

        it('adding a bcc; invalid alias (no such email address exists)', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/bccs",
                {
                 "account": common.account_id,
                 "bc_alias":"notexists@"+domain_name,
                 "bc_destination":"whatever@whatever.com",
                 "bc_direction":"IN",
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        addBcc("some1", null, true) // adding it as validated

        it('adding the very same combo should fail', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/bccs",
                {
                 "account": common.account_id,
                 "bc_alias":"some1@"+domain_name,
                 "bc_destination":"whatever@whatever.com",
                 "bc_direction":"IN",
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })

        addBcc("some2", "insider@"+domain_name) // adding it with internal destination (shoul thus be validated)

        canBeAdded(true)

        addBcc("some3") // and this is just a regular stuff, should be unvalidated

        it("verification code should have been sent out in email for the previous call", function(){
            assert.equal(Object.keys(verification_codes).length, 1)
        })

        canBeAdded(false)

        it('bccs should be rejected when the limit is overdrawn', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/bccs",
                {
                 "account": common.account_id,
                 "bc_alias":"some3@"+domain_name,
                 "bc_destination":"whatever@whatever.com",
                 "bc_direction":"IN",
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "BCC_LIMIT_IS_REACHED")
                done()

             })

        })

        var bccIds = []
        it('get bccs should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/bccs",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result, "bccs")
                assert.ok(Array.isArray(result.bccs))
                assert.equal(result.bccs.length, 3)
                result.bccs.forEach(x=>{
                   assert.property(x, "bc_id")
                   bccIds.push(x.bc_id)
                   delete x.bc_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.bccs, [
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some1@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some2@"+domain_name,
          "bc_destination": "insider@somebcc.hu",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some3@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_verified": 0,
          "bc_direction":"IN",
          "verification_expired": false,
        }
                ])
                done()

             })

        })

        var origCommander;
        it('relocating a domain', function(done) {
             origCommander = app.commander;
             app.commander = {
                spawn: function(cmd){
                   assert.equal(cmd.chain.executable, "/bin/chown");

                   assert.equal(cmd.chain.args[0], '-R');
                   assert.equal(cmd.chain.args[1], '--from=20001');
                   assert.equal(cmd.chain.args[2], 20020);
                   assert.ok(cmd.chain.args[3].indexOf('/web/mails/20001/some') === 0);

                   return Promise.resolve({
                      executionPromise: Promise.resolve(),
                   })
                }
             };

             mapi.post( "/domains/domain/"+domain_name+"/move", {unit_test: true, dest_wh_id: 20020, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })        

        it('get bccs should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/bccs",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result, "bccs")
                assert.ok(Array.isArray(result.bccs))
                assert.equal(result.bccs.length, 3)
                result.bccs.forEach(x=>{
                   assert.property(x, "bc_id")
                   delete x.bc_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.bccs, [
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20020,
          "bc_alias": "some1@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20020,
          "bc_alias": "some2@"+domain_name,
          "bc_destination": "insider@somebcc.hu",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20020,
          "bc_alias": "some3@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_verified": 0,
          "bc_direction":"IN",
          "verification_expired": false,
        }
                ])
                done()

             })

        })

        it('relocating a domain back to the original', function(done) {
             app.commander = {
                spawn: function(cmd){
                  console.inspect(cmd);
                   assert.equal(cmd.chain.executable, "/bin/chown");
                   assert.equal(cmd.chain.args[0], '-R');
                   assert.equal(cmd.chain.args[1], '--from=20020');
                   assert.equal(cmd.chain.args[2], 20001);
                   assert.ok(cmd.chain.args[3].indexOf('/web/mails/20020/some') === 0);

                   
                   return Promise.resolve({
                      executionPromise: Promise.resolve(),
                   })
                }
             };

             mapi.post( "/domains/domain/"+domain_name+"/move", {unit_test: true, dest_wh_id: 20001, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  app.commander = origCommander;
                  done();
             })
        })  

        it('get bccs should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/bccs",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result, "bccs")
                assert.ok(Array.isArray(result.bccs))
                assert.equal(result.bccs.length, 3)
                result.bccs.forEach(x=>{
                   assert.property(x, "bc_id")
                   delete x.bc_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.bccs, [
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some1@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some2@"+domain_name,
          "bc_destination": "insider@somebcc.hu",
          "bc_direction":"IN",
          "bc_verified": 1,
        },
        {
          "bc_user_id": "10001",
          "bc_webhosting": 20001,
          "bc_alias": "some3@"+domain_name,
          "bc_destination": "whatever@whatever.com",
          "bc_verified": 0,
          "bc_direction":"IN",
          "verification_expired": false,
        }
                ])
                done()

             })

        })
        it('get bccs list via the search lib', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/bccs/list", {order: "bc_alias", desc: true}, function(err, result, httpResponse){
                 // console.log("here",err, result)

                 assert.deepEqual(result, { count: 3,
  rows:
   [ { bc_id: 3,
       bc_alias: 'some3@somebcc.hu',
       bc_destination: 'whatever@whatever.com',
       bc_direction: 'IN',
       bc_verified: 0 },
     { bc_id: 2,
       bc_alias: 'some2@somebcc.hu',
       bc_destination: 'insider@somebcc.hu',
       bc_direction: 'IN',
       bc_verified: 1 },
     { bc_id: 1,
       bc_alias: 'some1@somebcc.hu',
       bc_destination: 'whatever@whatever.com',
       bc_direction: 'IN',
       bc_verified: 1 } ] })

                 done()

             })

        })

        it('calling the new limit only route', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/bccs/limit", function(err, result, httpResponse){
                 // console.log("here",err, result)
                 assert.propertyVal(result, "canBeAdded", false);
                 assert.equal(result.max, 3);
                 assert.equal(result.count, 3);

                 done()

             })

        })

        it('verifying an account', function(done) {
           var bccId = bccIds[2]
           //console.log("verifying", aliasId, verification_codes, verification_codes[aliasId])
                     mapi.post( "/domains/domain/"+domain_name+"/bcc/"+bccId+"/verify", {"token": verification_codes[bccId]}, function(err, result, httpResponse){
                        assert.equal(result, "ok")
                        done()
                     })
        })

        it('querying that account should now be verified', function(done) {
            var bccId = bccIds[2]
                     mapi.get( "/domains/domain/"+domain_name+"/bcc/"+bccId, function(err, result, httpResponse){
                        assert.propertyVal(result, "bc_verified", 1)
                        done()
                     })
        })



        it('get bccs list grouped by webhosting', function(done) {

             mapi.post( "/bccs/list", {account: common.account_id}, function(err, result, httpResponse){

                 assert.isNull(err)
                 assert.property(result, "bccs")
                 Object.keys(result.bccs).forEach(x=>{
                   Object.keys(result.bccs[x]).forEach(y=>{
                        result.bccs[x][y].forEach(z=>{
                          assert.property(z, "created_at")
                          assert.property(z, "updated_at")
                      })
                    })
                 })
                 // console.log(result.bccs)
                 done()

             })


        })

        it('get list of bccs via the webhosting route', function(done) {
             mapi.get( "/webhostings/"+common.expected_webhosting+"/bccs",function(err, result, httpResponse){

                assert.property(result, "bccs")

                done()

             })

        })

        it('removing bccs', function(done) {

            var bccsRemoved = 0
            bccIds.forEach(bccId=>{
                     mapi.delete( "/domains/domain/"+domain_name+"/bcc/"+bccId, {}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                        bccsRemoved++
                        if(bccsRemoved >= bccIds.length)
                          done()

                     })
            })

        })



        shouldBeEmpty()
        shouldBeEmptyDomainLevel()

        addBcc("some1", null, true) // adding it as validated

        var accountIds = []
        it('get accounts by domain', function(done) {

             var some1Id

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.ok(Array.isArray(result.accounts))
                result.accounts.forEach(r=>{


                     if(r.ea_email.match(/^some1@/))
                        some1Id = r.ea_id
                     else
                        accountIds.push(r.ea_id)

                })

                assert.ok(some1Id)

                accountIds.unshift(some1Id)

                // console.log("!!!!!!!!!!", result, accountIds)

                done()

             })

        })


        common.removeAccount(
            "some1@"+domain_name,
            accountIds,
            0,
            domain_name,
            'deleting account should remove bcc entries as well'
        )

        shouldBeEmpty()
        shouldBeEmptyDomainLevel()

    })



  function shouldBeEmpty(){
        it('get bccs should be empty globally', function(done) {

             mapi.get( "/bccs",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {bccs:[]})
                done()

             })

        })
  }

  function shouldBeEmptyDomainLevel(){
        it('get bccs should be empty for the target domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/bccs",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "bccs")
                assert.deepEqual(result.bccs, [])
                done()

             })

        })

  }

  function addBcc(email_user, destination, verified){

        it('adding a bcc should be fine: '+email_user, function(done) {

             common.setupMonsterInfoWebhosting(app)

             var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                    // console.log("here we go", data); process.reallyExit()

                     assert.propertyVal(data, "template", "email/verify-bcc")
                     assert.property(data, "context")
                     Array("bc_id", "bc_alias", "bc_destination", "bc_verification_token", "bc_verification_token_expires").forEach(q=>{
                       assert.property(data.context, q)
                     })
                     verification_codes[data.context.bc_id] = data.context.bc_verification_token
                     // console.log("crap", data, verification_codes)

                     return Promise.resolve("ok")
                 }
                 return re
             }


             var params = {
                 "account": common.account_id,
                 "bc_alias": email_user+"@"+domain_name,
                 "bc_destination": destination || "whatever@whatever.com",
                 "bc_direction":"IN",
                }

             if(typeof verified != "undefined")
                 params.bc_verified = verified

             mapi.put( "/domains/domain/"+domain_name+"/bccs", params,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.property(result, "bc_id")
                app.GetMailer = oGetMailer
                done()

             })

        })
  }

  function canBeAdded(what){
      common.canBeAdded("bccs", domain_name, what)
  }


}, 10000)

