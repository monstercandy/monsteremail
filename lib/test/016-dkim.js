require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const fs = require("fs")

    const domain_name_1 = "dkimdomain1.hu";
    const domain_name_2 = "dkimdomain2.hu";

    const webhosting_id = 20016;

    describe("creating a dkim key", function(){

        common.createDomain(domain_name_1, null, webhosting_id)

        it("should be empty initially", function(){

             return mapi.getAsync( "/dkim")
               .then(function(res){
                  assert.deepEqual(res.result, { active_domains: []})
               })
        })

        it("return value for non-existent keys", function(){

             return mapi.getAsync( "/dkim/"+domain_name_1, {})
               .then(function(res){
                  assert.deepEqual(res.result, {dkim_key_active: false});

               })
        })

        it("creating a dkim key", function(){

             return mapi.putAsync( "/dkim/"+domain_name_1, {})
               .then(function(res){
                  assert.ok(res.result);

                  assert.ok(res.result.full_bind);
                  delete res.result.full_bind;
                  assert.ok(res.result.raw_txt_value);
                  delete res.result.raw_txt_value;
                  assert.deepEqual(res.result, {
                      selector: 'mocha',
                      host_prefix: 'mocha._domainkey',
                      full_hostname: 'mocha._domainkey.'+domain_name_1,
                  });
               })
        })

        it("should be in the global list", function(){

             return mapi.getAsync( "/dkim")
               .then(function(res){
                  assert.deepEqual(res.result, { active_domains: [domain_name_1]})
               })
        })

        it("should find it by name", function(){

             return mapi.getAsync( "/dkim/"+domain_name_1, {})
               .then(function(res){
                  assert.ok(res.result);

                  assert.ok(res.result.full_bind);
                  delete res.result.full_bind;
                  assert.ok(res.result.raw_txt_value);
                  delete res.result.raw_txt_value;
                  assert.deepEqual(res.result, {
                      dkim_key_active: true,
                      dns_dkim_record_present: false,
                      selector: 'mocha',
                      host_prefix: 'mocha._domainkey',
                      full_hostname: 'mocha._domainkey.'+domain_name_1,
                  });
               })
        })

        it("removal of a dkim key", function(){

             return mapi.deleteAsync( "/dkim/"+domain_name_1, {})
               .then(function(res){
                  assert.equal(res.result, "ok")
               })
        })

        it("should be empty again", function(){

             return mapi.getAsync( "/dkim")
               .then(function(res){
                  assert.deepEqual(res.result, { active_domains: []})
               })
        })

        it("removal of a non-existing dkim key", function(){

             return mapi.deleteAsync( "/dkim/dkimdomain-nonexist.hu", {})
               .then(function(res){
                  assert.equal(res.result, "ok")
               })
        })

        common.createDomain(domain_name_2, null, webhosting_id)

        it("creating a dkim key again", function(){

             return mapi.putAsync( "/dkim/"+domain_name_2, {})
               .then(function(res){
                  assert.ok(res.result);
               })
        })

        common.deleteDomain(domain_name_2)

        it("domain removal should have removed the dkim key as well", function(){

             return mapi.getAsync( "/dkim")
               .then(function(res){
                  assert.deepEqual(res.result, { active_domains: []})
               })
        })
    })


}, 10000)

