require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "someaccounts.hu"


    describe('basic tests', function() {

        shouldBeEmpty()

        common.createDomain(domain_name, null, 0)

        it('adding an account to a domain not attached to any webhostings should be rejected', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"some@"+domain_name,
                 "ea_password":"12345678",
                 "ea_quota_bytes": common.validQuota,
                },

              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOMAIN_NOT_ATTACHED_TO_WEBHOSTING")
                done()

             })

        })

        common.createDomain(domain_name)

        it('adding an email account (invalid domain as email)', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"some@account.hu",
                 "ea_password":"12345678",
                 "ea_quota_bytes": common.validQuota,
                },

              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        addAccount("placeholder-for-quota")

        addAccount("some1")

        it('adding the very same should fail', function(done) {

             common.setupMonsterInfoWebhosting(app)

             app.GetRelayerMapiPool = function(){
               return {
                  postAsync: function(url, payload) {
                    return Promise.resolve({mocked:true})
                  }
                }
             }

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"some1@someaccounts.hu",
                 "ea_password":"12345678",
                 "ea_quota_bytes": common.validQuota,
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })

        it('adding one with same email and password', function(done) {

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"someusername1@someaccounts.hu",
                 "ea_password":"someusername1@someaccounts.hu",
                 "ea_quota_bytes": common.validQuota,
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })
        })

        it('adding one with same user and password', function(done) {

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"someusername2@someaccounts.hu",
                 "ea_password":"someusername2",
                 "ea_quota_bytes": common.validQuota,
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })
        })


        it('adding one with quota too high', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"some0@someaccounts.hu",
                 "ea_password":"12345678",
                 "ea_quota_bytes": common.validQuota*100,
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "QUOTA_IS_TOO_HIGH")
                done()

             })

        })

        canBeAdded(true)

        addAccount("some2") // adding it with internal destination (shoul thus be validated)

        canBeAdded(false)

        it('adding account should be rejected when the limit is overdrawn', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/accounts",
                {
                 "account": common.account_id,
                 "ea_email":"some3@"+domain_name,
                 "ea_password":"12345678",
                 "ea_quota_bytes": common.validQuota,
                },

              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ACCOUNT_LIMIT_IS_REACHED")
                done()

             })

        })


        var accountIds = []
        it('get accounts by domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.ok(Array.isArray(result.accounts))
                result.accounts.forEach(r=>{
                     accountIds.push(r.ea_id)
                     delete r.ea_id

                     assert.property(r, "created_at")
                     delete r.created_at
                     assert.property(r, "updated_at")
                     delete r.updated_at
                })
                assert.deepEqual(result.accounts,
                [
                  {
                    "ea_user_id": "10001",
                    "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "placeholder-for-quota@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/placeholder-for-quota@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some1@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/some1@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some2@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/some2@someaccounts.hu",
                  }
                ])
                done()

             })

        })


        var origCommander;
        it('relocating a domain', function(done) {
             origCommander = app.commander;
             app.commander = {
                spawn: function(cmd){
                   assert.equal(cmd.chain.executable, "/bin/chown");

                   assert.equal(cmd.chain.args[0], '-R');
                   assert.equal(cmd.chain.args[1], '--from=20001');
                   assert.equal(cmd.chain.args[2], 20020);
                   assert.ok(cmd.chain.args[3].indexOf('/web/mails/20001/') === 0);

                   return Promise.resolve({
                      executionPromise: Promise.resolve(),
                   })
                }
             };

             mapi.post( "/domains/domain/"+domain_name+"/move", {unit_test: true, dest_wh_id: 20020, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })        

        it('get accounts by domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.ok(Array.isArray(result.accounts))
                result.accounts.forEach(r=>{
                     delete r.ea_id

                     assert.property(r, "created_at")
                     delete r.created_at
                     assert.property(r, "updated_at")
                     delete r.updated_at
                })
                assert.deepEqual(result.accounts,
                [
                  {
                    "ea_user_id": "10001",
                    "ea_webhosting": 20020,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "placeholder-for-quota@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20020/placeholder-for-quota@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20020,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some1@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20020/some1@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20020,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some2@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20020/some2@someaccounts.hu",
                  }
                ])
                done()

             })

        })
        it('relocating a domain back to the original', function(done) {
             app.commander = {
                spawn: function(cmd){
                  console.inspect(cmd);
                   assert.equal(cmd.chain.executable, "/bin/chown");
                   assert.equal(cmd.chain.args[0], '-R');
                   assert.equal(cmd.chain.args[1], '--from=20020');
                   assert.equal(cmd.chain.args[2], 20001);
                   assert.ok(cmd.chain.args[3].indexOf('/web/mails/20020/') === 0);

                   
                   return Promise.resolve({
                      executionPromise: Promise.resolve(),
                   })
                }
             };

             mapi.post( "/domains/domain/"+domain_name+"/move", {unit_test: true, dest_wh_id: 20001, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  app.commander = origCommander;
                  done();
             })
        })  

        it('get accounts by domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.ok(Array.isArray(result.accounts))
                result.accounts.forEach(r=>{
                     delete r.ea_id

                     assert.property(r, "created_at")
                     delete r.created_at
                     assert.property(r, "updated_at")
                     delete r.updated_at
                })
                assert.deepEqual(result.accounts,
                [
                  {
                    "ea_user_id": "10001",
                    "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "placeholder-for-quota@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/placeholder-for-quota@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some1@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/some1@someaccounts.hu",
                  },
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 0,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "some2@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                    "ea_maildir_path": "/web/mails/20001/some2@someaccounts.hu",
                  }
                ])
                done()

             })

        })

        it('get accounts list via the search lib', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/accounts/list", {filter: "some%@", order: "ea_email", desc: true}, function(err, result, httpResponse){
                 // console.log("here",err, result)

                 assert.deepEqual(result, { 
                    count: 2,
                      rows:
                       [        
                  {
                    "ea_id": 3,
                    "ea_autoexpunge_days": 0,
                    "ea_email": "some2@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                  },
                       {
                    "ea_id": 2,
                    "ea_autoexpunge_days": 0,
                    "ea_email": "some1@someaccounts.hu",
                    "ea_quota_bytes": 10485760,
                    "ea_suspended": 0,
                  },

                   ] })

                 done()

             })

        })

        it('calling the new limit only route', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts/limit", function(err, result, httpResponse){
                 // console.log("here",err, result)
                 assert.propertyVal(result, "canBeAdded", false);
                 assert.equal(result.max, 3);
                 assert.equal(result.count, 3);

                 done()

             })

        })


        it('changing quota and auto purge days', function(done) {

                     mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0], {ea_quota_bytes: common.validQuota*2, ea_autoexpunge_days: 5}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                          done()

                     })

        })

        it('changing suspendedness', function(done) {

                     mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0], {ea_suspended: true}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                          done()

                     })

        })

        it('changing spam settings', function(done) {

                     mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0], {ea_spam_tag2_level:1.2, ea_spam_kill_level:2.3}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                          done()

                     })

        })


        it('get account by id should return the changed parameters', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+accountIds[0],function(err, result, httpResponse){
                // console.log("here",err, result)
                 delete result.ea_id

                 assert.property(result, "created_at")
                 delete result.created_at
                 assert.property(result, "updated_at")
                 delete result.updated_at

                assert.deepEqual(result,
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 5,
                    "ea_spam_tag2_level": 1.2,
                    "ea_spam_kill_level": 2.3,
                    "ea_email": "placeholder-for-quota@someaccounts.hu",
                    "ea_quota_bytes": common.validQuota * 2,
                    "ea_suspended": 1,
                    "ea_maildir_path": "/web/mails/20001/placeholder-for-quota@someaccounts.hu",
                  }
                )

                done()

             })

        })


        it('reseting spam settings', function(done) {

                     mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0], {ea_spam_tag2_level:0, ea_spam_kill_level:0}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                          done()

                     })

        })


        it('get account by id should return null for spam parameters', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+accountIds[0],function(err, result, httpResponse){

                // console.log("here",err, result)
                 delete result.ea_id

                 assert.property(result, "created_at")
                 delete result.created_at
                 assert.property(result, "updated_at")
                 delete result.updated_at

                assert.deepEqual(result,
                  {
                      "ea_user_id": "10001",
                      "ea_webhosting": 20001,
                    "ea_autoexpunge_days": 5,
                    "ea_spam_tag2_level": null,
                    "ea_spam_kill_level": null,
                    "ea_email": "placeholder-for-quota@someaccounts.hu",
                    "ea_quota_bytes": common.validQuota * 2,
                    "ea_suspended": 1,
                    "ea_maildir_path": "/web/mails/20001/placeholder-for-quota@someaccounts.hu",
                  }
                )

                done()


             })

        })


        it('changing auto purge days through email route', function(done) {

                     mapi.post( "/accounts/email/placeholder-for-quota@someaccounts.hu", {ea_autoexpunge_days: 10}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                          done()

                     })

        })


        it('should return the changed parameters through the email route as well', function(done) {

             mapi.get( "/accounts/email/placeholder-for-quota@someaccounts.hu",function(err, result, httpResponse){

                assert.propertyVal(result, "ea_autoexpunge_days", 10)

                done()

             })

        })

        it('setting tallies explicitly', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0]+"/tallies/set", {messages:10, bytes: 1000},function(err, result, httpResponse){

                assert.equal(result,"ok")

                done()

             })

        })



        it('get account by id should return the tally details as well', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+accountIds[0],function(err, result, httpResponse){

                assert.propertyVal(result,"tally_bytes", 1000)
                assert.propertyVal(result,"tally_messages", 10)

                done()

             })

        })

        it('get list of accounts should contain the tally just set', function(done) {
             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){

                assert.property(result, "accounts")
                var found = false
                result.accounts.forEach(x=>{
                    if(x.ea_id == accountIds[0]) {
                        assert.propertyVal(x,"tally_bytes", 1000)
                        assert.propertyVal(x,"tally_messages", 10)

                        found = true
                    }
                })

                assert.ok(found)

                done()

             })

        })

        it('get list of accounts via the webhosting route', function(done) {
             mapi.get( "/webhostings/"+common.expected_webhosting+"/accounts",function(err, result, httpResponse){

                assert.property(result, "accounts")

                done()

             })

        })

        it('getting the account through webhosting', function(done) {

             mapi.get( "/webhostings/"+common.expected_webhosting+"/domains/domain/"+domain_name+"/account/"+accountIds[0],function(err, result, httpResponse){
                assert.isNull(err)
                done()

             })

        })


        it('getting the same account, through webhosting route via invalid webohsting id should be rejected', function(done) {

             mapi.get( "/webhostings/"+common.unexpected_webhosting+"/domains/domain/"+domain_name+"/account/"+accountIds[0],function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOMAIN_NOT_FOUND")
                done()

             })

        })



        it('should invoke the correct doveadm command upon tally recalculation request', function(done) {

             const id = "something"
             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    // console.log("!!!", cmd, params)
                    assert.deepEqual(cmd, { chain:
   { executable: '[doveadm_path]',
     args: [ 'quota', 'recalc', '-u', '[ea_email]' ] } })

                    assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), { ea_email: 'placeholder-for-quota@someaccounts.hu' } ])
                    return Promise.resolve({id:id, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/webhostings/"+common.expected_webhosting+"/domains/domain/"+domain_name+"/account/"+accountIds[0]+"/tallies/recalculate", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:id})
                app.commander = oCommander
                done()

             })

        })

        it('should invoke the correct doveadm command upon when asking for deleting old emails', function(done) {

             const id = "something"
             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    // console.log("!!!", cmd, params)
                    assert.deepEqual(cmd, { chain:
   { executable: '[doveadm_path]',
     args:
      [ '-v',
        'expunge',
        '-u',
        '[ea_email]',
        'mailbox',
        'INBOX',
        'SENTBEFORE',
        '[older_than_days]d' ] },
  emitter: undefined })

                    assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), { ea_email: 'placeholder-for-quota@someaccounts.hu' },
  { older_than_days: 10 } ])
                    return Promise.resolve({id:id, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/webhostings/"+common.expected_webhosting+"/domains/domain/"+domain_name+"/account/"+accountIds[0]+"/cleanup", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:id})
                app.commander = oCommander
                done()

             })

        })


        it('global autoexpunge cleanup should invoke the correct doveadm commands', function(done) {

             var mainReturned = false

             var emitterCloseCalled = 0
             var subSpawns = 0
             const emitterId = "something"
             var oCommander = app.commander
             app.commander = {
                EventEmitter: function() {
                    return {
                        spawn: function(){
                            return Promise.resolve({id: emitterId})
                        },
                        close: function(c){
                            console.log("close called", c)
                            assert.isUndefined(c)
                            emitterCloseCalled++
                            checkForReady()
                        },
                        send_stdout_ln: function(n){
                            console.log("called", n)
                        }
                    }
                },
                spawn: function(cmd, params) {
                    subSpawns++
                    // console.log("!!!", cmd, params)
                    assert.property(cmd, "emitter")
                    assert.isNotNull(cmd.emitter)
                    delete cmd.emitter

                    assert.deepEqual(cmd, { chain:
   { executable: '[doveadm_path]',
     args:
      [ '-v',
        'expunge',
        '-u',
        '[ea_email]',
        'mailbox',
        'INBOX',
        'SENTBEFORE',
        '[older_than_days]d' ] } })

                    assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), { ea_email: 'placeholder-for-quota@someaccounts.hu' },
  { older_than_days: 10 } ])


                    checkForReady()

                    return Promise.resolve({id:subSpawns, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/accounts/cleanup/autoexpunge", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:emitterId})
                mainReturned = true
                checkForReady()

             })


             function checkForReady(){
                if(!mainReturned) return
                if(subSpawns != 1) return
                if(emitterCloseCalled != 1) return

                app.commander = oCommander
                done()
             }

        })


        it('get accounts list grouped by webhosting', function(done) {

             mapi.post( "/accounts/list", {account: common.account_id}, function(err, result, httpResponse){

                 assert.isNull(err)
                 assert.property(result, "accounts")
                 Object.keys(result.accounts).forEach(x=>{
                   Object.keys(result.accounts[x]).forEach(y=>{
                        result.accounts[x][y].forEach(z=>{
                            assert.property(z, "created_at")
                            assert.property(z, "updated_at")
                        })
                   })
                 })
                 // console.log(result.accounts)

                 done()

             })


        })


        it('teaching spam assassin should invoke the correct command', function(done) {

             const id = "something"
             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                     // console.log("!!!", cmd, params)
                    assert.deepEqual(cmd, { upload: true, chain:
   { executable: '[salearn_path]',
     args: [ '--[spam_or_ham]', '--username=[email]' ], } })

                     assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), { spam_or_ham: 'ham',
    email: 'placeholder-for-quota@someaccounts.hu' } ])
                    return Promise.resolve({id:id, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/webhostings/"+common.expected_webhosting+"/domains/domain/"+domain_name+"/account/"+accountIds[0]+"/salearn", {spam_or_ham:"ham"}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:id})
                app.commander = oCommander
                done()

             })

        })


        common.removeAccount(
            "placeholder-for-quota@"+domain_name,
            accountIds,
            0,
            domain_name,
            'removing account by id'
        )


        common.deleteDomain(domain_name)


        shouldBeEmpty()
    })


    describe('cleaning up junks', function() {

        it('should invoke the correct doveadm command upon request', function(done) {

             const id = "something"
             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    // console.log("!!!", cmd, params)
                    assert.deepEqual(cmd, { chain:
   { executable: '[doveadm_path]',
     args:
      [ 'expunge',
        '-A',
        'mailbox',
        'INBOX.Junk',
        'SENTBEFORE',
        '[cleanup_junk_older_than_days]d' ] } })

                    assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params") ])
                    return Promise.resolve({id:id, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/accounts/cleanup/junk", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:id})
                app.commander = oCommander
                done()

             })

        })



    })


  function shouldBeEmpty(){
        it('get accounts should be empty globally', function(done) {

             mapi.get( "/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {accounts:[]})
                done()

             })

        })
  }

  function shouldBeEmptyDomainLevel(){
        it('get accounts should be empty for the target domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.deepEqual(result.accounts, [])
                done()

             })

        })

  }

  function addAccount(email_user){

      return common.addAccount(email_user, domain_name)

  }


  function canBeAdded(what){
      common.canBeAdded("accounts", domain_name, what)
  }


}, 10000)

