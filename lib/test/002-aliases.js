require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert, app)

    const domain_name = "somealias.hu"

    var verification_codes = {}

    describe('basic tests', function() {

        shouldBeEmpty()

        common.createDomain(domain_name, null, 0)

        it('adding an alias to a domain not attached to any webhostings should be rejected', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/aliases", 
                {
                 "account": common.account_id, 
                 "el_alias":"some@"+domain_name, 
                 "el_destination":"whatever@whatever.com"
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOMAIN_NOT_ATTACHED_TO_WEBHOSTING")
                done()

             })

        })

        common.createDomain(domain_name)

        it('adding an alias (invalid domain as alias)', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/aliases", 
                {
                 "account": common.account_id, 
                 "el_alias":"some@alias.hu", 
                 "el_destination":"whatever@whatever.com"
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        addAlias("some1", null, true) // adding it as validated

        it('adding the very same combo should fail', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/aliases", 
                {
                 "account": common.account_id, 
                 "el_alias":"some1@"+domain_name, 
                 "el_destination":"whatever@whatever.com"
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })

        addAlias("some2", "insider@"+domain_name) // adding it with internal destination (shoul thus be validated)

        canBeAdded(true)

        addAlias("some3") // and this is just a regular stuff, should be unvalidated

        it("verification code should have been sent out in email for the previous call", function(){
            assert.equal(Object.keys(verification_codes).length, 1)
        })

        canBeAdded(false)

        it('aliases should be rejected when the limit is overdrawn', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/domain/"+domain_name+"/aliases", 
                {
                 "account": common.account_id, 
                 "el_alias":"some3@"+domain_name, 
                 "el_destination":"whatever@whatever.com"
                },
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALIAS_LIMIT_IS_REACHED")
                done()

             })

        })

        var aliasIds = []
        it('get aliases should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/aliases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result,"aliases")
                assert.ok(Array.isArray(result.aliases))
                assert.equal(result.aliases.length, 3)
                result.aliases.forEach(x=>{
                   assert.property(x, "el_id")
                   aliasIds.push(x.el_id)
                   delete x.el_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.aliases, [
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some1@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some2@somealias.hu",
          "el_destination": "insider@somealias.hu",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some3@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 0,
          "verification_expired": false,
        }                  
                ])
                done()

             })

        })    

        it('relocating a domain', function(done) {
             mapi.post( "/domains/domain/"+domain_name+"/move", {dest_wh_id: 20020, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })

        it('get aliases should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/aliases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result,"aliases")
                assert.ok(Array.isArray(result.aliases))
                assert.equal(result.aliases.length, 3)
                result.aliases.forEach(x=>{
                   assert.property(x, "el_id")
                   delete x.el_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.aliases, [
        {
          "el_user_id": "10001",
          "el_webhosting": 20020,
          "el_alias": "some1@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20020,
          "el_alias": "some2@somealias.hu",
          "el_destination": "insider@somealias.hu",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20020,
          "el_alias": "some3@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 0,
          "verification_expired": false,
        }                  
                ])
                done()

             })

        })    

        it('relocating a domain back to the original', function(done) {
             mapi.post( "/domains/domain/"+domain_name+"/move", {dest_wh_id: 20001, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })

        it('get aliases should return them w verification status as expected', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/aliases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result,"aliases")
                assert.ok(Array.isArray(result.aliases))
                assert.equal(result.aliases.length, 3)
                result.aliases.forEach(x=>{
                   assert.property(x, "el_id")
                   delete x.el_id
                   delete x.created_at
                   delete x.updated_at
                })

                assert.deepEqual(result.aliases, [
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some1@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some2@somealias.hu",
          "el_destination": "insider@somealias.hu",
          "el_verified": 1,
        },
        {
          "el_user_id": "10001",
          "el_webhosting": 20001,
          "el_alias": "some3@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 0,
          "verification_expired": false,
        }                  
                ])
                done()

             })

        })    

        it('get aliases list via the search lib', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/aliases/list", {order: "el_alias", desc: true}, function(err, result, httpResponse){
                 // console.log("here",err, result)

                 assert.deepEqual(result, { count: 3,
  rows:
   [ 
        {
          "el_id": 3,
          "el_alias": "some3@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 0,
        },
        {
          "el_id": 2,
          "el_alias": "some2@somealias.hu",
          "el_destination": "insider@somealias.hu",
          "el_verified": 1,
        },
        {
          "el_id": 1,
          "el_alias": "some1@somealias.hu",
          "el_destination": "whatever@whatever.com",
          "el_verified": 1,
        },
    ] })

                 done()

             })

        })

        it('calling the new limit only route', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/aliases/limit", function(err, result, httpResponse){
                 // console.log("here",err, result)
                 assert.propertyVal(result, "canBeAdded", false);
                 assert.equal(result.max, 3);
                 assert.equal(result.count, 3);

                 done()

             })

        })

        it('verifying an account', function(done) {
           var aliasId = aliasIds[2]
           //console.log("verifying", aliasId, verification_codes, verification_codes[aliasId])
                     mapi.post( "/domains/domain/"+domain_name+"/alias/"+aliasId+"/verify", {"token": verification_codes[aliasId]}, function(err, result, httpResponse){
                        assert.equal(result, "ok")
                        done()
                     })
        })

        it('querying that account should now be verified', function(done) {
            var aliasId = aliasIds[2]
                     mapi.get( "/domains/domain/"+domain_name+"/alias/"+aliasId, function(err, result, httpResponse){
                        assert.propertyVal(result, "el_verified", 1)
                        done()
                     })
        })


        it('get aliases list grouped by webhosting', function(done) {

             mapi.post( "/aliases/list", {account: common.account_id}, function(err, result, httpResponse){
                 // console.log(require("util").inspect(result, false, null))
                 assert.isNull(err)
                 assert.property(result, "aliases")
                 Object.keys(result.aliases).forEach(x=>{
                    Object.keys(result.aliases[x]).forEach(y=>{
                      result.aliases[x][y].forEach(z=>{
                        assert.property(z, "created_at")
                        assert.property(z, "updated_at")
                      })
                    })
                 })
                 // console.log(result.aliases)

                 done()

             })


        })


        it('get list of aliases via the webhosting route', function(done) {
             mapi.get( "/webhostings/"+common.expected_webhosting+"/aliases",function(err, result, httpResponse){

                assert.property(result, "aliases")

                done()

             })

        })


        it('removing aliases', function(done) {

            var aliasesRemoved = 0
            aliasIds.forEach(aliasId=>{
                     mapi.delete( "/domains/domain/"+domain_name+"/alias/"+aliasId, {}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.equal(result, "ok")

                        aliasesRemoved++
                        if(aliasesRemoved >= aliasIds.length)
                          done()

                     })
            })

        })    





        shouldBeEmpty()
        shouldBeEmptyDomainLevel()
    })

  function canBeAdded(what){
      common.canBeAdded("aliases", domain_name, what)
  }

  function shouldBeEmpty(){
        it('get aliases should be empty globally', function(done) {

             mapi.get( "/aliases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {aliases:[]})
                done()

             })

        })    
  }

  function shouldBeEmptyDomainLevel(){
        it('get aliases should be empty for the target domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/aliases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "aliases")
                assert.deepEqual(result.aliases, [])
                done()

             })

        })    

  }

  function addAlias(email_user, destination, verified){

        it('adding an alias should be fine: '+email_user, function(done) {

             common.setupMonsterInfoWebhosting(app)

             var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.propertyVal(data, "template", "email/verify-alias")
                     assert.property(data, "context")
                     Array("el_id", "el_alias", "el_destination", "el_verification_token", "el_verification_token_expires").forEach(q=>{
                       assert.property(data.context, q)
                     })
                     verification_codes[data.context.el_id] = data.context.el_verification_token
                     // console.log("crap", data, verification_codes)

                     return Promise.resolve("ok")
                 }
                 return re
             }


             var params = {
                 "account": common.account_id, 
                 "el_alias": email_user+"@"+domain_name, 
                 "el_destination": destination || "whatever@whatever.com",
                }

             if(typeof verified != "undefined")
                 params.el_verified = verified

             mapi.put( "/domains/domain/"+domain_name+"/aliases", params,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.property(result, "el_id")
                app.GetMailer = oGetMailer
                done()

             })

        })    
  }



}, 10000)

