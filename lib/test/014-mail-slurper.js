require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const slurperLib = require('../lib-mail-slurper.js');

    const dotq = require("MonsterDotq");
    const fs = dotq.fs();

	describe('basic tests', function() {

        it("legit mails should be sent nicely", function() {

             const pwd = 123;
             const storage = 16384;
             const svcEmail = "mailer@"+storage+".mocha.svc";
             const fileToSlurp = "test/email1.txt";

             var callChain = [];

             const slurper = slurperLib(app, {
                 smtp_client_config: {foo: "bar"},
                 accounts: {
                    GetAccountByEmail: function(email){
                        callChain.push("GetAccountByEmail");
                        assert.equal(email, svcEmail);
                        return Promise.resolve({ea_password: pwd});
                    }
                 },
                 fs: {
                    statAsync: function(path){
                         callChain.push("statAsync");
                         assert.equal(path, fileToSlurp);
                         return fs.statAsync(path);

                    },
                    createReadStream: function(path){
                         callChain.push("createReadStream");
                         assert.equal(path, fileToSlurp);
                         return fs.createReadStream(path);

                    },
                    ensureRemovedAsync: function(path){
                        callChain.push("ensureRemovedAsync");
                        assert.equal(path, fileToSlurp);
                        return Promise.resolve();
                    }
                 },
                 nodemailer: {
                    createTransport: function(smtp_client_config) {
                        callChain.push("createTransport");
                        assert.deepEqual(smtp_client_config, {
                                 "auth": {
                                  "pass": pwd,
                                  "user": svcEmail,
                                },
                                "foo": "bar"
                          });
                        return {
                            sendMail: function(msg, callback) {
                                callChain.push("sendMail");
                                assert.deepEqual(msg.envelope, {
                                            "from": [
                                              "info@femforgacs.hu"
                                            ],
                                            "cc": [],
                                            "bcc": [],
                                            "php": storage+":/bm.monstermedia.hu/pages/hacktest/index.php",
                                            "subject": "Femforgacs - Moderalando hir",
                                            "to": [
                                              "radimre83@gmail.com"
                                            ]
                                })
                                callback(null, "ok");
                            }
                        }
                    }
                 }
             });
             return slurper.processFile(fileToSlurp, true)
               .then(result=>{
                  assert.deepEqual(callChain, [
                        "statAsync",
                        "createReadStream",
                        "GetAccountByEmail",
                        "createTransport",
                        "createReadStream",
                        "sendMail",
                        "ensureRemovedAsync",
                  ]);
                  assert.deepEqual(result, {status:"sent", storage: ""+storage})
               })
        })


        it("mails with no php header line should be rejected", rejectLogic("test/email2.txt"));

        it("mails with multiple php header lines should be rejected", rejectLogic("test/email3.txt"));

        it("mails with no braces shall return the form field correctly", function(){
             var fullStr = fs.readFileSync("test/email4.txt");
             const smtpLib = require('../email-smtp-server.js');
             var m = smtpLib.parseMailHeaders(fullStr);
             assert.equal("szalay.andras@iroda-raktar.hu", m);
        })

        it("the from header shall have low priority", function(){
             var fullStr = fs.readFileSync("test/email5.txt");
             const smtpLib = require('../email-smtp-server.js');
             var m = smtpLib.parseMailHeaders(fullStr);
             assert.equal("tech@monstermedia.hu", m);
        })

        it("email addresses wrapped in special symbols", function(){
             var fullStr = fs.readFileSync("test/email6.txt");
             const smtpLib = require('../email-smtp-server.js');
             var m = smtpLib.parseMailHeaders(fullStr);
             assert.equal("szalay.andras@iroda-raktar.hu", m);
        })

        function rejectLogic(fileToSlurp){
            return function(){

             var callChain = [];

             const slurper = slurperLib(app, {
                 smtp_client_config: {foo: "bar"},
                 fs: {
                    statAsync: function(path){
                         callChain.push("statAsync");
                         assert.equal(path, fileToSlurp);
                         return fs.statAsync(path);

                    },
                    createReadStream: function(path){
                         callChain.push("createReadStream");
                         assert.equal(path, fileToSlurp);
                         return fs.createReadStream(path);

                    },
                    renameAsync: function(path1, path2){
                         callChain.push("renameAsync");
                         assert.equal(path1, fileToSlurp);
                         assert.equal(path2, fileToSlurp+".err");
                         return Promise.resolve();
                    }
                 },
             });
             return slurper.processFile(fileToSlurp, true)
               .then(result=>{
                  assert.deepEqual(callChain, [
                        "statAsync",
                        "createReadStream",
                        "renameAsync",
                  ]);
                  // console.log(result);
                  assert.propertyVal(result, "status", "error");
               })
            }
        }

    })


}, 20000)

