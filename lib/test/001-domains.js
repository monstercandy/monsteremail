require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name_1 = "somewhat1.hu"
    const domain_name_2 = "somewhat2.hu"

    const whitelisted_domain = "something.demo.foobar.hu"

	describe('basic tests', function() {

        shouldBeEmpty()

        common.createDomain(domain_name_1)

        it('adding the same domain with the same webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: common.expected_webhosting,
                    do_domain_name: domain_name_1
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })

        it('adding the same domain with the no webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: 0,
                    do_domain_name: domain_name_1
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })

        it('adding the same domain with the different webhosting id should be rejected', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: common.unexpected_webhosting,
                    do_domain_name: domain_name_1
                },
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "DOMAIN_IS_ALREADY_ATTACHED_TO_WEBHOSTING")
                    done()
                })

        })


        it('adding a new domain with the zero webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: 0,
                    do_domain_name: domain_name_2
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })

        it('adding the same domain with the no webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: 0,
                    do_domain_name: domain_name_2
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })

        it('adding the same domain with the new webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: common.expected_webhosting,
                    do_domain_name: domain_name_2
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })

        it('adding the same domain with the same webhosting id should be accepted', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: common.expected_webhosting,
                    do_domain_name: domain_name_2
                },
                function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()
                })

        })


        it('adding the same domain with the new webhosting id should be rejected', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/",
                {
                    account: common.account_id,
                    do_webhosting: common.unexpected_webhosting,
                    do_domain_name: domain_name_2
                },
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "DOMAIN_IS_ALREADY_ATTACHED_TO_WEBHOSTING")
                    done()
                })

        })

        common.deleteDomain(domain_name_2)



        it('get domains list should show the first one only', function(done) {

             mapi.get( "/domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "domains")
                result.domains.forEach(domain=>{
                     assert.property(domain,"created_at")
                     delete domain.created_at
                     assert.property(domain,"updated_at")
                     delete domain.updated_at
                })
                assert.deepEqual(result.domains, [
                    {
                      "do_active": 1,
                      "do_suspended": 0,
                      "do_domain_name": "somewhat1.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20001,
                    }
                  ])
                done()

             })

        })

        it('searching that domain should find it', function(done) {

             mapi.search( "/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(result, "do_domain_name", domain_name_1)
                done()

             })

        })

        it('searching a domain that is not present should return false', function(done) {

             mapi.search( "/domains/domain/xxcdfdsdf.hu",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, false)
                done()

             })

        })

        it('getting the domain by name', function(done) {

             mapi.get( "/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                assert.propertyVal(result, "do_domain_name", domain_name_1)
                assert.propertyVal(result, "do_active", 1)
                done()

             })

        })


        it('changing active to false', function(done) {

             mapi.post( "/domains/domain/"+domain_name_1, {do_active:false},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })

        })

        it('getting the domain by name, it should now be inactive', function(done) {

             mapi.get( "/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                assert.propertyVal(result, "do_active", 0)
                done()

             })

        })

        it('getting the same domain by name, through the webhosting route', function(done) {

             mapi.get( "/webhostings/"+common.expected_webhosting+"/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                assert.isNull(err)
                done()

             })

        })


        it('getting the same domain by name, through webhosting route via invalid webohsting id should be rejected', function(done) {

             mapi.get( "/webhostings/"+common.unexpected_webhosting+"/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOMAIN_NOT_FOUND")
                done()

             })

        })

        common.createDomain(domain_name_2, true)
        common.deleteDomain(domain_name_2, true)


        common.createDomain(domain_name_2)

        it('but the third one should be rejected due to quota', function(done) {

             common.setupMonsterInfoAccount(app)
             common.setupMonsterInfoWebhosting(app)

             mapi.put( "/domains/", {
                account: common.account_id,
                do_webhosting: 20001,
                do_domain_name: "somewhat3.hu"
              },function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOMAIN_LIMIT_IS_REACHED")
                done()

             })

        })

        it('relocating a domain', function(done) {
             mapi.post( "/domains/domain/"+domain_name_1+"/move", { dest_wh_id: 20020, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })


        it('get domains list should reflect it', function(done) {

             mapi.get( "/domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "domains")
                result.domains.forEach(domain=>{
                     assert.property(domain,"created_at")
                     delete domain.created_at
                     assert.property(domain,"updated_at")
                     delete domain.updated_at
                })
                assert.deepEqual(result.domains, [
                    {
                      "do_active": 0,
                      "do_suspended": 0,
                      "do_domain_name": "somewhat1.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20020,
                    },
                    {
                      "do_active": 1,
                      "do_suspended": 0,
                      "do_domain_name": "somewhat2.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20001,
                    }
                  ])
                done()

             })

        })

        it('relocating a domain back to the original', function(done) {
             mapi.post( "/domains/domain/"+domain_name_1+"/move", {dest_wh_id: 20001, dest_u_id: common.account_id}, function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done();
             })
        })

        common.createDomain(whitelisted_domain, true, null, 'whitelisted demo domains should still be possible to be added')


        it('get domains list should show the ones added', function(done) {

             mapi.get( "/domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "domains")
                result.domains.forEach(domain=>{
                     assert.property(domain,"created_at")
                     delete domain.created_at
                     assert.property(domain,"updated_at")
                     delete domain.updated_at
                })
                assert.deepEqual(result.domains, [
                    {
                      "do_active": 0,
                      "do_suspended": 0,
                      "do_domain_name": "somewhat1.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20001,
                    },
                    {
                      "do_active": 1,
                      "do_suspended": 0,
                      "do_domain_name": "somewhat2.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20001,
                    },
                    {
                      "do_active": 1,
                      "do_suspended": 0,
                      "do_domain_name": "something.demo.foobar.hu",
                      "do_user_ids": ["10001"],
                      "do_webhosting": 20001,
                    }
                  ])
                done()

             })

        })

        it('get domains list grouped by webhosting', function(done) {

             mapi.post( "/domains/list", {account: common.account_id}, function(err, result, httpResponse){

                 assert.isNull(err)
                 assert.property(result, "domains")
                 Object.keys(result.domains).forEach(x=>{
                    result.domains[x].forEach(y=>{
                        delete y.created_at
                        delete y.updated_at
                    })
                 })
                 assert.deepEqual(result, {domains:{ '20001':
   [ { do_user_ids: ["10001"],
       do_suspended: 0,
       do_webhosting: 20001,
       do_domain_name: 'somewhat1.hu',
       do_active: 0 },
     { do_user_ids: ["10001"],
       do_suspended: 0,
       do_webhosting: 20001,
       do_domain_name: 'somewhat2.hu',
       do_active: 1 },
     { do_user_ids: ["10001"],
       do_suspended: 0,
       do_webhosting: 20001,
       do_domain_name: 'something.demo.foobar.hu',
       do_active: 1 } ] }})

                 done()

             })


        })

        it('changing suspended to true', function(done) {

             mapi.post( "/domains/domain/"+domain_name_1, {do_suspended:true},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })

        })

        it('getting the domain by name, it should now be suspended', function(done) {

             mapi.get( "/domains/domain/"+domain_name_1,function(err, result, httpResponse){
                assert.propertyVal(result, "do_suspended", 1)
                done()

             })

        })


        common.deleteDomain(domain_name_1)
        common.deleteDomain(domain_name_2)
        common.deleteDomain(whitelisted_domain)

        shouldBeEmpty()

	})



  function shouldBeEmpty(){
        it('get domains should be empty', function(done) {

             mapi.get( "/domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "domains")
                assert.deepEqual(result.domains, [])
                done()

             })

        })
  }



}, 20000)

