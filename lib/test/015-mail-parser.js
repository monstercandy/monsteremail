require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const mailLogParserLib = require('../lib-mail-log-parser.js');

    const dotq = require("MonsterDotq");
    const fs = dotq.fs();

    var common = require("000-common.js")(mapi, assert, app)

    const wh_id = 20015;

    const domain_name = "mailparse.hu";

    const successfulLogLines = [
      "Feb  4 17:30:51 monstermedia dovecot: imap-login: Login: user=<account@mailparse.hu>, method=PLAIN, rip=1.1.1.1, lip=88.151.102.104, mpid=5326, TLS, session=<c48GeGVkYJO5HrCU>",
      "Feb  4 17:30:56 monstermedia dovecot: pop3-login: Login: user=<account@mailparse.hu>, method=PLAIN, rip=1.1.1.1, lip=88.151.102.104, mpid=5332, session=<pWBReGVkgtC8eaTR>",
    ];

	describe('basic tests', function() {

        common.createDomain(domain_name, null, wh_id)
        common.addAccount("account", domain_name, wh_id);

        successfulLogLines.forEach(line=>{
            it("successful login should be translated to a login event: "+line, function(done) {

                 var oInsertEvent = app.InsertEvent;

                 app.InsertEvent = function(req, stuff) {
                    assert.isNull(req);
                    assert.deepEqual(stuff, { e_ip: '1.1.1.1',
     e_event_type: 'auth-credential',
     e_username: 'account@mailparse.hu',
     e_user_id: '10001',
     e_other: { successful: true } });
                    app.InsertEvent = oInsertEvent;
                    done();
                 }

                 const mailLogParser = mailLogParserLib(app);
                 mailLogParser.processLine(line)
            })

        })



            it("quota exceeded messages should send an email notification", function(done) {

                 const line = "Feb  3 12:16:45 monstermedia cbpolicyd[28800]: module=Quotas, action=reject, host=89.132.197.192, helo=kgzs.hu, from=account@mailparse.hu, to=info@kissgaborzsolt.hu, reason=quota_match, policy=7, quota=4, limit=5, track=SASLUsername:inner.router@kgzs.hu, counter=MessageCount, quota=201.73/200 (100.9%)";
                 
                 var oGetMailer = app.GetMailer;

                 app.GetMailer = function() {
                    return {
                        SendMailAsync: function(p){
                            assert.deepEqual(p, { toAccount: { user_id: '10001', emailCategory: 'TECH' },
     cc: 'account@mailparse.hu',
     template: 'email/quota-exceeded',
     context: { emailAccount: 'account@mailparse.hu', quotaLimit: 200 } });
                            done();
                        }
                    }
                 }

                 const mailLogParser = mailLogParserLib(app);
                 mailLogParser.processLine(line)
            })

            it("even when parsing postfix logs and not cluebringer", function(done) {

                 const line = "Sep 21 10:14:39 eric postfix/smtpd[55]: 830031481598: reject: RCPT from unknown[192.72.190.200]: 554 5.7.1 <Thajoker578@gmail.com>: Recipient address rejected: You are allowed to send only 111 emails daily!; from=<account@mailparse.hu> to=<Thajoker578@gmail.com> proto=ESMTP helo=<[127.0.0.1]>";
                 
                 var oGetMailer = app.GetMailer;

                 app.GetMailer = function() {
                    return {
                        SendMailAsync: function(p){
                            assert.deepEqual(p, { toAccount: { user_id: '10001', emailCategory: 'TECH' },
     cc: 'account@mailparse.hu',
     template: 'email/quota-exceeded',
     context: { emailAccount: 'account@mailparse.hu', quotaLimit: 111 } });
                            done();
                        }
                    }
                 }

                 const mailLogParser = mailLogParserLib(app);
                 mailLogParser.processLine(line)
            })

            it("unsuccessful login should be translated to an unsuccessful login event", function(done) {

                 const line = "Feb  4 17:30:47 monstermedia postfix/smtpd[2821]: warning: 220.ip-54-37-74.eu[54.37.74.220]: SASL LOGIN authentication failed: UGFzc3dvcmQ6";

                 var oInsertEvent = app.InsertEvent;

                 app.InsertEvent = function(req, stuff) {
                    assert.isNull(req);
                    assert.deepEqual(stuff, { e_ip: '54.37.74.220',
     e_event_type: 'auth-credential',
     e_other: { successful: false } });
                    app.InsertEvent = oInsertEvent;
                    done();
                 }

                 const mailLogParser = mailLogParserLib(app);
                 mailLogParser.processLine(line)
            })

    })


}, 20000)

