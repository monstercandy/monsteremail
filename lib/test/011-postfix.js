require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert, app)

    const fs = require("fs")



    describe("postfix config", function(){

        it("should list the files correctly", function(){

             return mapi.postAsync( "/postfix/config/list", {})
               .then(function(res){
                  assert.deepEqual(res.result, [ "01-always.cf" ])
               })
        })


        it("fetching it should be possible", function(){

             return mapi.postAsync( "/postfix/config/fetch", {filename: "01-always.cf"})
               .then(function(res){
                  assert.deepEqual(res.result, { data: 'foobar\r\n' })
               })
        })

    })


	describe('delete messages by email', function() {

        var msgsToBeParsed = fs.readFileSync("test/mailq1.txt")

        it('deleting mails by recipient', function(done) {

             var commands = 0

             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    //  console.log("!!!", cmd, params)
                    commands++
                    if(commands == 1) {
                        assert.deepEqual(cmd, { dontLog_stdout: true,
  removeImmediately: true,
  chain: { executable: '/usr/sbin/postqueue', args: [ '-p' ] } })

                        return Promise.resolve({id:"foolist", executionPromise: Promise.resolve({output: msgsToBeParsed})})
                    }
                    else
                    if(commands == 2) {
                        assert.deepEqual(cmd, { chain:
   { executable: '/usr/sbin/postsuper',
     args: [ '-d', '[mail_id]' ],
     stdin: 'BADD5B08CA0\n' } })
                        assert.deepEqual(params, [ { mail_id: '-' } ])

                        return Promise.resolve({id:"foodel", executionPromise: Promise.resolve({output: "hello"})})
                    }
                    else
                        throw new Error("Unexpected number of commands")

                }
             }

             mapi.delete( "/postfix/mailq/by/sender",{emails:["toth.zsuzsanna@iroda-raktar.hu"]}, function(err, result, httpResponse){
                // console.log("here", err, result)
                assert.propertyVal(result, "id", "foodel")
                app.commander = oCommander
                assert.equal(commands, 2)
                done()
             })

        })

        it('listing entries by domain', function(done) {

             var commands = 0

             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    //  console.log("!!!", cmd, params)
                    commands++
                    if(commands == 1) {
                        assert.deepEqual(cmd, { dontLog_stdout: true,
  removeImmediately: true,
  chain: { executable: '/usr/sbin/postqueue', args: [ '-p' ] } })

                        return Promise.resolve({id:"foolist", executionPromise: Promise.resolve({output: msgsToBeParsed})})
                    }
                    else
                        throw new Error("Unexpected number of commands")

                }
             }

             mapi.get( "/postfix/mailq/by/domain/easymail.hu", function(err, result, httpResponse){
                // console.log("here", err, result)
                assert.deepEqual(result, [ { mail_id: '1720D258EC06',
    starred: false,
    size: 27652,
    ts: 'Fri May 26 19:00:08',
    sender: 'postmaster@femforgacs.hu',
    status: '(Host or domain name not found. Name service error for name=easymail.hu type=MX: Host not found, try again)',
    recipient: 'snecy@easymail.hu' },
  { mail_id: '4EE3E83BE1B',
    starred: false,
    size: 27665,
    ts: 'Thu May 25 09:01:09',
    sender: 'postmaster@femforgacs.hu',
    status: '(Host or domain name not found. Name service error for name=easymail.hu type=MX: Host not found, try again)',
    recipient: 'snecy@easymail.hu' } ])
                app.commander = oCommander
                assert.equal(commands, 1)
                done()
             })

        })


        it('listing entries by webhosting', function(done) {

             var commands = 0

             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    //  console.log("!!!", cmd, params)
                    commands++
                    if(commands == 1) {
                        assert.deepEqual(cmd, { dontLog_stdout: true,
  removeImmediately: true,
  chain: { executable: '/usr/sbin/postqueue', args: [ '-p' ] } })

                        return Promise.resolve({id:"foolist", executionPromise: Promise.resolve({output: msgsToBeParsed})})
                    }
                    else
                        throw new Error("Unexpected number of commands")

                }
             }

             mapi.get( "/postfix/mailq/by/webhosting/99999", function(err, result, httpResponse){
                // console.log("here", err, result)
                assert.deepEqual(result, [ ])
                app.commander = oCommander
                assert.equal(commands, 1)
                done()
             })

        })

    })

    describe('managing autocleanup', function() {
        var payload = {mc_email:"email@addre.ss", mc_type:"email"}
        it("should be empty", function(done){

             mapi.get( "/postfix/mailq-auto",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()
             })
        })


        it("inserting", function(done){

             mapi.put( "/postfix/mailq-auto", payload, function(err, result, httpResponse){
                assert.property(result, "mc_id")
                payload.mc_id = result.mc_id
                assert.ok(payload.mc_id)
                done()
             })
        })

        it("should be present", function(done){

             mapi.get( "/postfix/mailq-auto",function(err, result, httpResponse){
                assert.deepEqual(result, [payload])
                done()
             })
        })

        it("can be deleted", function(done){

             mapi.delete( "/postfix/mailq-auto/"+payload.mc_id, {},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it("should be empty", function(done){

             mapi.get( "/postfix/mailq-auto",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()
             })
        })


    })


}, 10000)

