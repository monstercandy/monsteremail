require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const wh_id = 20013;

    const domain_name1 = "limit1.hu";
    const domain_name2 = "limit2.hu";
    const domain_name3 = wh_id+app.config.get("service_account_tld");

    
    describe('basic tests', function() {

        common.createDomain(domain_name1, null, wh_id)
        common.createDomain(domain_name2, null, wh_id)
        common.createDomain(domain_name3, null, wh_id)

        shouldBeEmptyDomainLevel()

        common.addAccount("account1", domain_name1, wh_id);
        common.addAccount("account2", domain_name1, wh_id);
        canBeAdded(true);
        common.addAccount("account3", domain_name2, wh_id);

        canBeAdded(false);

        it('adding more accounts should be rejected', function(done) {

             var email_full = "account4@"+domain_name2;

             var params = {
                 "account": common.account_id,
                 "ea_email": email_full,
                 "ea_password": common.default_password,
                 "ea_quota_bytes": common.validQuota,
                }

             mapi.put( "/domains/domain/"+domain_name2+"/accounts", params,
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ACCOUNT_LIMIT_IS_REACHED");
                done()

             })

        })

        it('the service domain should be handled specially (but non-service account should still be rejected!)', function(done) {

             var email_full = "account4@"+domain_name3;

             var params = {
                 "account": common.account_id,
                 "ea_email": email_full,
                 "ea_password": common.default_password,
                 "ea_quota_bytes": common.validQuota,
                }

             mapi.put( "/domains/domain/"+domain_name3+"/accounts", params,
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "INVALID_SERVICE_ACCOUNT");
                done()

             })

        })        

        // but it should still be possible to add a correct service account
        common.addAccount("mailer", domain_name3, wh_id);

    })

  function shouldBeEmptyDomainLevel(){
     Array(domain_name1, domain_name2).forEach(domain=>{
        it('get accounts should be empty for the target domain: '+domain, function(done) {

             mapi.get( "/domains/domain/"+domain+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.deepEqual(result.accounts, [])
                done()

             })

        })

     })

  }


  function canBeAdded(what){
      common.canBeAdded("accounts", domain_name1, what)
      common.canBeAdded("accounts", domain_name2, what)
  }


}, 10000)

