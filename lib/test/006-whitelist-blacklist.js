require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "somewh.hu"

    const email_user = "foo"
    const email_full = `${email_user}@${domain_name}`

    const webhostingId = 20006

    var ea_id

    var accountIds = []


    describe('basic tests', function() {


        common.createDomain(domain_name, null, webhostingId)

        addAccount(email_user)

        shouldBeEmpty()


        addWb("some@account.hu", "W")

        addWb("@account.hu", "B")


        it('querying them', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist",

              function(err, result, httpResponse){
                assert.isNull(err)
                // console.log(result)
                assert.deepEqual(result, [ { wb_senderemail: 'some@account.hu', wb_action: 'W' },
  { wb_senderemail: '@account.hu', wb_action: 'B' } ])
                done()

             })

        })



        it('deleting a whitelist item', function(done) {


             mapi.delete( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist", 
                {
                 "wb_senderemail":"some@account.hu", 
                },

              function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(result, "ok")
                done()

             })

        })    


        it('querying them again', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist",

              function(err, result, httpResponse){
                assert.isNull(err)
                // console.log(result)
                assert.deepEqual(result, [{ wb_senderemail: '@account.hu', wb_action: 'B' } ])
                done()

             })

        })

        common.removeAccount(email_full, accountIds, 0, domain_name, "removing account", webhostingId)

        common.deleteDomain(domain_name)

    })

  function addWb(email, wb){

        it('adding an email to the whitelist: '+email+" ("+wb+")", function(done) {

             mapi.put( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist", 
                {
                 "wb_senderemail":email, 
                 "wb_action": wb || "W",
                },

              function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(result, "ok")
                done()

             })

        })    
  }

  function addAccount(email_user){

      return common.addAccount(email_user, domain_name, webhostingId, function(result){

         ea_id = result.ea_id
         accountIds.push(ea_id)
      })

  }

  function shouldBeEmpty(){
        it('querying whitelists should be empty', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist",

              function(err, result, httpResponse){
                assert.isNull(err)
                assert.deepEqual(result, [])
                done()

             })

        })

  }


}, 10000)

