require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const storage_id = 20020;

    var latestBackup;
    var getInfoCalls = 0;

     const this_server = app.config.get("mc_server_name");
     const service_account_tld = app.config.get("service_account_tld");
     const service_account_username = app.config.get("service_account_username");
     var svc_domain = storage_id+"."+this_server+service_account_tld;

    const expectedBackup = { domains:
   [ { do_domain_name: 'wie.hu',
       do_active: 1,
       do_suspended: 0,
       },
     { do_domain_name: '[[wh_id]].[[server]]'+service_account_tld,
       do_active: 1,
       do_suspended: 0,
       }
        ],
  accounts:
   [ { ea_email: 'wie@wie.hu',
       ea_quota_bytes: 10485760,
       ea_autoexpunge_days: 0,
      "ea_spam_tag2_level": null,
      "ea_spam_kill_level": null,
       ea_suspended: 0,
       // ea_maildir_path: "/web/mails/[[wh_id]]/wie@wie.hu",
       Policy: "WiePolicy",
       tally_bytes: 123,
       tally_messages: 111,
        },
          {
            "ea_autoexpunge_days": 0,
            "ea_spam_tag2_level": null,
            "ea_spam_kill_level": null,
            "ea_email": "mailer@[[wh_id]].[[server]]"+service_account_tld,
            ea_suspended: 0,
            // "ea_maildir_path": "/web/mails/[[wh_id]]/"+service_account_username+"@[[wh_id]].[[server]].svc",
            "ea_quota_bytes": 10485760,
          }
         ],
  aliases:
   [ { el_alias: 'wie@wie.hu',
       el_destination: 'whatever@whatever.com',
       el_verified: 0,
       el_verification_ts: null, } ],
  bccs:
   [ { bc_alias: 'wie@wie.hu',
       bc_destination: 'whatever@whatever.com',
       bc_direction: 'IN',
       bc_verified: 0,
       bc_verification_ts: null, } ] };

   const domain_name = "wie.hu";
   const email_user = "wie";
   const email = email_user+"@"+domain_name;
   var ea_id;
   var svc_ea_id;



	describe('preparation', function() {


        common.setupMonsterInfoAccount(app)
        common.setupMonsterInfoWebhosting(app)

        common.createDomain(domain_name, false, storage_id)
        common.createDomain(svc_domain, false, storage_id)

        common.addAccount(email_user, domain_name, storage_id, (result)=>{
           ea_id = result.ea_id;
        })

        common.addAccount(service_account_username, svc_domain, storage_id, (result)=>{
           svc_ea_id = result.ea_id;
        })

        it("reading back the service account", function(done){

             mapi.post( "/accounts/mail-service-account/fetch", {wh_id: storage_id}, function(err, result, httpResponse){
                 assert.ok(result.ea_password);
                 delete result.ea_password;
                 assert.deepEqual(result, { ea_email: 'mailer@20020.mocha.svc' });

                 done()

             })
        })

        it("configuring tallies", function(done){

             mapi.post( "/domains/domain/"+domain_name+"/account/"+ea_id+"/tallies/set", {messages: 111, bytes: 123},
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                done()

             })
        })

        it('adding an alias should be fine: '+email_user, function(done) {

             var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     return Promise.resolve("ok")
                 }
                 return re
             }


             var params = {
                 "account": common.account_id,
                 "el_alias": email,
                 "el_destination": "whatever@whatever.com",
                }

             if(typeof verified != "undefined")
                 params.el_verified = verified

             mapi.put( "/domains/domain/"+domain_name+"/aliases", params,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.property(result, "el_id")
                app.GetMailer = oGetMailer
                done()

             })

        })


        it('adding a bcc should be fine: '+email_user, function(done) {

             common.setupMonsterInfoWebhosting(app)

             var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     return Promise.resolve("ok")
                 }
                 return re
             }


             var params = {
                 "account": common.account_id,
                 "bc_alias": email,
                 "bc_destination": "whatever@whatever.com",
                 "bc_direction":"IN",
                }

             if(typeof verified != "undefined")
                 params.bc_verified = verified

             mapi.put( "/domains/domain/"+domain_name+"/bccs", params,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.property(result, "bc_id")
                app.GetMailer = oGetMailer
                done()

             })

        })


        it('adding an email to the whitelist (W)', function(done) {

             mapi.put( "/domains/domain/"+domain_name+"/account/"+ea_id+"/whiteblacklist",
                {
                 "wb_senderemail":"stuff@stuff.hu",
                 "wb_action": "W",
                },

              function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(result, "ok")
                done()

             })

        })


        it('adding one more policy for more sophiscated tests', function(done) {

             mapi.put( "/cluebringer/policies/outbound",{"Name":"WiePolicy", "Description": "something", Quotas:[{ Interval: 'hourly',
    Message: 'You are allowed to send only 2000 emails hourly!',
    Limit: 2000 }]},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })


        it('assigning an email account to this non-default policy', function(done) {

             mapi.put( "/cluebringer/policies/outbound/WiePolicy/members",{ ea_email: email},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })
	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

        cleanup();

        it('get list of accounts should be empty', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "DOMAIN_NOT_FOUND");
                done();

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork(true);

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })

        })
  }


  function cleanup(){
        common.setupRelayerForFilemanRemove(email, storage_id);
        common.deleteDomain(domain_name);
  }

  function backupShouldWork(swap){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                 console.log("wie!", result)

                latestBackup = simpleCloneObject(result);

                var actual = result;

                Array("accounts","aliases","bccs","domains").forEach(cat=>{
                   actual[cat].forEach(row=>{
                       Array("created_at", "updated_at").forEach(x=>{
                          assert.ok(row[x]);
                          delete row[x];
                       })

                   })
                })

                   actual.accounts.forEach(row=>{
                       Array("ea_password").forEach(x=>{
                          assert.ok(row[x]);
                          delete row[x];
                       })

                   })

                   actual.aliases.forEach(row=>{
                       Array("el_verification_token","el_verification_token_expires").forEach(x=>{
                          assert.ok(row[x]);
                          delete row[x];
                       })

                   })

                   actual.bccs.forEach(row=>{
                       Array("bc_verification_token","bc_verification_token_expires").forEach(x=>{
                          assert.ok(row[x]);
                          delete row[x];
                       })

                   })

                if(swap) {
                   doSwap(actual.accounts, 0, 1);
                   doSwap(actual.domains, 0, 1);
                }

                assert.deepEqual(actual, expectedBackup);
                done()


                function doSwap(arr, i1, i2) {
                   var x = arr[i1];
                   arr[i1] = arr[i2];
                   arr[i2] = x;
                }

             })

        })
  }


}, 10000)

