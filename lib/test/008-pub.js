require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "somepub.hu"
    const email_user = "account1"
    const email_full = `${email_user}@${domain_name}`
    const raw_password = "crap";
    const email_user_raw = "rawpassword";
    const email_full_raw = email_user_raw+"@"+domain_name;

    const webhostingId = 20008

    const new_password = "new_password"

    var accountIds = []

    describe('basic tests', function() {

        common.createDomain(domain_name, null, webhostingId)

        addAccount(email_user);
        addAccount(email_user_raw, "{CRYPT}bnvaWpDDhX2VY"); // this is raw_password

        it('calling an unknown route should still enforce authentication (sending a request without password)', function(done) {

             mapi.post( "/pub/foobar", {ea_email: email_full}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        it('calling an unknown route should still enforce authentication (sending a request with a non string password)', function(done) {

             mapi.post( "/pub/foobar", {ea_email: email_full, ea_password: []}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })

        it('calling an unknown route should still enforce authentication (sending invalid password)', function(done) {

             mapi.post( "/pub/foobar", {ea_email: email_full, ea_password: common.default_password+"X"}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "AUTH_FAILED")
                done()

             })

        })

        it('calling test with correct password', function(done) {

             mapi.post( "/pub/test", {ea_email: email_full, ea_password: common.default_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('calling test2 with correct password', function(done) {

             mapi.post( "/pub/test2", {ea_email: email_full, ea_password: common.default_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, { ea_autoexpunge_days: 0,
  ea_spam_tag2_level: null,
  ea_spam_kill_level: null })
                done()

             })

        })

        it('calling test with correct password of a legacy password account', function(done) {

             mapi.post( "/pub/test", {ea_email: email_full_raw, ea_password: raw_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('calling change password with invalid password', function(done) {

             mapi.post( "/pub/change-password", {ea_email: email_full, ea_password: common.default_password+"X"}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "AUTH_FAILED")
                done()

             })

        })

        it('calling change password with correct password but without a valid new one', function(done) {

             mapi.post( "/pub/change-password", {ea_email: email_full, ea_password: common.default_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "NOTHING_TO_CHANGE")
                done()

             })

        })

        it('calling change password with correct password but with a valid new one', function(done) {

             mapi.post( "/pub/change-password", {ea_email: email_full, ea_password: common.default_password, new_password: new_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })


        it('calling test with the old password should fail', function(done) {

             mapi.post( "/pub/test", {ea_email: email_full, ea_password: common.default_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "AUTH_FAILED")
                done()

             })

        })

        it('calling test with the new password should be ok', function(done) {

             mapi.post( "/pub/test", {ea_email: email_full, ea_password: new_password}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })



        it('teaching spam assassin should invoke the correct command', function(done) {

             const id = "something"
             var oCommander = app.commander
             app.commander = {
                spawn: function(cmd, params) {
                    //  console.log("!!!", cmd, params)
                    assert.deepEqual(cmd, { upload: true, chain:
   { executable: '[salearn_path]',
     args: [ '--[spam_or_ham]', '--username=[email]' ], } })

                     assert.deepEqual(params, [ app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), { spam_or_ham: 'ham',
    email: email_full } ])
                    return Promise.resolve({id:id, executionPromise: Promise.resolve()})
                }
             }

             mapi.post( "/pub/salearn",
                {ea_email: email_full, ea_password: new_password, spam_or_ham:"ham"},
                function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, {id:id})
                app.commander = oCommander
                done()

             })

        })

        common.removeAccount(email_full, accountIds, 0, domain_name, "removing account", webhostingId)


    })


  function addAccount(email_user, rawPassword){

      return common.addAccount(email_user, domain_name, webhostingId, function(r){
         accountIds.push(r.ea_id)
      }, rawPassword)

  }



}, 10000)

