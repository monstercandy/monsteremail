require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "someclue1.hu"
    const webhostingId = 20010
    const email_prefix = "foo"
    const email = email_prefix+"@"+domain_name

    var accountId

	describe('domains should be added into cluebringer by default', function() {

        shouldNotBeThere()

        common.createDomain(domain_name, null, webhostingId)

        shouldBeThere()

        common.deleteDomain(domain_name)

        shouldNotBeThere()

	})

    describe("managing groups", function(){

        it('listing groups', function(done) {

             mapi.get( "/cluebringer/groups",function(err, result, httpResponse){
                // console.log("here",err, result)
                var group_names = result.map(x => x.Name)
                assert.deepEqual(group_names, ["internal_ips", "internal_domains", "greylist_domains"])
                done()

             })

        })

        it('putting one in there', function(done) {

             mapi.put( "/cluebringer/groups/internal_domains", {Member: "@"+domain_name},function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        shouldBeThere()

        it('and removing it', function(done) {

             mapi.delete( "/cluebringer/groups/internal_domains", {Member: "@"+domain_name},function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        shouldNotBeThere()

    })


    describe('managing policies', function() {
        it('list all outbound policies', function(done) {

             mapi.get( "/cluebringer/policies/outbound",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                delete result[0].ID
                assert.propertyVal(result[0], "DefaultPolicy", true)
                done()

             })

        })

        it('list quotas for the default outbound policy', function(done) {
             mapi.get( "/cluebringer/policies/outbound/SASL/quotas",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ { Interval: 'hourly',
    Message: 'You are allowed to send only 200 emails hourly!',
    Limit: 200 },
  { Interval: 'daily',
    Message: 'You are allowed to send only 200 emails daily!',
    Limit: 200 } ])
                done()

             })

        })


        var good_payload = {"Name":"Choosen_Ones", "Description": "Something", Quotas:[
                {Message:"No!", Interval: "daily", Limit: 1000},
                {Interval: 3600, "Limit": 500}
                ]};

        it('adding a new policy', function(done) {

             mapi.put( "/cluebringer/policies/outbound",good_payload,
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('adding the same policy again should return ALREADY_EXISTS', function(done) {

             mapi.put( "/cluebringer/policies/outbound",good_payload,
               function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })

        it('list all outbound policies', function(done) {

             mapi.get( "/cluebringer/policies/outbound",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 2)
                delete result[0].ID
                delete result[1].ID
                assert.deepEqual(result, [{ Name: 'SASL',
    Priority: 30,
    Description: 'Default policy for SASL authenticated users',
    DefaultPolicy: true },{
    Name: 'Choosen_Ones',
    Priority: 10,
    Description: 'Something',
    DefaultPolicy: false }])
                done()

             })

        })

        it('list quotas for the policy just added', function(done) {
             mapi.get( "/cluebringer/policies/outbound/Choosen_Ones/quotas",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ { Interval: 'daily',
    Message: 'No!',
    Limit: 1000 },
  { Interval: 'hourly',
    Message: 'Max number of emails / 3600s: 500',
    Limit: 500 } ])
                done()

             })

        })

        it('changing quotas of the default policy', function(done) {

             mapi.post( "/cluebringer/policies/outbound/SASL/quotas",[ { Interval: 3000,
    Message: 'You are allowed to send only 100 emails hourly!',
    Limit: 100 },
  { Interval: 'daily',
    Message: 'You are allowed to send only 100 emails daily!',
    Limit: 100 } ],
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })


        it('should be reflected', function(done) {
             mapi.get( "/cluebringer/policies/outbound/SASL/quotas",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ { Interval: 3000,
    Message: 'You are allowed to send only 100 emails hourly!',
    Limit: 100 },
  { Interval: 'daily',
    Message: 'You are allowed to send only 100 emails daily!',
    Limit: 100 } ])
                done()

             })

        })

        it('changing quotas of the Chosen Ones policy', function(done) {

             mapi.post( "/cluebringer/policies/outbound/Choosen_Ones/quotas",[ { Interval: 'hourly',
    Message: 'You are allowed to send only 2000 emails hourly!',
    Limit: 2000 } ],
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })


        it('should be reflected', function(done) {
             mapi.get( "/cluebringer/policies/outbound/Choosen_Ones/quotas",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ { Interval: 'hourly',
    Message: 'You are allowed to send only 2000 emails hourly!',
    Limit: 2000 } ])
                done()

             })

        })
    })

    describe("assigning to email accounts", function(){


        it('adding one more policy for more sophiscated tests', function(done) {

             mapi.put( "/cluebringer/policies/outbound",{"Name":"Empty", "Description": "Empty", Quotas:[{ Interval: 'hourly',
    Message: 'You are allowed to send only 2000 emails hourly!',
    Limit: 2000 }]},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        common.createDomain(domain_name, null, webhostingId)

        common.addAccount(email_prefix, domain_name, webhostingId, function(params){
              accountId= params.ea_id
        })


        it('assigning an email account to this non-default policy', function(done) {

             mapi.put( "/cluebringer/policies/outbound/Choosen_Ones/members",{ ea_email: email},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('should be visible in the group membership query', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Choosen_Ones/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [email])
                done()

             })

        })

        it('also when querying the email addresses of the domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.equal(result.accounts.length, 1)
                assert.propertyVal(result.accounts[0], "Policy", "Choosen_Ones")
                done()

             })

        })

        it("and also when querying members directly by group: choosen_ones_users", function(done){

             mapi.get( "/cluebringer/groups/choosen_ones_users",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                assert.propertyVal(result[0], "Member", "$"+email)
                done()

             })
        })

        it('assigning another policy to the same email account', function(done) {

             mapi.put( "/cluebringer/policies/outbound/Empty/members",{ ea_email: email},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('should disappear in the group membership query', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Choosen_Ones/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        it('new policy should appear in group membership query', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Empty/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [email])
                done()

             })

        })

        it('also when querying the email addresses of the domain', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.equal(result.accounts.length, 1)
                assert.propertyVal(result.accounts[0], "Policy", "Empty")
                done()

             })


        })


        it('removing policy (to be default again)', function(done) {

             mapi.delete( "/cluebringer/policies/outbound/Empty/members",{ ea_email: email},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('empty policy should not list the email anymore', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Empty/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        it('Choosen Ones neither', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Choosen_Ones/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        it('assigning Emtpy policy to the same email account again', function(done) {

             mapi.put( "/cluebringer/policies/outbound/Empty/members",{ ea_email: email},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })



        it('removing default policy should be rejected', function(done) {

             mapi.delete( "/cluebringer/policies/outbound/SASL",{},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "DEFAULT_POLICY_CANNOT_BE_REMOVED")
                done()

             })

        })


        it('but removing Empty policy should be ok', function(done) {

             mapi.delete( "/cluebringer/policies/outbound/Empty",{},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('Empty policy should not be found', function(done) {

             mapi.get( "/cluebringer/policies/outbound/Empty/members",
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "GROUP_NOT_FOUND")
                done()

             })

        })

        it('and the Policy field should have disappeared from email account properties', function(done) {

             mapi.get( "/domains/domain/"+domain_name+"/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "accounts")
                assert.equal(result.accounts.length, 1)
                assert.notProperty(result.accounts[0], "Policy")
                done()

             })


        })



        it('flushing tracked emails that belong to the Choosen Ones policy', function(done) {

             mapi.post( "/cluebringer/policies/outbound/Choosen_Ones/flush",{},
               function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        var originalPolicy1 = { 
    Description: 'Default policy for SASL authenticated users',
    Name: 'SASL',
    Quotas:
     [ { Interval: 3000,
         Message: 'You are allowed to send only 100 emails hourly!',
         Limit: 100 },
       { Interval: 'daily',
         Message: 'You are allowed to send only 100 emails daily!',
         Limit: 100 } ] };

         var originalPolicy2 = { 
    Description: 'Something',
    Name: 'Choosen_Ones',
    Quotas:
     [ { Interval: 'hourly',
         Message: 'You are allowed to send only 2000 emails hourly!',
         Limit: 2000 } ] };

        const expectedPolicyBackup = [ originalPolicy1, originalPolicy2 ];

        it('backuping the policies', function(done) {

             mapi.post( "/cluebringer/policies/backup",{},
               function(err, result, httpResponse){
                // console.log(require("util").inspect(result, false, null))
                assert.deepEqual(result, expectedPolicyBackup)
                done()

             })

        })


        const additionalStuff = { 
                Description: 'restored desc',
                Name: 'restored',
                Quotas:
                 [ { Interval: 'hourly',
                     Message: 'You are allowed to send only 9999 emails hourly!',
                     Limit: 9999 } ] };

        it('merging some additional policies', function(done) {

             mapi.post( "/cluebringer/policies/merge", [
                 {
                    Description: "Existing policies should not be touched",
                    Name: 'SASL',
                 },
                 additionalStuff
             ],
               function(err, result, httpResponse){
                assert.equal(result, "ok");
                done();

             })

        })

        const expectedPolicyBackup2 = [ originalPolicy1, originalPolicy2, additionalStuff ];

        it('backuping the policies again, the new one should be listed', function(done) {

             mapi.post( "/cluebringer/policies/backup",{},
               function(err, result, httpResponse){
                // console.log(require("util").inspect(result, false, null))
                assert.deepEqual(result, expectedPolicyBackup2)
                done()

             })

        })

    })


    describe("greylisting", function(){

        queryGreylist(false)

        doGreylist(true)

        queryGreylist(true)

        it("should be listed via direct group lookup", function(done){
             mapi.get( "/cluebringer/groups/greylist_domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(findDomain(result))
                done()

             })

        })

        doGreylist(true)

        queryGreylist(true)

        doGreylist(false)

        queryGreylist(false)

        doGreylist(false)

        queryGreylist(false)

    })


    describe("per account sessions/quotas", function(){

        it('listing cluebringer sessions', function(done) {

             mapi.get( "/webhostings/"+webhostingId+"/domains/domain/"+domain_name+"/account/"+accountId+"/cluebringer/sessions",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ ])
                done()

             })

        })

        it('listing cluebringer quotas', function(done) {

             mapi.get( "/webhostings/"+webhostingId+"/domains/domain/"+domain_name+"/account/"+accountId+"/cluebringer/quotas",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [ ])
                done()

             })

        })

    })

    function shouldBeThere(){
        it('domain should be among the internal ones after creating', function(done) {

             mapi.get( "/cluebringer/groups/internal_domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(findDomain(result))
                done()

             })

        })
    }


    function shouldNotBeThere(){
        it('domain should not be among the internal ones', function(done) {

             mapi.get( "/cluebringer/groups/internal_domains",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.notOk(findDomain(result))
                done()

             })

        })

    }

    function findDomain(resultSet, domain){
        domain = "@" + (domain || domain_name)
        for(var q of resultSet){
            if(q.Member == domain)
                return q
        }
    }


    function queryGreylist(expectedStatus){
        it('getting domains by name should include greylisting status: '+expectedStatus, function(done) {

             mapi.search( "/domains/domain/"+domain_name,function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(result, "Greylisted", expectedStatus)
                done()

             })

        })

    }

    function doGreylist(newStatus){
        it('setting new greylist status: '+newStatus, function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/greylisting",{active:newStatus},function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })
        })

    }


}, 10000)

