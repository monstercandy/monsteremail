module.exports = function(mapi, assert, app, request) {



  var re = {}

  re.account_id = '10001'

  re.expected_webhosting = 20001
  re.unexpected_webhosting = re.expected_webhosting-1

  re.default_password = "12345678"

  re.validQuota = 10*1024*1024

  re.createDomain = function(domain, webhostingRoute, webhostingId, desc){

        it(desc || ('adding a domain'+(webhostingRoute?" through webhosting route":"")+': '+domain), function(done) {

             re.setupMonsterInfoAccount(app)
             re.setupMonsterInfoWebhosting(app)

             mapi.put( (webhostingRoute?"/webhostings/"+(webhostingId||re.expected_webhosting):"") + "/domains/",
               {
                  account: re.account_id,
                  do_webhosting: typeof webhostingId != "undefined" ? webhostingId : re.expected_webhosting,
                  do_domain_name: domain
               },
               function(err, result, httpResponse){
                  assert.equal(result, "ok")
                  done()
               }
             )

        })

  }


  re.setupMonsterInfoAccount = function(){

        app.MonsterInfoAccount = {
            GetInfo: function(account_id){
                assert.equal(account_id, re.account_id)
                return Promise.resolve({"u_id":re.account_id,"u_locale": "hu"})
            }
        }

  }


  re.setupMonsterInfoWebhosting = function(){

        app.MonsterInfoWebhosting = {
            GetInfo: function(d){
                // console.log("xxxxx",d)
                assert.ok(d >= 20001 && d <= 20020)

                return Promise.resolve(
                {
                   wh_id: d,
                   wh_user_id: re.account_id,
                   wh_is_expired: false,
                   wh_server: "dbstest_withmysqlrole",
                   wh_max_execution_second: 30,
                   wh_php_fpm_conf_extra_lines: "line1\nline2\n",
                   wh_name: "some name",
                   wh_template: "WEB10000",
                   wh_template_override: "{\"t_storage_max_quota_hard_mb\":20000}",
                   wh_php_version: "5.6",
                   template: {
                    t_domains_max_number_attachable:2,
                    t_email_max_number_of_aliases:3,
                    t_email_max_number_of_accounts:3,
                    t_email_max_number_of_bccs: 3,
                    t_storage_max_quota_soft_mb: 100,
                   },
                   extras: {
                     mail_path: "/web/mails/"+d
                   }
                })
            }
        }

  }


  re.deleteDomain = function(domainName, webhostingRoute) {

    it('delete should work'+(webhostingRoute?" through the webhosting route":"")+': '+domainName, function(done) {

             mapi.delete( (webhostingRoute?"/webhostings/"+re.expected_webhosting:"") + "/domains/domain/"+domainName,{},function(err, result, httpResponse){
                assert.property(result, "id")

                var url = "http://127.0.0.1:"+app.config.get("listen_port")+"/email/tasks/"+result.id


                request({method:'GET',uri: url}, (error, response, body) => {
                      // console.log("task request done", err, body)
                      assert.ok(body.indexOf(": successful :") > -1)
                      done()
                })

             })

        })


  }

  re.setupRelayerForFilemanRemove=function(email_full,webhostingId){
             var oGetRelayerMapiPool = app.GetRelayerMapiPool

             app.GetRelayerMapiPool = function(){
               return {
                  postAsync: function(url, payload) {
                     // console.log("postAsync", url, payload, email_full)
                     assert.equal("/s/[this]/fileman/"+(webhostingId||re.expected_webhosting)+"/remove", url)
                     assert.deepEqual(payload, {
                       pathes: ['/'+email_full],
                       path_category: 'mail'
                     })

                     return Promise.resolve({mocked:true})
                  }
               }
             }

            return oGetRelayerMapiPool
  }

  re.removeAccount=function(email_full, accountIds, index, domain_name, desc, webhostingId){

      it(desc||'removing an account should be fine: '+email_full, function(done) {

          var accountId = accountIds[index]

          var oGetRelayerMapiPool = re.setupRelayerForFilemanRemove(email_full, webhostingId)

           // console.log("!!!! calling delete", "/domains/domain/"+domain_name+"/account/"+accountId, accountIds, index)
           mapi.delete( "/domains/domain/"+domain_name+"/account/"+accountId, {}, function(err, result, httpResponse){
                assert.property(result, "id")

                var url = "http://127.0.0.1:"+app.config.get("listen_port")+"/email/tasks/"+result.id

                request({method:'GET',uri: url}, (error, response, body) => {
                      // console.log("task request done", err, body)
                      assert.ok(body.indexOf(": successful :") > -1)
                      app.GetRelayerMapiPool = oGetRelayerMapiPool
                      done()
                })

             })
      })

  }

  re.addAccount=function(email, domain_name, webhostingId, resultCallback, rawPassword){

        it('adding an account should be fine: '+email, function(done) {

             re.setupMonsterInfoWebhosting(app)

             var email_full = email+"@"+domain_name
             var oGetRelayerMapiPool = app.GetRelayerMapiPool

             app.GetRelayerMapiPool = function(){
               return {
                  postAsync: function(url, payload) {
                     // console.log("postAsync", url, payload)
                     assert.equal("/s/[this]/fileman/"+(webhostingId||re.expected_webhosting)+"/maildirmake", url)
                     assert.deepEqual(payload, {
                       dst_dir_path: '/'+email_full,
                       path_category: 'mail'
                     })

                     return Promise.resolve({mocked:true})
                  }
               }
             }

             var params = {
                 "account": re.account_id,
                 "ea_email": email_full,
                 "ea_password": rawPassword || re.default_password,
                 "ea_quota_bytes": re.validQuota,
                }

             mapi.put( "/domains/domain/"+domain_name+"/accounts", params,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.property(result, "ea_id")
                app.GetRelayerMapiPool = oGetRelayerMapiPool
                if(resultCallback) resultCallback(result)
                done()

             })

        })
  }

  re.canBeAdded = function(category, domain_name, what){

        it("canBeAdded should be "+what+" when listing "+category, function(done){

             mapi.get( "/domains/domain/"+domain_name+"/"+category,
              function(err, result, httpResponse){
                assert.isNull(err)
                assert.propertyVal(result, "canBeAdded", what)
                done()

             })

        })
  }


  return re
}