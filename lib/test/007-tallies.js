require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)


var ExpressTesterLib = require("ExpressTester")
var app = require( "../email-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const request = require('RequestCommon')
    var common = require("000-common.js")(mapi, assert, app, request)

    const domain_name = "sometallies.hu"
    const email_user1 = "account1"
    const email_full1 = "account1@sometallies.hu"
    const email_user2 = "account2"
    const email_full2 = "account2@sometallies.hu"

    const webhostingId = 20007

    var accountIds = []

    describe('basic tests', function() {

        common.createDomain(domain_name, null, webhostingId)

        addAccount(email_user1)

        addAccount(email_user2)

        it('setting tallies explcitly #1', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[0]+"/tallies/set", {bytes: parseInt(1.1*1024*1024), messages: 1}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('setting tallies explcitly #2', function(done) {

             mapi.post( "/domains/domain/"+domain_name+"/account/"+accountIds[1]+"/tallies/set", {bytes: parseInt(2.2*1024*1024), messages: 2}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })


        it('and asking for tallies to be reported', function(done) {

             var om = app.GetWebhostingMapiPool
             app.GetWebhostingMapiPool = function(){
                 return {
                    postAsync: function(uri, payload){
                        // console.log("report", uri, payload)

                        assert.equal(uri, "/s/[this]/webhosting/tallydetails")

                        assert.property(payload, webhostingId)
                        assert.deepEqual(payload[webhostingId], { wh_tally_mail_storage:
      { 'account1@sometallies.hu': '1.10',
        'account2@sometallies.hu': '2.20' } })

                        return Promise.resolve()
                    }
                 }
             }

             mapi.post( "/accounts/report/tallies", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                app.GetWebhostingMapiPool = om
                done()

             })

        })


        common.removeAccount(email_full1, accountIds, 0, domain_name, "removing account", webhostingId)
        common.removeAccount(email_full2, accountIds, 1, domain_name, "removing account", webhostingId)


    })


  function addAccount(email_user){

      return common.addAccount(email_user, domain_name, webhostingId, function(r){
         accountIds.push(r.ea_id)
      })

  }



}, 10000)

