var lib = module.exports = function(app, knexes){

    var knexAmavis = knexes.amavis

    const MError = require("MonsterError")

    var Common = require("MonsterValidators").array()

	var re = {}

  function toObject(prom) {
     return prom.then(rows=>{
           rows.map(row => dbRowToObject(row))
           Common.AddCleanupSupportForArray(rows)

           return Promise.resolve(rows)
     })
  }

  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("MSG_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }

  re.GetMailById = function(mail_id){
      return shouldBeOneRow(toObject(knexAmavis("msgs").select().whereRaw("mail_id=?", [mail_id])))
  }

  re.GetMailsInQuarantine = function(in_data){
      return knexAmavis("msgs").select("mail_id","time_iso","client_addr","size","spam_level","from_addr","subject").whereRaw("quar_loc != ''")
      .then(rows=>{
        return Promise.resolve(rows)
      })
  }

  re.GetLastMsgs = function(in_data){
      var minutes = 120
      return knexAmavis.raw(`
            SELECT
            time_iso,
            SUBSTR(policy,1,2) as pb,
            msgrcpt.content AS c, dsn_sent AS dsn, ds, bspam_level AS level, size,
            SUBSTR(sender.email,1,18) AS s,
            SUBSTR(recip.email,1,18)  AS r,
            SUBSTR(msgs.subject,1,10) AS subj
            FROM msgs LEFT JOIN msgrcpt         ON msgs.mail_id=msgrcpt.mail_id
                      LEFT JOIN maddr AS sender ON msgs.sid=sender.id
                      LEFT JOIN maddr AS recip  ON msgrcpt.rid=recip.id
            WHERE msgrcpt.content IS NOT NULL AND strftime('%Y%m%dT%H%M%SZ', 'now', '-${minutes} minute') < time_iso
            ORDER BY msgs.time_num DESC;
      `)
      .then(rows=>{
        return Promise.resolve(rows)
      })
  }

  re.GetCleanMsgs = function(in_data){
      return knexAmavis.raw(`
          SELECT count(*) as cnt, avg(bspam_level) AS bspam_level, sender.domain AS domain
          FROM msgs
          LEFT JOIN msgrcpt ON msgs.mail_id=msgrcpt.mail_id
          LEFT JOIN maddr AS sender ON msgs.sid=sender.id
          LEFT JOIN maddr AS recip ON msgrcpt.rid=recip.id
          WHERE msgrcpt.content='C'
          GROUP BY sender.domain ORDER BY cnt DESC LIMIT 50
      `)
      .then(rows=>{
        return Promise.resolve(rows)
      })

  }
  re.GetSpammyMsgs = function(in_data){
      return knexAmavis.raw(`
          SELECT count(*) as cnt, avg(bspam_level) AS spam_avg, sender.domain AS domain
          FROM msgs
          LEFT JOIN msgrcpt ON msgs.mail_id=msgrcpt.mail_id
          LEFT JOIN maddr AS sender ON msgs.sid=sender.id
          LEFT JOIN maddr AS recip ON msgrcpt.rid=recip.id
          WHERE bspam_level IS NOT NULL
          GROUP BY sender.domain 
          HAVING count(*) > 10 AND spam_avg > 0
          ORDER BY spam_avg DESC LIMIT 50
      `)
      .then(rows=>{
        return Promise.resolve(rows)
      })
  }
  re.GetDomains = function(in_data){
      return knexAmavis.raw(`
        SELECT count(*) as cnt, avg(bspam_level) AS spam_avg, sender.domain AS domain
        FROM msgs
        LEFT JOIN msgrcpt ON msgs.mail_id=msgrcpt.mail_id
        LEFT JOIN maddr AS sender ON msgs.sid=sender.id
        LEFT JOIN maddr AS recip ON msgrcpt.rid=recip.id
        GROUP BY sender.domain HAVING count(*) > 100
        ORDER BY sender.domain DESC LIMIT 100
      `)
      .then(rows=>{
        return Promise.resolve(rows)
      })
  }

  re.Cleanup = function(){
      var msg_db_days = app.config.get("amavis_cleanup_db_older_than_days")
      var msg_db_hours = app.config.get("amavis_cleanup_db_empty_older_than_hours")
      var quarantine_days = app.config.get("amavis_cleanup_quarantine_older_than_days")
      var amavis_running_uid = app.config.get("amavis_running_uid")
      var quarantine_path = app.config.get("amavis_quarantine_path")


      return knexAmavis.raw("DELETE FROM msgs WHERE time_iso < strftime('%Y%m%dT%H%M%SZ', 'now', '-"+msg_db_days+" day')")
        .then(()=>{
            return knexAmavis.raw("DELETE FROM msgs WHERE time_iso < strftime('%Y%m%dT%H%M%SZ', 'now', '-"+msg_db_hours+" day') AND content IS NULL")
        })
        .then(rows=>{
            return knexAmavis.raw(`
                DELETE FROM maddr WHERE id IN (
                  SELECT id FROM maddr LEFT JOIN (
                    SELECT sid AS id, 1 AS f FROM msgs UNION ALL
                    SELECT rid AS id, 1 AS f FROM msgrcpt
                  ) AS u USING(id) WHERE u.f IS NULL);
            `)

        })
        .then(()=>{



           if((!amavis_running_uid)||(!quarantine_path)) return Promise.reject("NOT_CONFIGURED")

           const deleteOld = require("DeleteOld")
           return deleteOld(
              quarantine_path,
              {olderThanDays: quarantine_days, setuid: amavis_running_uid})
              .catch(ex=>{
                  console.error("Error during removing old difflog backup files", ex)
                  return Promise.resolve()
              })


        })


  }

  re.SpamassassinTasks = function(){
     var cmd_spamassassin_update = app.config.get("cmd_spamassassin_update")
     return app.commander.spawn(cmd_spamassassin_update)
       .then(h=>{
          return h.executionPromise;
       })
  }

  installCron()

  return re


  function installCron(){

    if(app.cron_spamassassin_auto) return;

    app.cron_spamassassin_auto = true

    const cron = require('Croner');

    var csp = app.config.get("cron_spamassassin_auto")
    if(!csp) return;
    cron.schedule(csp, function(){
        console.log("Spamassassin related daily jobs");
        return re.SpamassassinTasks()
            .catch(ex=>{
               console.error("Exception while doing spamassassin jobs", ex);
            })
    });

  }


    function dbRowToObject(msg){

      const fs = require("MonsterDotq").fs()
      const path = require("path")
      msg.filename = path.basename(msg.quar_loc)

      msg.Cleanup = function(){
        return msg
      }

      msg.Delete = function(){
        return fs.unlinkAsync(msg.getFullFilename())
          .then(()=>{
              return knex("msgs").whereRaw("mail_id=?", msg.mail_id).delete()
          })
          .then(()=>{
              return Promise.resolve()
          })
      }

      msg.FetchAsTask = function(){
         var cmd_amavis_cat = app.config.get("cmd_amavis_cat")
         var uid = app.config.get("amavis_running_uid")
         var gid = app.config.get("amavis_running_gid")
         if((!uid)||(!gid)) throw new MError("AMAVIS_UIDS_NOT_CONFIGURED")
         return app.commander.spawn({chain:cmd_amavis_cat,downloadMimeType:"message/rfc822",download:true,spawnOptions:{uid:uid, gid: gid}}, [{filename:msg.getFullFilename()}])
      }

      msg.getFullFilename = function(){
         var quar_main = app.config.get("amavis_quarantine_path")
         if(!quar_main) throw new MError("QUARANTINE_NOT_FOUND")
         if(!msg.quar_loc) throw new MError("MSG_IS_NOT_QUARANTINED")
         return path.join(quar_main, msg.quar_loc)
      }

      return msg


    }


}
