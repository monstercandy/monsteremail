var lib = module.exports = function(app, knexes) {

    var router = app.ExpressPromiseRouter()

    var accountsLib = require("lib-accounts.js")

    var accounts = accountsLib(app, knexes)

    var RateLimit = require("RateLimit")

    var limiter = new RateLimit({
      windowMs: app.config.get("public_ratelimit_window_sec") *1000, // 15 minutes
      max: app.config.get("public_ratelimit_max"), // limit each IP to 100 requests per windowMs
    });

    var publicParameters = Array("ea_autoexpunge_days","ea_spam_tag2_level","ea_spam_kill_level");

    router.use(limiter)

      const corsResponseHeaders = {
            'Content-Type': 'text/html',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'content-type',
          }

    router.use(function(req,res,next){
        return accounts.ValidatePubRequest(req.body.json)
          .then(account=>{
              req.additionalResponseHeaders = corsResponseHeaders
              req.account = account
              next()
          })
    })

    router.post("/test", function(req,res,next){
         return req.sendOk(Promise.resolve())
    })

    router.post("/test2", function(req,res,next){
         var d = {};
         publicParameters.forEach(n=>{
             d[n] = req.account[n];
         })
         return req.sendResponse(d)
    })

    router.post("/change-password", function(req,res,next){
         // ea_password is the old password and is submitted for the common authentication
         var d = {ea_password: req.body.json.new_password}
         return req.sendOk(req.account.Change(d, req))
    })

    router.post("/change", function(req,res,next){
         var d = {};
         publicParameters.forEach(n=>{
            if(typeof req.body.json[n] != "undefined") {
              d[n] = req.body.json[n];
            }
         })
         return req.sendOk(req.account.Change(d, req))
    })

    router.post("/cleanup", function(req,res,next){
         return req.sendTask(req.account.CleanupEmails(req.body.json, null, req))
    })

    router.post("/salearn", function(req,res,next){
         return req.sendTask(req.account.SaLearn(req.body.json, req))
    })

    router.post("/whiteblacklist/put",function(req,res,next){
         return req.sendOk(req.account.AddWhitelistBlacklistItem(req.body.json, req))
    })
    router.post("/whiteblacklist/get",function(req,res,next){
         return req.sendPromResultAsIs(req.account.GetWhitelistBlacklistItems())
    })
    router.post("/whiteblacklist/delete",function(req,res,next){
         return req.sendOk(req.account.DelWhitelistBlacklistItem(req.body.json, req))
    })


    return router



}
