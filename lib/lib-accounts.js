var lib = module.exports = function(app, knexes, domain){

  var knexPostfix = knexes.postfixDovecot
  var knexDovecotDict = knexes.dovecotDict

    const KnexLib = require("MonsterKnex")
    const dotq = require("MonsterDotq");

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()

    var Common = ValidatorsLib.array()


   function nonMatchingPassword(in_data){
      var value = in_data.ea_email;
      if(in_data.ea_password != value) {
          var i = value.indexOf("@");
          if(i < 0) 
             return;
          var s = value.substr(0, i);
          if(in_data.ea_password != s)
             return;
      }

      throw new MError("VALIDATION_ERROR", {ea_password: "password can't be the same"});
   }


  const emailAccountConstraints = {
        "ea_domain": {presence: true},
        "ea_user_id": {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
        "ea_webhosting": {presence: true},

        "ea_password": (process.env.NODE_ENV == "development" ? {presence:true, isString: true} : ValidatorsLib.passwordEnforcements ),
        "ea_email": {presence: true, mcEmail: true},
        "ea_quota_bytes": {presence: true, isInteger: true},
        "ea_autoexpunge_days": {isInteger: {greaterThanOrEqualTo: 0}},
        "ea_suspended": {isBooleanLazy: true},
        "ea_spam_tag2_level": {numericality: {greaterThanOrEqualTo:0}},
        "ea_spam_kill_level": {numericality: {greaterThanOrEqualTo:0}},
      }

  const emailAccountChangeConstraints = prepareChangeConstraints()

	var re = {}

  function getAccountsByRaw(rawSql, rawParams, joinDomain) {

    var p = knexPostfix("emailaccounts").select("emailaccounts.*")
    if(joinDomain)
      p = p.leftJoin("domains", "domains.do_id", "emailaccounts.ea_domain").select("domains.do_domain_name")

    var drows
    var helper = {}
    return p.whereRaw(rawSql, rawParams)
      .then(arows=>{
           drows = arows

           var whereStrs = [];
           var whereParams = [];


           var uniqueDomains = {};
           var r = /.+@(.+)/;
           var emailAddresses = []
           drows.forEach(x => {
             helper[x.ea_email] = x
             emailAddresses.push(x.ea_email)

             var m = r.exec(x.ea_email);
             if(m) {
                var domain = m[1];
                if(!uniqueDomains[domain]) {
                  uniqueDomains[domain] = 1;
                  whereParams.push("%@"+domain);
                  whereStrs.push("username LIKE ?");
                }
             }
           })

           if(whereStrs.length <= 0) return [];

           return knexDovecotDict("emailtallies").select().whereRaw(whereStrs.join(" OR "), whereParams);
      })
      .then(trows=>{

          trows.forEach(x=>{
              if(!helper[x.username]) return;
              helper[x.username].tally_bytes = x.bytes
              helper[x.username].tally_messages = x.messages
          })

          drows.map(row => dbRowToObject(row))
          Common.AddCleanupSupportForArray(drows)

          return Promise.resolve(drows)
      })
  }


  function getAccountsCountByRaw(rawSql, rawParams) {

    var p = knexPostfix("emailaccounts").count()

    var drows
    var helper = {}
    return p.whereRaw(rawSql, rawParams)
      .then(arows=>{
           return arows[0]["count(*)"]
      })
  }

  function getAccountsBy(filterHash, joinDomain) {

    var f = KnexLib.filterHash(filterHash)

    return getAccountsByRaw(f.sqlFilterStr, f.sqlFilterValues, joinDomain)
  }
  function getAccountsCountBy(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    return getAccountsCountByRaw(f.sqlFilterStr, f.sqlFilterValues)
  }

  re.RestoreBackup = function(in_data){
     return knexPostfix("emailaccounts").insert(in_data).return();
  }

  re.ReportTallies = function(){

      return re.GetAccounts()
        .then(rows=>{
           var stats = {wh_tally_mail_storage_flush: true}
           rows.forEach(row=>{
              if(!row.tally_bytes) return
              if(!stats[row.ea_webhosting]) stats[row.ea_webhosting] = {wh_tally_mail_storage: {}}

              stats[row.ea_webhosting]["wh_tally_mail_storage"][row.ea_email] = (row.tally_bytes / 1024 / 1024).toFixed(2)
           })

           // this is started in the background, we are not particularly intersted in the result
           return app.GetWebhostingMapiPool().postAsync("/s/[this]/webhosting/tallydetails",stats)
        })


  }

  re.CleanupJunkFolders = function() {
      // /usr/local/dovecot/bin/doveadm expunge -A mailbox INBOX.Junk SENTBEFORE 30d
     var chain = app.config.get("cmd_doveadm_cleanup_junk")
     return app.commander.spawn({chain: chain}, [app.config.get("cmd_pathes"), app.config.get("cmd_common_params")])
  }

  re.AutoExpunge = function(gemitter) {
     // note: the integer expression alone (ea_autoexpunge_days > 0) matches empty strings in sqlite...
     return getAccountsByRaw("ea_autoexpunge_days > 0 AND ea_autoexpunge_days != ''", [])
       .then(accounts=>{
          return dotq.linearMap({array:accounts, catch: true, action: function(account){
                if(gemitter) gemitter.send_stdout_ln("Cleaning up old emails for "+account.ea_email+" ("+account.ea_autoexpunge_days+")")

                return account.CleanupEmails(null, gemitter)
                  .then(h=>{
                    return h.executionPromise
                  })

          }})
       })
  }

  re.GetAccountsByUserAccountAndWebhosting = function(in_data){
    return vali.async(in_data, app.listAccountWebhostingConstraint)
      .then(d=>{
         var params = {ea_user_id: d.account}
         if(d.webhosting)
            params.ea_webhosting = d.webhosting
         return getAccountsBy(params, true)
      })
      .then(accounts=>{
         return Promise.resolve(app.groupByWebhosting(accounts, "accounts", "ea"))
      })
  }

	re.GetAccounts = function(){
		 return getAccountsBy({})
	}
  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("EMAIL_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }
  re.ValidatePubRequest = function(in_data){
     var d
     return vali.async(in_data, {
       ea_email: {presence:true, mcEmail: true},
       ea_password: {presence:true, isString: true},
     })
     .then(ad=>{
        d = ad
        return re.GetAccountByEmail(d.ea_email)
     })
     .then(account=>{
        return verifyPassword(account, d.ea_password)
         .then(()=>{
            return account;
         })
     })

  }
  function verifyPassword(account, pw_candidate){
    return Promise.resolve()
      .then(()=>{
          const sshaLib = require("ssha")
          if(sshaLib.ssha256.verify(pw_candidate, account.ea_password))
            return;

          if(!account.ea_password.startsWith("{CRYPT}"))
             throw new MError("AUTH_FAILED")

          var raw = account.ea_password.substr("{CRYPT}".length);

          // note: we are spawning the command here without the commander lib to prevent the passwords being logged
          const { execFile } = require('child_process');
          const openssl_path = app.config.get("openssl_path");
          return new Promise(function(resolve,reject){
             const pw = execFile(openssl_path, ["passwd", "-crypt", "-salt", raw, pw_candidate], function(err, stdout, stderr){
                stdout = stdout.trim();
                // console.log("openssl returned", err, stdout, "x", stderr, "x", raw);
                if(err) return reject(new MError("OPENSSL_ERROR", null, err));
                if(raw != stdout) return reject(new MError("AUTH_FAILED"));

                resolve();
             })
          })

      })

  }
  re.GetAccountByEmail = function(ea_email){
     return shouldBeOneRow( getAccountsBy({ea_email: ea_email}) )
  }
  re.GetAccountByDomainAndId = function(domain, ea_id){
     return shouldBeOneRow( getAccountsBy({ea_domain: domain, ea_id: ea_id}) )
  }
  re.GetAccountById = function(ea_id){
     return shouldBeOneRow( getAccountsBy({ea_id: ea_id}) )
  }
  re.GetAccountsCountByWebhosting = function(wh_id){
     return getAccountsCountBy({ea_webhosting: wh_id})
  }
  re.GetAccountsByWebhosting = function(wh_id){
     return getAccountsBy({ea_webhosting: wh_id})
  }
  re.LookupServiceAccount = function(in_data) {
     return vali.async(in_data, {wh_id: {presence: true, isInteger: true}})
        .then(d=>{
          var svcTld = app.config.get("service_account_tld");
          var user = app.config.get("service_account_username");
          var this_server_name = app.config.get("mc_server_name");


          var domain_name = d.wh_id+"."+this_server_name+svcTld;
          var email_full = user+"@"+domain_name;
          console.log("Searching email address in database", email_full);
          return shouldBeOneRow(knexPostfix("emailaccounts").select().whereRaw("ea_email=?", [email_full]))
        })
        .then(email=>{
            return {ea_email: email.ea_email, ea_password: email.ea_password};
        })
  }
	re.Insert = function(in_data, domain, req){

      var maxAccounts
      var d
      // console.log("crap", in_data)
      return vali.async(in_data, emailAccountConstraints)
    	.then(ad=>{

          d = ad

          Array("ea_spam_tag2_level", "ea_spam_kill_level").forEach(n=>{
             if(!d[n]) {
                d[n] = null;
             }
          })

          nonMatchingPassword(d);

          if(!d.ea_password.startsWith('{CRYPT}')) {
             // console.log("transforming password")
             d.ea_password = transformPassword(d.ea_password)
          }

          return validateWebhosting(d.ea_quota_bytes)

      })
      .then(re=>{
          var svcTld = app.config.get("service_account_tld");
          var isServiceDomain = d.ea_email.endsWith(svcTld);

          if(!re.canBeAdded) {
              if(!isServiceDomain)
                throw new MError("ACCOUNT_LIMIT_IS_REACHED")
          }

          if(isServiceDomain) {
              var service_account_username = app.config.get("service_account_username");
              if(!d.ea_email.startsWith(service_account_username+"@"))
                throw new MError("INVALID_SERVICE_ACCOUNT");
          }

          

          const path = require("path").posix;
          d.ea_maildir_path = path.join(re.webhosting.extras.mail_path, d.ea_email);

      })
       .then(()=>{

           if(in_data.skip_maildirmake) return Promise.resolve()

           return doFileman(d.ea_webhosting, "maildirmake", {dst_dir_path:"/"+d.ea_email})

       })
       .then(()=>{

           return knexPostfix
             .insert(d)
             .into("emailaccounts")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                if(ex.code == 'SQLITE_CONSTRAINT') {
                   console.error(ex)
                   throw new MError("ALREADY_EXISTS")
                }
                throw ex
             })

        })
        .then((id)=>{
           app.InsertEvent(req, {e_event_type: "email-account-create",
		      e_user_id: d.ea_user_id,
              e_other: {ea_id:id, ea_email: d.ea_email}
		   })

           return Promise.resolve({ea_id:id})
        })

	}

  re.GetAccountsByDomain = function(domain_id){
     return getAccountsBy({ea_domain: domain_id})

  }

  re.DeleteAccountsByDomain = function(domain_id, in_data,emitter, req){
     return re.GetAccountsByDomain(domain_id)
       .then(emails=>{
          return dotq.linearMap({array: emails, action: (email)=>{
              return email.Delete(in_data, emitter, req);
          }})
       })

  }

	return re


  function transformPassword(clearText) {
      const sshaLib = require("ssha")
      return sshaLib.ssha256.create(clearText)
  }

  function validateWebhosting(quota_bytes) {

      return domain.GetAccountsAndLimit(true)
        .then(re=>{
           var webhosting = re.webhosting

           if((quota_bytes)&&(webhosting.template.t_storage_max_quota_soft_mb * 1024 * 1024 < quota_bytes))
                   throw new MError("QUOTA_IS_TOO_HIGH")

           /*
           // this is just not needed; accounts wont be accessible due to the domain level do_suspended flag
           if(webhosting.wh_is_expired)
             throw new MError("WEBSTORE_IS_EXPIRED");
           */

           return re

        })

  }


    function dbRowToObject(account){


    	account.Cleanup = function(){
        // console.log(account)

        delete account.ea_password
        delete account.ea_domain
    		return account
    	}

      function getClueBringer(){
          var cluebringerLib = require("lib-cluebringer.js")
          var cluebringer = cluebringerLib(app, knexes)
          return cluebringer
      }

      account.GetClueQuotas = function(){
         return getClueBringer().FetchQuotasForEmail(account.ea_email)
      }
      account.GetClueSessions = function(){
         return getClueBringer().FetchSessionsForEmail(account.ea_email)
      }

      account.Change = function(in_data, req){


          // console.log("validating as", emailAccountChangeConstraints)
          var d
          return vali.async(in_data, emailAccountChangeConstraints)
            .then(ad=>{
                d = ad

                if(!Object.keys(d).length) throw new MError("NOTHING_TO_CHANGE")

                Array("ea_spam_tag2_level", "ea_spam_kill_level").forEach(n=>{
                   if(!d[n]) {
                      d[n] = null;
                   }
                })

                if(d.ea_quota_bytes)
                   return validateWebhosting(d.ea_quota_bytes)

                return Promise.resolve()
            })
            .then(()=>{
               if(d.ea_password)
                  d.ea_password = transformPassword(d.ea_password)

              return knexPostfix("emailaccounts").whereRaw("ea_id=?",[account.ea_id]).update(d)
            })
            .then(()=>{
                  app.InsertEvent(req, {e_event_type: "account-change", e_username: account.ea_email, e_other: true});
            })

      }

      account.FetchPolicy = function(){
           return getClueBringer().FetchPolicyForEmail(account.ea_email)
      }

      account.GetDomain = function(){
         if(domain) return Promise.resolve(domain)
         return getDomains().GetDomainById(account.ea_domain)
      }


      account.Delete = function(in_data, emitter, req){

            if(emitter)emitter.send_stdout_ln("Deleting account "+account.ea_email+"...")

            // console.log("delete called on account:", account)

            return account.GetDomain()
              .then(adomain=>{
                 return adomain.GetBccsByName(account.ea_email)
              })
              .then(bccs=>{
                 var ps = []
                 if(bccs.length > 0) {
                    if(emitter)emitter.send_stdout_ln("Deleting bccs")
                 }

                 bccs.forEach(bcc=>{
                    ps.push(bcc.Delete(null, req))
                 })
                 return Promise.all(ps)
              })
              .then(()=>{
                 return doFileman(account.ea_webhosting, "remove", {pathes:["/"+account.ea_email]}, emitter)
                   .catch(ex=>{
                      // it has already been deleted it seems... lets proceed.
                      if(ex.message.match(/^(NOT_FOUND|WEBHOSTING_NOT_FOUND)$/))
                         return Promise.resolve()

                      throw ex
                   })
              })
              .then(()=>{
                 return knexPostfix("emailaccounts").whereRaw("ea_id=?",[account.ea_id]).delete()
              })
              .then(()=>{
                 return knexDovecotDict("emailtallies").whereRaw("username=?",[account.ea_email]).delete()
              })
              .then(()=>{
                 return knexPostfix("whitelist_blacklist").whereRaw("wb_emailaccount=?",[account.ea_id]).delete()
              })
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "email-account-remove",
    		        e_user_id: account.ea_user_id,
                    e_other: {ea_id:account.ea_id, ea_email: account.ea_email}}
                 )

                 return Promise.resolve()
              })

      }

      account.GetWhitelistBlacklistItems = function(in_data){
          return knexPostfix("whitelist_blacklist").select("wb_senderemail", "wb_action")
            .whereRaw("wb_emailaccount=?", [account.ea_id])
            .then(rows=>{

                return Promise.resolve(rows)

            })

      }

      account.DelWhitelistBlacklistItem = function(in_data, req){
          return vali.async(in_data, {
            "wb_senderemail": {presence: true, mcEmail: {catchDomainAllowed: true}},
          })
          .then(d=>{
                return knexPostfix("whitelist_blacklist").whereRaw(
                  "wb_emailaccount=? AND wb_senderemail=?",
                  [account.ea_id, in_data.wb_senderemail]
                ).delete()
          })
          .then(()=>{
              app.InsertEvent(req, {e_event_type: "whitelist-removed", e_other: true});
          })
      }

      account.AddWhitelistBlacklistItem = function(in_data, req){
          var d
          return vali.async(in_data, {
            "wb_senderemail": {presence: true, mcEmail: {catchDomainAllowed: true}},
            "wb_action": {presence: true, inclusion: ["W","B"]},
          })
          .then((ad)=>{
             d = ad

             return account.GetWhitelistBlacklistItems()
          })
          .then(rows=>{
              if(rows.length > app.config.get("max_whitelist_blacklist_items_per_email_account"))
                throw new MError("TOO_MANY_ITEMS")

               d.wb_emailaccount = account.ea_id
               return knexPostfix
                 .insert(d)
                 .into("whitelist_blacklist")
                 .then(ids=>{
                    return Promise.resolve(ids[0])
                 })

          })
          .then(r=>{
                app.InsertEvent(req, {e_event_type: "whitelist-insert", e_other: true});
                return r;
          })

      }

      account.SaLearn = function(in_data, req) {

         var sa = app.getSaLearner()
         in_data.email = account.ea_email
         app.InsertEvent(req, {e_event_type:"spamassassin-learn",e_other:true});
         return sa.Learn(in_data)
      }

      account.SetTallies = function(in_data){
         return vali.async(in_data, {messages:{presence: true, isInteger:true}, bytes:{presence:true, isInteger:true}})
           .then(d=>{
              return knexDovecotDict.raw('INSERT OR REPLACE INTO emailtallies (username, messages, bytes) VALUES(?,?,?)', [account.ea_email, d.messages, d.bytes])
                .then(()=>{
                  return Promise.resolve()
                })
           })
      }

      account.CleanupEmails = function(in_data, emitter, req){
        //    /usr/local/dovecot/bin/doveadm -v expunge -u tech@monstermedia.hu mailbox INBOX SENTBEFORE 10000d

         return vali.async(in_data || {}, {older_than_days: {isInteger: true}})
           .then(d=>{
               if(!d.older_than_days)
                  d.older_than_days = account.ea_autoexpunge_days

               if(!d.older_than_days)
                 throw new MError("DAYS_MISSING")

               app.InsertEvent(req, {e_event_type:"cleanup-old-emails",e_other:true});

               var chain = app.config.get("cmd_doveadm_expunge")
               return app.commander.spawn({chain: chain, emitter: emitter}, [app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), dovecotCommonParams(), d])
           })

      }

      account.RecalculateTallies = function(in_data) {
           //   /usr/local/dovecot/bin/doveadm quota recalc -u tech@monstermedia.hu

           var chain = app.config.get("cmd_doveadm_quota_recalc")
           return app.commander.spawn({chain: chain}, [app.config.get("cmd_pathes"), app.config.get("cmd_common_params"), dovecotCommonParams()])
      }

    	return account


      function dovecotCommonParams(){
          return {ea_email: account.ea_email}
      }
    }


    function prepareChangeConstraints(){
          var constraints = {}

          Object.keys(emailAccountConstraints).forEach(x=>{
            if(Array(
                "ea_password", 
                "ea_autoexpunge_days", 
                "ea_quota_bytes", 
                "ea_suspended", 
                "ea_spam_tag2_level",
                "ea_spam_kill_level",
              ).indexOf(x) < 0) 
            {
              return;
            }
            constraints[x] = simpleCloneObject(emailAccountConstraints[x])
            delete constraints[x].presence
          })

          return constraints
    }


    function doFileman(wh_id, op, payload, emitter){
        var relayer = app.GetRelayerMapiPool()

        payload.path_category = "mail"

        return relayer.postAsync("/s/[this]/fileman/"+wh_id+"/"+op, payload)
            .then(u=>{

               if(u.mocked) return Promise.resolve()

               return new Promise(function(resolve,reject){

                  var pipeConfig = app.Mapi.cloneServersAsPipe(app.ServersFileman.FirstServer())
                  var mc = app.Mapi(pipeConfig)

                  return mc.request("GET", '/fileman/tasks/'+u.result.id)
                    .on('error', reject)
                    .on("data", function(data){
                      if(emitter) emitter.send_stdout(data)
                    })
                    .on("response", function(response){
                       response.on("end", resolve)
                    })

               })

            })

    }

    function getDomains(){
         var domainsLib = require("lib-domains.js")
         return domainsLib(app, knexes)
    }


}
