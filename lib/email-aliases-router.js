var lib = module.exports = function(app, knexes) {

    var router = app.ExpressPromiseRouter({mergeParams: true})

    var aliasesLib = require("lib-aliases.js")

    var aliases = aliasesLib(app, knexes)

    router.post("/list", function(req,res,next){
      return req.sendPromResultAsIs(aliases.GetAliasesByUserAccountAndWebhosting(req.body.json))
    })

    router.route("/alias/:alias")
      .post(function(req,res,next){
          return req.sendOk(
             aliases.GetAliasById(req.params.alias)
             .then(alias=>{
                return alias.Change(req.body.json)
             })
          )

      })    
      .delete(function(req,res,next){
          return req.sendOk(
             aliases.GetAliasById(req.params.alias)
             .then(alias=>{
                return alias.Delete(null, req)
             })
          )

      })

    router.route("/")
      .get(function(req,res,next){
         return aliases.GetAliases(req.params.webhosting_id).then(aliases=>{
         	req.sendResponse({aliases:aliases.Cleanup()})
         })
      })


    return router


}
