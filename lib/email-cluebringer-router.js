var lib = module.exports = function(app, knexes) {

    var cluebringerLib = require("lib-cluebringer.js")

    var cluebringer = cluebringerLib(app, knexes)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.route("/info/quotas/")
      .get(function(req,res,next){
          return req.sendPromResultAsIs(cluebringer.FetchAllQuotas())
      })


    router.route("/info/sessions/")
      .get(function(req,res,next){
          return req.sendPromResultAsIs(cluebringer.FetchAllSessions())
      })

    router.post("/cleanup", function(req,res,next){
       return req.sendTask(cluebringer.Cleanup())
    })

    router.post("/policies/backup", function(req,res,next){
        return req.sendPromResultAsIs(cluebringer.BackupPolicies())
    })
    router.post("/policies/merge", function(req,res,next){
        return req.sendOk(cluebringer.MergePolicies(req.body.json))
    })

    router.post("/policies/outbound/:policy_name/flush", function(req,res,next){
        return req.sendOk(cluebringer.FlushOutboundQuotasForPolicy(req.params.policy_name))
    })

    router.route("/policies/outbound/:policy_name/members")
      .get(function(req,res,next){
        return req.sendPromResultAsIs(cluebringer.FetchEmailsForPolicy(req.params.policy_name))
      })
      .put(function(req,res,next){
        return req.sendOk(
           getAccountLib().GetAccountByEmail(req.body.json.ea_email)
             .then(()=>{
                 return cluebringer.AddEmailForPolicy(req.params.policy_name, req.body.json)
             })
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-policy-member-insert", e_other: true});
            })
        )
      })
      .delete(function(req,res,next){
        return req.sendOk(
            cluebringer.DeleteEmailForPolicy(req.params.policy_name, req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-policy-member-removed", e_other: true});
            })
        )
      })

    router.route("/policies/outbound/:policy_name/quotas")
      .get(function(req,res,next){
        return req.sendPromResultAsIs(cluebringer.FetchQuotasForPolicy(req.params.policy_name))
      })
      .post(function(req,res,next){
        return req.sendOk(
           cluebringer.ReplaceQuotasForPolicy(req.params.policy_name, req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-quota-change", e_other: true});
            })
        )
      })

    router.route("/policies/outbound/:policy_name")
      .delete(function(req,res,next){
        return req.sendOk(
            cluebringer.RemovePolicy(req.params.policy_name)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-policy-removed", e_other: true});
            })
        )
      })

    router.route("/policies/outbound/")
      .get(function(req,res,next){
        // return policies beginning with mc_
         return req.sendPromResultAsIs(cluebringer.FetchAllOutboundPolicies())
      })
      .put(function(req,res,next){
        // creating a new policy
         return req.sendOk(
           cluebringer.AddNewPolicy(req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-policy-insert", e_other: true});
            })
         )
      })

    router.route("/groups/")
      .get(function(req,res,next){
         return req.sendPromResultAsIs(cluebringer.FetchAllGroups())
      })

    router.route("/groups/:group_name")
      .get(function(req,res,next){
         return req.sendPromResultAsIs(cluebringer.FetchGroupMembersByName(req.params.group_name))
      })
      .put(function(req,res,next){
         return req.sendOk(
           cluebringer.AddGroupMember(req.params.group_name, req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-group-member-insert", e_other: true});
            })
         )
      })
      .delete(function(req,res,next){
         return req.sendOk(
            cluebringer.RemoveGroupMember(req.params.group_name, req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "cluebringer-group-member-removed", e_other: true});
            })
         )
      })

    return router


  function getAccountLib(){
      return require("lib-accounts.js")(app, knexes)
  }
}
