var lib = module.exports = function(app, knexes) {

    var domainsLib = require("lib-domains.js")
    var domains = domainsLib(app, knexes)

      var accountsLib = require("lib-accounts.js")
      var accounts = accountsLib(app, knexes)

      var aliasesLib = require("lib-aliases.js")
      var aliases = aliasesLib(app, knexes)

      var bccsLib = require("lib-bccs.js")
      var bccs = bccsLib(app, knexes)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.post("/", function(req,res,next){

      var re = {}
      return domains.GetDomainsByUserAccountAndWebhosting(req.body.json)
        .then(x=>{
           re.domains = x.domains
           return accounts.GetAccountsByUserAccountAndWebhosting(req.body.json)
        })
        .then(x=>{
           re.accounts = x.accounts
           return bccs.GetBccsByUserAccountAndWebhosting(req.body.json)
        })
        .then(x=>{
           re.bccs = x.bccs
           return aliases.GetAliasesByUserAccountAndWebhosting(req.body.json)
        })
        .then(x=>{
           re.aliases = x.aliases
           return req.sendResponse(re)
        })

    })


    return router


}
