FROM monstercommon

ADD lib /opt/MonsterEmail/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterEmail/lib/bin/email-install.sh && /opt/MonsterEmail/lib/bin/email-test.sh

ENTRYPOINT ["/opt/MonsterEmail/lib/bin/email-start.sh"]
